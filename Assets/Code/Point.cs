﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Application {
    public struct Point {
        public readonly int X;
        public readonly int Y;

        public static readonly Point Zero = new Point(0, 0);

        public Point(int x, int y) {
            X = x;
            Y = y;
        }

        public static Point operator +(Point a, Point b) {
            return new Point(a.X + b.X, a.Y + b.Y);
        }

        public static Point operator -(Point point) {
            return new Point(-point.X, -point.Y);
        }
    
        public static Point operator -(Point a, Point b) {
            return new Point(a.X - b.X, a.Y - b.Y);
        }

        public static Point operator*(Point p, double n) {
            return new Point((int)(p.X * n), (int)(p.Y * n));
        }

        public static Point operator/(Point p, int n) {
            return new Point(p.X / n, p.Y / n);
        }

        public static bool operator ==(Point a, Point b) {
            return a.X == b.X && a.Y == b.Y;
        }

        public static bool operator !=(Point a, Point b) {
            return !(a == b);
        }

        public override string ToString() {
            return $"({X}, {Y})";
        }

        public override bool Equals(object obj) {
            return obj is Point && this == (Point)obj;
        }

        public override int GetHashCode() {
            return X * 100000 + Y;
        }

        public int Manhattan(Point other) {
            return Math.Abs(X - other.X) + Math.Abs(Y - other.Y);
        }

        public int Chebyshev(Point other) {
            return Math.Max(Math.Abs(X - other.X), Math.Abs(Y - other.Y));
        }

        public Point Sign() {
            return new Point(Math.Sign(X), Math.Sign(Y));
        }

        // TODO: make more correct
        public Orientation Orientation => X != 0 ? Orientation.Horizontal : Orientation.Vertical;

        public IEnumerable<Point> Neighbors {
            get {
                yield return this + Up;
                yield return this + Down;
                yield return this + Left;
                yield return this + Right;
            }
        }

        public IEnumerable<Point> InclusiveNeighbors {
            get {
                yield return this + Up;
                yield return this + Down;
                yield return this + Left;
                yield return this + Right;
                yield return this + Up + Left;
                yield return this + Up + Right;
                yield return this + Down + Left;
                yield return this + Down + Right;
            }
        }

        public Vector2 ToVector() {
            return new Vector2(X, Y);
        }

        public Vector3 ToVector3() {
            return new Vector3(X, Y, 0);
        }

        public static Point Horizontal(int x) {
            return new Point(x, 0);
        }

        public static Point Vertical(int y) {
            return new Point(0, y);
        }

        public static Point Square(int size) {
            return new Point(size, size);
        }

        public Point Along(Orientation orientation) {
            return orientation == Orientation.Horizontal
                ? new Point(X, 0)
                : new Point(0, Y);
        }

        public int Min() {
            return Math.Min(X, Y);
        }

        public int Max() {
            return Math.Max(X, Y);
        }

        public static Point Left = new Point(-1, 0);
        public static Point Right = new Point(1, 0);
        public static Point Up = new Point(0, 1);
        public static Point Down = new Point(0, -1);
    }
}
