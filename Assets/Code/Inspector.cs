namespace Application {
    public class Inspector {
        public Entity InspectedEntity;

        public void Inspect(Entity entity) {
            InspectedEntity = entity;
        }

        public void Clear() {
            InspectedEntity = null;
        }
    }
}