namespace Application {
    public enum Intent {
        Enter,
        Exit,
        PickUp,
    }
}