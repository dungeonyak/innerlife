using System.Collections.Generic;

namespace Application {
    public static class Messages {
        public static ICollection<RichText> History = new List<RichText>();

        public static void Write(string markup) {
            History.Add(new RichText(markup));
        }

        public static void Clear() {
            History.Clear();
        }
    }
}