namespace Application {
    public interface TileRenderer {
        void Draw(Map map, Point point, int layer, int order);
    }
}