﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Application {
    using MaterialKey = ValueTuple<Texture2D, Color?, Color?>;

    public class SpritePool {
        private IList<GameObject> Objects = new List<GameObject>();
        private int UsedCount = 0;
        private int LastUsedCount = 0;
        private Material SpriteMaterial;
        private Dictionary<MaterialKey, MaterialPropertyBlock> PropertyBlocks
            = new Dictionary<MaterialKey, MaterialPropertyBlock>();

        [Inject] public SpriteCropper SpriteCropper;

        public SpritePool() {
            SpriteMaterial = Resources.Load("SpriteMaterial", typeof(Material)) as Material;
        }

        public void Begin() {
            UsedCount = 0;
        }

        public void End() {
            var unusedObjects = Objects.Skip(UsedCount).Take(LastUsedCount - UsedCount);

            foreach (var o in unusedObjects) {
                o.SetActive(false);
            }

            LastUsedCount = UsedCount;
        }

        public void Draw(
            Sprite sprite,
            Point position,
            int layer,
            int? order = 1,
            Color? color = null,
            bool flipped = false,
            Point? sliceSize = null,
            Vector2? scale = null,
            Rect? sourceRect = null,
            Color? outlineColor = null,
            float? alpha = null,
            float? fade = null
        ) {
            var o = DraftObject();
            var spriteRenderer = o.GetComponent<SpriteRenderer>();

            o.SetActive(true);

            o.layer = layer;

            o.transform.position = new Vector3(
                position.X,
                position.Y,
                1f
            );

            if (sourceRect.HasValue) {
                spriteRenderer.sprite = SpriteCropper.Crop(sprite, sourceRect.Value);
            } else {
                spriteRenderer.sprite = sprite;
            }

            spriteRenderer.sortingOrder = order ?? 0;

            if (color.HasValue) {
                spriteRenderer.color = color.Value;
            } else {
                spriteRenderer.color = Color.white;
            }

            if (fade.HasValue) {
                spriteRenderer.color *= new Color(
                    spriteRenderer.color.r * fade.Value,
                    spriteRenderer.color.g * fade.Value,
                    spriteRenderer.color.b * fade.Value,
                    spriteRenderer.color.a
                );
            }

            if (alpha.HasValue) {
                spriteRenderer.color = new Color(
                    spriteRenderer.color.r,
                    spriteRenderer.color.g,
                    spriteRenderer.color.b,
                    alpha.Value
                );
            }

            if (sliceSize.HasValue) {
                spriteRenderer.drawMode = SpriteDrawMode.Sliced;
                spriteRenderer.size = new Vector2(sliceSize.Value.X, sliceSize.Value.Y);
            } else {
                spriteRenderer.drawMode = SpriteDrawMode.Simple;
                spriteRenderer.size = new Vector2(1, 1);
            }

            o.transform.localScale = scale ?? new Vector2(1, 1);

            if (flipped) {
                spriteRenderer.flipX = true;
                o.transform.position += new Vector3(sprite.rect.width, 0, 0);
            } else {
                spriteRenderer.flipX = false;
            }

            var block = new MaterialPropertyBlock();
            spriteRenderer.GetPropertyBlock(block);

            if (outlineColor.HasValue) {
                block.SetFloat("_Outline", 1);
                block.SetColor("_OutlineColor", outlineColor.Value);
            } else {
                block.SetFloat("_Outline", 0);
            }

            spriteRenderer.SetPropertyBlock(block);
        }

        private GameObject DraftObject() {
            if (UsedCount >= Objects.Count) {
                Allocate();
            }

            return Objects[UsedCount++];
        }

        private void Allocate() {
            var count = 10;

            for (var i = 0; i < count; i++) {
                Objects.Add(CreateObject());
            }
        }

        private GameObject CreateObject() {
            var o = new GameObject();
            o.name = $"Pooled Sprite {Objects.Count}";
            var spriteRenderer = o.AddComponent<SpriteRenderer>();
            spriteRenderer.material = SpriteMaterial;
            return o;
        }

        public void Clear() {
            Objects.ToList().ForEach(o => GameObject.Destroy(o));
            Objects.Clear();
            UsedCount = 0;
        }
    }
}
