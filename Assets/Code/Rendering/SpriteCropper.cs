using System.Collections.Generic;
using UnityEngine;

namespace Application {
    public class SpriteCropper {
        private Dictionary<CropSpecification, Sprite> Sprites = new Dictionary<CropSpecification, Sprite>();

        public Sprite Crop(Sprite sprite, Rect region) {
            var spec = new CropSpecification() {
                Sprite = sprite,
                Region = region,
            };

            return Sprites.SetDefault(spec, () => {
                return Sprite.Create(
                    sprite.texture,
                    new Rect(
                        sprite.rect.position + region.position,
                        region.size
                    ),
                    sprite.pivot,
                    sprite.pixelsPerUnit,
                    0,
                    SpriteMeshType.FullRect,
                    sprite.border,
                    false
                );
            });
        }
    }

    struct CropSpecification {
        public Sprite Sprite;
        public Rect Region;
    }
}