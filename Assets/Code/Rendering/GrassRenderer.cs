using System;
using static Application.NeighborRequirement;

namespace Application {
    public class GrassRenderer : NeighboredTileRenderer {
        public static IAnimation TopLeft = DawnLike.Still("Objects/Floor", "GreenGrass_TopLeft");
        public static IAnimation TopRight = DawnLike.Still("Objects/Floor", "GreenGrass_TopRight");
        public static IAnimation BottomLeft = DawnLike.Still("Objects/Floor", "GreenGrass_BottomLeft");
        public static IAnimation BottomRight = DawnLike.Still("Objects/Floor", "GreenGrass_BottomRight");
        public static IAnimation Top = DawnLike.Still("Objects/Floor", "GreenGrass_Top");
        public static IAnimation Bottom = DawnLike.Still("Objects/Floor", "GreenGrass_Bottom");
        public static IAnimation Left = DawnLike.Still("Objects/Floor", "GreenGrass_Left");
        public static IAnimation Right = DawnLike.Still("Objects/Floor", "GreenGrass_Right");
        public static IAnimation Center = DawnLike.Still("Objects/Floor", "GreenGrass_Center");
        public static IAnimation TopNarrow = DawnLike.Still("Objects/Floor", "GreenGrass_TopNarrow");
        public static IAnimation VerticalNarrow = DawnLike.Still("Objects/Floor", "GreenGrass_VerticalNarrow");
        public static IAnimation BottomNarrow = DawnLike.Still("Objects/Floor", "GreenGrass_BottomNarrow");
        public static IAnimation LeftNarrow = DawnLike.Still("Objects/Floor", "GreenGrass_LeftNarrow");
        public static IAnimation RightNarrow = DawnLike.Still("Objects/Floor", "GreenGrass_RightNarrow");
        public static IAnimation HorizontalNarrow = DawnLike.Still("Objects/Floor", "GreenGrass_HorizontalNarrow");
        public static IAnimation Island = DawnLike.Still("Objects/Floor", "GreenGrass_Island");

        public GrassRenderer() {
            // top left
            DefineTile(
                TopLeft,
                new NeighborSpecification(
                    Any, Off, Any,
                    Off, /**/ Yes,
                    Any, Yes, Any
                )
            );

            // top/bottom
            DefineTile(
                TopRight,
                new NeighborSpecification(
                    Any, Off, Any,
                    Yes, /**/ Off,
                    Any, Yes, Any
                )
            );

            // top right
            DefineTile(
                BottomLeft,
                new NeighborSpecification(
                    Any, Yes, Any,
                    Off, /**/ Yes,
                    Any, Off, Any
                )
            );

            // top right
            DefineTile(
                BottomRight,
                new NeighborSpecification(
                    Any, Yes, Any,
                    Yes, /**/ Off,
                    Any, Off, Any
                )
            );

            // left/right
            DefineTile(
                Top,
                new NeighborSpecification(
                    Any, Off, Any,
                    Yes, /**/ Yes,
                    Any, Yes, Any
                )
            );

            // bottom left
            DefineTile(
                Bottom,
                new NeighborSpecification(
                    Any, Yes, Any,
                    Yes, /**/ Yes,
                    Any, Off, Any
                )
            );
                
            // bottom right
            DefineTile(
                Left,
                new NeighborSpecification(
                    Any, Yes, Any,
                    Off, /**/ Yes,
                    Any, Yes, Any
                )
            );

            // bottom right
            DefineTile(
                Right,
                new NeighborSpecification(
                    Any, Yes, Any,
                    Yes, /**/ Off,
                    Any, Yes, Any
                )
            );

            // bottom right
            DefineTile(
                Center,
                new NeighborSpecification(
                    Any, Yes, Any,
                    Yes, /**/ Yes,
                    Any, Yes, Any
                )
            );

            // bottom right
            DefineTile(
                TopNarrow,
                new NeighborSpecification(
                    Any, Off, Any,
                    Off, /**/ Off,
                    Any, Yes, Any
                )
            );
                        // bottom right
            DefineTile(
                VerticalNarrow,
                new NeighborSpecification(
                    Any, Yes, Any,
                    Off, /**/ Off,
                    Any, Yes, Any
                )
            );
                        // bottom right
            DefineTile(
                BottomNarrow,
                new NeighborSpecification(
                    Any, Yes, Any,
                    Off, /**/ Off,
                    Any, Off, Any
                )
            );

                        // bottom right
            DefineTile(
                LeftNarrow,
                new NeighborSpecification(
                    Any, Off, Any,
                    Off, /**/ Yes,
                    Any, Off, Any
                )
            );

                                    // bottom right
            DefineTile(
                RightNarrow,
                new NeighborSpecification(
                    Any, Off, Any,
                    Yes, /**/ Off,
                    Any, Off, Any
                )
            );

                                    // bottom right
            DefineTile(
                HorizontalNarrow,
                new NeighborSpecification(
                    Any, Off, Any,
                    Yes, /**/ Yes,
                    Any, Off, Any
                )
            );

                                    // bottom right
            DefineTile(
                Island,
                new NeighborSpecification(
                    Any, Off, Any,
                    Off, /**/ Off,
                    Any, Off, Any
                )
            );
        }

        protected override int GetBitfield(Map map, Point point) {
            var calculator = new MapTileNeighborCalculator(map, tile => tile == Tile.Grass || tile == Tile.Wall);
            return calculator.GetBitfield(point);
        }
    }
}
