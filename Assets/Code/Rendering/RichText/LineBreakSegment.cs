using System;
using UnityEngine;

namespace Application {
    public struct LineBreakSegment : RichTextSegment {
        public int GetDuration() {
            return 4;
        }
    }
}
