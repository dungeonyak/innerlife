﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using UnityEngine;

namespace Application {
    public class RichTextParser {
        private Dictionary<string, IEnumerable<RichTextSegment>> Cache = new Dictionary<string, IEnumerable<RichTextSegment>>();

        public IEnumerable<RichTextSegment> Parse(string markup) {
            return Cache.SetDefault(markup, () => {
                var preparedMarkup = Regex.Replace(markup, "([^<>]+)(<[^/]|$)", "<normal>$1</normal>$2");
                preparedMarkup = $"<text>{preparedMarkup}</text>";
                
                var doc = XDocument.Parse(preparedMarkup).Document;

                return doc.Elements().SelectMany(element => ReadElement(element));
            });
        }

        private IEnumerable<RichTextSegment> ReadElement(XElement element, Color? parentColor = null) {
            var tagName = element.Name.ToString();

            if (!element.Elements().Any()) {
                if (TranslateColor(tagName) is Color color) {
                    return ReadCharacters(element.Value, color);
                } else if (tagName == "char") {
                    var charCode = int.Parse(element.Attribute("code").Value);
                    var character = (char) charCode;
                    var charSegment = new CharacterSegment() {
                        Character = character,
                        Color = parentColor ?? DawnLike.White,
                    };
                    return Enumerable.Repeat(charSegment as RichTextSegment, 1);
                }
            } else if (TranslateColor(tagName) is Color color) {
                return element.Elements().SelectMany(descendant => ReadElement(descendant, color));
            } else {
                return element.Elements().SelectMany(descendant => ReadElement(descendant, parentColor));
            }

            throw new Exception($"Don't know what to do with {tagName}");
        }

        private IEnumerable<RichTextSegment> ReadCharacters(string text, Color? color = null) {
            return text.Select<char, RichTextSegment>(character => {
                if (character == '\n') {
                    return new LineBreakSegment();
                } else if (character == ' ') {
                    return new SpaceSegment();
                } else {
                    return new CharacterSegment() {
                        Character = character,
                        Color = color ?? DawnLike.White,
                    };
                }
            });
        }
        
        private Color? TranslateColor(string colorName) {
            switch (colorName) {
                case "normal": return DawnLike.White;
                case "red": return DawnLike.Red;
                case "blue": return DawnLike.Blue;
                case "brown": return DawnLike.Brown;
                case "green": return DawnLike.Green;
                case "slate": return DawnLike.Slate;
                case "gold": return DawnLike.Yellow;
                case "grey": return DawnLike.Grey;
            }

            return null;
        }
    }
}
