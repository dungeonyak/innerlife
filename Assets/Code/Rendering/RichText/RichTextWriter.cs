﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Application {
    class WrapCache : Dictionary<
        ValueTuple<RichText, SpriteFont, int>,
        IEnumerable<RichTextLine>
    > {}

    public static class RichTextWriter {
        private static WrapCache WrapCache = new WrapCache();

        public static void Write(
            SpriteFont font,
            RichText richText,
            Point anchorPoint,
            int layer,
            int? wrapWidth = null,
            int? progress = null,
            int? order = null,
            int spacing = 2,
            int? maxLines = null,
            Anchor anchor = Anchor.TopLeft
        ) {
            var lines = BreakIntoLines(richText, font, wrapWidth);

            var currentLine = 0;
            var requiredProgress = 0;

            var topLeft = anchor == Anchor.TopLeft
                ? anchorPoint
                : anchorPoint + Point.Vertical(MeasureHeight(richText, font, wrapWidth, spacing));

            IEnumerable<RichTextLine> linesToDraw;

            if (maxLines.HasValue) {
                if (anchor == Anchor.BottomLeft) {
                    linesToDraw = lines.Reverse().Take(maxLines.Value).Reverse();

                    var undrawnCount = lines.Count() - linesToDraw.Count();
                    topLeft -= Point.Vertical(undrawnCount * (font.Height + spacing));
                } else {
                    linesToDraw = lines.Take(maxLines.Value);
                }
            } else {
                linesToDraw = lines;
            }

            foreach (var line in linesToDraw) {
                var currentPosition = topLeft
                    - Point.Vertical(font.Height)
                    - Point.Vertical(font.Height + spacing) * currentLine;

                foreach (var segment in line.Segments) {
                    if (progress.HasValue && requiredProgress > progress.Value) {
                        return;
                    }

                    if (segment is CharacterSegment charSegment) {
                        font.Write(charSegment.Character, currentPosition, layer, charSegment.Color, order: order);
                        currentPosition += Point.Horizontal(font.Width);
                    } else if (segment is SpaceSegment) {
                        currentPosition += Point.Horizontal(font.Width);
                    }

                    requiredProgress += segment.GetDuration();
                }

                currentLine++;
            }
        }

        public static int MeasureWidth(RichText richText, SpriteFont font, int? wrapWidth = null) {
            return BreakIntoLines(richText, font, wrapWidth)
                .Select(line => line.MeasureWidth(font))
                .Max();
        }

        public static int MeasureHeight(RichText richText, SpriteFont font, int? wrapWidth = null, int spacing = 2) {
            var lineCount = CountLines(richText, font, wrapWidth);
            return MeasureHeight(lineCount, font, spacing);
        }

        public static int CountLines(RichText richText, SpriteFont font, int? wrapWidth) {
            return BreakIntoLines(richText, font, wrapWidth).Count();
        }

        public static int MeasureHeight(int lineCount, SpriteFont font, int spacing = 2) {
            return lineCount * (font.Height + spacing) - spacing;
        }

        private static IEnumerable<RichTextLine> BreakIntoLines(
            RichText richText,
            SpriteFont font,
            int? wrapWidth = null
        ) {
            if (richText.LineBreaks == 0 && !wrapWidth.HasValue) {
                return Enumerable.Repeat(new RichTextLine(richText.Segments), 1);
            }

            var width = wrapWidth ?? Int32.MaxValue;

            return WrapCache.SetDefault((richText, font, width), () => {
                var lines = new List<RichTextLine>();
                var currentLineSegments = new List<RichTextSegment>();
                var currentWord = new List<RichTextSegment>();

                foreach (var segment in richText.Segments) {
                    currentWord.Add(segment);

                    if (segment is CharacterSegment charSegment) {
                        var maxLength = width / font.Width;
                        var currentWordLength = currentWord.OfType<CharacterSegment>().Count();

                        if (currentLineSegments.Count + currentWordLength > maxLength) {
                            lines.Add(new RichTextLine(currentLineSegments));
                            currentLineSegments = new List<RichTextSegment>();
                        }
                    } else if (segment is SpaceSegment) {
                        currentLineSegments.AddRange(currentWord);
                        currentWord = new List<RichTextSegment>();
                    } else if (segment is LineBreakSegment) {
                        currentLineSegments.AddRange(currentWord);
                        lines.Add(new RichTextLine(currentLineSegments));
                        currentLineSegments = new List<RichTextSegment>();
                        currentWord = new List<RichTextSegment>();
                    }
                }

                if (currentWord.Any()) {
                    currentLineSegments.AddRange(currentWord);
                }

                if (currentLineSegments.Any()) {
                    lines.Add(new RichTextLine(currentLineSegments));
                }

                return lines;
            });
        }
    }
}
