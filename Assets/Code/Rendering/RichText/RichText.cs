using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Application {
    public class RichText {
        private readonly string Markup;
        public readonly IEnumerable<RichTextSegment> Segments;

        public int Length => CharCount + SpaceCount;
        private int SpaceCount => Segments.OfType<SpaceSegment>().Count();
        private int CharCount => Segments.OfType<CharacterSegment>().Count();

        public int TotalDuration => Segments.Select(s => s.GetDuration()).Sum();

        public readonly int LineBreaks;

        public RichText(string markup) {
            Markup = markup;
            Markup = Regex.Replace(Markup, "^\\s+|\\s+$", "", RegexOptions.Multiline);
            Markup = Regex.Replace(Markup, "\\s*\\n\\s*", "\n", RegexOptions.Multiline);

            Segments = Container.Resolve<RichTextParser>().Parse(Markup);

            LineBreaks = markup.Count(c => c == '\n');
        }

        public override bool Equals(object o) {
            return o is RichText && ((RichText) o).Markup == Markup;
        }

        public override int GetHashCode() {
            return Markup.GetHashCode();
        }

        public static RichText operator+(RichText a, RichText b) {
            return new RichText(a.Markup + b.Markup);
        }
    }
}