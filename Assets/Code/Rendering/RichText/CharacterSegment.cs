﻿using System;
using UnityEngine;

namespace Application {
    public struct CharacterSegment : RichTextSegment {
        public char Character;
        public Color Color;

        public int GetDuration() {
            return
                Character == '.' ? 120 :
                Character == ',' ? 80 :
                10;
        }
    }
}
