using System.Collections.Generic;
using System.Linq;

namespace Application
{
    public class RichTextLine
    {
        public readonly IEnumerable<RichTextSegment> Segments;

        public RichTextLine(IEnumerable<RichTextSegment> segments)
        {
            Segments = segments;
        }

        public int MeasureWidth(SpriteFont font)
        {
            return font.Width * Segments.Where(s => s is CharacterSegment || s is SpaceSegment).Count();
        }
    }
}