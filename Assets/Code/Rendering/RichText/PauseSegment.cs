using System;
using UnityEngine;

namespace Application {
    public struct PauseSegment : RichTextSegment {
        public readonly int Duration;

        public PauseSegment(int duration) {
            Duration = duration;
        }

        public int GetDuration() {
            return Duration;
        }
    }
}
