namespace Application {
    public interface RichTextSegment {
        int GetDuration();
    }
}