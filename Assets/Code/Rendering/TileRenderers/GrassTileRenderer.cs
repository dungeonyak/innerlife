namespace Application {
    public class GrassTileRenderer : TileRenderer {
        private GrassRenderer GrassRenderer = new GrassRenderer();
        private SpritePool SpritePool = Container.Resolve<SpritePool>();

        private IAnimation DarkGrassAnimation = DawnLike.Still("Objects/Floor", "DarkGrass_Center");

        public void Draw(Map map, Point position, int layer, int order) {
            var drawPosition = position * RenderUtils.TileSize;

            SpritePool.Draw(
                GetBaseAnimation(map, position).GetCurrentFrame(),
                drawPosition,
                layer,
                order
            );

            if (SelectAdornment(map, position) is IAnimation adornment) {
                SpritePool.Draw(
                    adornment.GetCurrentFrame(),
                    drawPosition,
                    layer,
                    order + 1
                );
            }
        }

        private IAnimation GetBaseAnimation(Map map, Point position) {
            if (map.MapType == MapType.Graveyard) {
                return DarkGrassAnimation;
            } else {
                return GrassRenderer.GetAnimation(map, position);
            }
        }

        private IAnimation SelectAdornment(Map map, Point position) {
            var randomness = map.GetRandomnessAt(position) * 2;

            return randomness < 8 ? DawnLike.Animation("Objects/Ground", 0) :
                randomness < 16 ? DawnLike.Animation("Objects/Ground", 1) :
                randomness < 24 ? DawnLike.Animation("Objects/Ground", 8) :
                randomness < 32 ? DawnLike.Animation("Objects/Ground", 9) :
                randomness < 36 ? DawnLike.Animation("Objects/Ground", 16) :
                randomness < 40 ? DawnLike.Animation("Objects/Ground", 17) :
                null;
        }
    }
}