namespace Application {
    public class WaterTileRenderer : TileRenderer {
        private PitRenderer WaterRenderer = new PitRenderer("BrickWater", tile => tile == Tile.Water);
        private SpritePool SpritePool = Container.Resolve<SpritePool>();

        public void Draw(Map map, Point position, int layer, int order) {
            var drawPosition = position * RenderUtils.TileSize;

            SpritePool.Draw(
                WaterRenderer.GetAnimation(map, position).GetCurrentFrame(),
                drawPosition,
                layer,
                order
            );
        }
    }
}