namespace Application {
    public class WallTileRenderer : TileRenderer {
        private WallRenderer DarkWallRenderer = new WallRenderer("Dark");
        private SpritePool SpritePool = Container.Resolve<SpritePool>();

        public void Draw(Map map, Point position, int layer, int order) {
            if (map.MapType == MapType.Nest) {
                var drawPosition = position * RenderUtils.TileSize;

                SpritePool.Draw(
                    DarkWallRenderer.GetAnimation(map, position).GetCurrentFrame(),
                    drawPosition,
                    layer,
                    order
                );
            } else {
                // don't bother
            }
        }
    }
}