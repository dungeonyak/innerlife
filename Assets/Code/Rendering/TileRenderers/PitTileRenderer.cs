namespace Application {
    public class PitTileRenderer : TileRenderer {
        private PitRenderer WaterRenderer = new PitRenderer("RockyPit", tile => tile == Tile.Pit || tile == Tile.Void);
        private SpritePool SpritePool = Container.Resolve<SpritePool>();

        public void Draw(Map map, Point position, int layer, int order) {
            var drawPosition = position * RenderUtils.TileSize;

            SpritePool.Draw(
                WaterRenderer.GetAnimation(map, position).GetCurrentFrame(),
                drawPosition,
                layer,
                order
            );
        }
    }
}