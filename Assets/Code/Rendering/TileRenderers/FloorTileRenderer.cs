using UnityEngine;

namespace Application {
    public class FloorTileRenderer : TileRenderer {
        private TileRenderer WoodFloorRenderer = new SimpleTileRenderer(DawnLike.Still("Objects/Floor", "WoodFloor_Center"));
        private TileRenderer DarkFloorRenderer = new SimpleTileRenderer(DawnLike.Still("Objects/Floor", "VeryDarkGrassCenter"));
        private TileRenderer DirtFloorRenderer = new SimpleTileRenderer(DawnLike.Still("Objects/Floor", "MediumSpeckledCenter"));

        private SpritePool SpritePool = Container.Resolve<SpritePool>();

        public void Draw(Map map, Point position, int layer, int order) {
            if (map.MapType == MapType.Nest) {
                DarkFloorRenderer.Draw(map, position, layer, order);
            } else if (map.MapType == MapType.Mountain) {
                DirtFloorRenderer.Draw(map, position, layer, order);
            } else {
                WoodFloorRenderer.Draw(map, position, layer, order);
            }

            if (SelectAdornment(map, position) is IAnimation adornment) {
                SpritePool.Draw(
                    adornment.GetCurrentFrame(),
                    position * RenderUtils.TileSize,
                    layer,
                    order + 2
                );
            }
        }

        private IAnimation SelectAdornment(Map map, Point position) {
            if (map.MapType == MapType.Nest || map.MapType == MapType.Mountain) {
                var randomness = map.GetRandomnessAt(position) * 2;

                return randomness < 12 ? DawnLike.Animation("Objects/Ground", 4) :
                    randomness < 24 ? DawnLike.Animation("Objects/Ground", 5) :
                    randomness < 36 ? DawnLike.Animation("Objects/Ground", 12) :
                    randomness < 48 ? DawnLike.Animation("Objects/Ground", 13) :
                    null;
            }

            return null;
        }
    }
}