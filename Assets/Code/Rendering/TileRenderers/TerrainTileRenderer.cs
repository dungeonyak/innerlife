using System;
using System.Linq;
using UnityEngine;

namespace Application {
    public class TerrainTileRenderer : TileRenderer {
        private TileRenderer WaterRenderer = new WaterTileRenderer();
        private TileRenderer GrassRenderer = new GrassTileRenderer();
        private TileRenderer FloorRenderer = new FloorTileRenderer();
        private TileRenderer ScalesRenderer = new SimpleTileRenderer(DawnLike.Still("Objects/Floor", "LightScalesCenter"));
        private TileRenderer WallRenderer = new WallTileRenderer();
        private TileRenderer PitRenderer = new PitTileRenderer();
        private TileRenderer NullRenderer = new NullTileRenderer();

        public void Draw(Map map, Point position, int layer, int order) {
            var tile = map.GetTileAt(position);
            GetTileRenderer(tile).Draw(map, position, layer, order);
        }

        private TileRenderer GetTileRenderer(Tile tile) {
            switch (tile) {
                case Tile.Floor: return FloorRenderer;
                case Tile.Grass: return GrassRenderer;
                case Tile.Water: return WaterRenderer;
                case Tile.Scales: return ScalesRenderer;
                case Tile.Wall: return WallRenderer;
                case Tile.Pit: return PitRenderer;
                case Tile.Void:
                    return NullRenderer;
            }

            throw new Exception($"Don't know how to render tile {Enum.GetName(typeof(Tile), tile)}");
        }
    }
}
