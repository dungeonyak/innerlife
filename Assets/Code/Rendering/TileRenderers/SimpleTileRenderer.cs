namespace Application {
    public class SimpleTileRenderer : TileRenderer {
        private readonly IAnimation Animation;
        private SpritePool SpritePool = Container.Resolve<SpritePool>();
        public SimpleTileRenderer(IAnimation animation) {
            Animation = animation;
        }

        public void Draw(Map map, Point position, int layer, int order) {
            var drawPosition = position * RenderUtils.TileSize;
            SpritePool.Draw(
                Animation.GetCurrentFrame(),
                drawPosition,
                layer,
                order
            );
        }
    }
}