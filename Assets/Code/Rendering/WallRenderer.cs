﻿using System;
using static Application.NeighborRequirement;

namespace Application {
    public class WallRenderer : NeighboredTileRenderer {
        public WallRenderer(string assetBase) {
            // top left
            DefineTile(
                DawnLike.Still("Objects/Wall", $"{assetBase}TopLeft"),
                new NeighborSpecification(
                    Any, Off, Any,
                    Off, /**/ Yes,
                    Any, Yes, Any
                )
            );

            // top/bottom
            DefineTile(
                DawnLike.Still("Objects/Wall", $"{assetBase}TopBottom"),
                new NeighborSpecification(
                    Any, Off, Any,
                    Yes, /**/ Yes,
                    Any, Off, Any
                )
            );

            // top right
            DefineTile(
                DawnLike.Still("Objects/Wall", $"{assetBase}TopRight"),
                new NeighborSpecification(
                    Any, Off, Any,
                    Yes, /**/ Off,
                    Any, Yes, Any
                )
            );

            // left/right
            DefineTile(
                DawnLike.Still("Objects/Wall", $"{assetBase}LeftRight"),
                new NeighborSpecification(
                    Any, Yes, Any,
                    Off, /**/ Off,
                    Any, Yes, Any
                )
            );

            // bottom left
            DefineTile(
                DawnLike.Still("Objects/Wall", $"{assetBase}BottomLeft"),
                new NeighborSpecification(
                    Any, Yes, Any,
                    Off, /**/ Yes,
                    Any, Off, Any
                )
            );
                
            // bottom right
            DefineTile(
                DawnLike.Still("Objects/Wall", $"{assetBase}BottomRight"),
                new NeighborSpecification(
                    Any, Yes, Any,
                    Yes, /**/ Off,
                    Any, Off, Any
                )
            );

            // bottom right
            DefineTile(
                DawnLike.Still("Objects/Wall", $"{assetBase}RightJunction"),
                new NeighborSpecification(
                    Any, Yes, Any,
                    Yes, /**/ Off,
                    Any, Yes, Any
                )
            );

            // bottom right
            DefineTile(
                DawnLike.Still("Objects/Wall", $"{assetBase}LeftJunction"),
                new NeighborSpecification(
                    Any, Yes, Any,
                    Off, /**/ Yes,
                    Any, Yes, Any
                )
            );

            // bottom right
            DefineTile(
                DawnLike.Still("Objects/Wall", $"{assetBase}TopJunction"),
                new NeighborSpecification(
                    Any, Off, Any,
                    Yes, /**/ Yes,
                    Any, Yes, Any
                )
            );

            // bottom right
            DefineTile(
                DawnLike.Still("Objects/Wall", $"{assetBase}BottomJunction"),
                new NeighborSpecification(
                    Any, Yes, Any,
                    Yes, /**/ Yes,
                    Any, Off, Any
                )
            );

            // bottom right
            DefineTile(
                DawnLike.Still("Objects/Wall", $"{assetBase}Center"),
                new NeighborSpecification(
                    Any, Off, Any,
                    Yes, /**/ Off,
                    Any, Yes, Any
                )
            );
            
            // bottom right
            DefineTile(
                DawnLike.Still("Objects/Wall", $"{assetBase}NarrowBottom"),
                new NeighborSpecification(
                    Any, Yes, Any,
                    Off, /**/ Off,
                    Any, Off, Any
                )
            );

            // bottom right
            DefineTile(
                DawnLike.Still("Objects/Wall", $"{assetBase}LeftRight"),
                new NeighborSpecification(
                    Any, Off, Any,
                    Off, /**/ Off,
                    Any, Yes, Any
                )
            );

            // bottom right
            DefineTile(
                DawnLike.Still("Objects/Wall", $"{assetBase}TopBottom"),
                new NeighborSpecification(
                    Any, Off, Any,
                    Yes, /**/ Off,
                    Any, Off, Any
                )
            );

            // bottom right
            DefineTile(
                DawnLike.Still("Objects/Wall", $"{assetBase}TopBottom"),
                new NeighborSpecification(
                    Any, Off, Any,
                    Off, /**/ Yes,
                    Any, Off, Any
                )
            );

            // bottom right
            DefineTile(
                DawnLike.Still("Objects/Wall", $"{assetBase}Center"),
                new NeighborSpecification(
                    Any, Off, Any,
                    Off, /**/ Off,
                    Any, Off, Any
                )
            );

            // bottom right
            DefineTile(
                DawnLike.Still("Objects/Wall", $"{assetBase}Center"),
                new NeighborSpecification(
                    Any, Yes, Any,
                    Yes, /**/ Yes,
                    Any, Yes, Any
                )
            );
        }

        protected override int GetBitfield(Map map, Point point) {
            var calculator = new MapTileNeighborCalculator(map, tile => tile == Tile.Wall || tile == Tile.Void);
            return calculator.GetBitfield(point);
        }
    }
}
