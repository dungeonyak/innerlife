﻿using System;
using static Application.NeighborRequirement;


namespace Application {
    public class PitRenderer : NeighboredTileRenderer {
        private Func<Tile, bool> CheckTile;

        public PitRenderer(string assetBase, Func<Tile, bool> checkTile) {
            CheckTile = checkTile;

            // top left corner
            DefineTile(
                DawnLike.Animation("Objects/Pit", $"{assetBase}_TopLeft"),
                new NeighborSpecification(
                    Any, Off, Any,
                    Off, /**/ Yes,
                    Any, Any, Any
                )
            );

            // top middle
            DefineTile(
                DawnLike.Animation("Objects/Pit", $"{assetBase}_Top"),
                new NeighborSpecification(
                    Any, Off, Any,
                    Yes, /**/ Yes,
                    Any, Any, Any
                )
            );

            // top right corner
            DefineTile(
                DawnLike.Animation("Objects/Pit", $"{assetBase}_TopRight"),
                new NeighborSpecification(
                    Any, Off, Any,
                    Yes, /**/ Off,
                    Any, Any, Any
                )
            );

            // left side
            DefineTile(
                DawnLike.Animation("Objects/Pit", $"{assetBase}_Left"),
                new NeighborSpecification(
                    Any, Yes, Any,
                    Off, /**/ Yes,
                    Any, Any, Any
                )
            );

            // right side
            DefineTile(
                DawnLike.Animation("Objects/Pit", $"{assetBase}_Right"),
                new NeighborSpecification(
                    Any, Yes, Any,
                    Yes, /**/ Off,
                    Any, Any, Any
                )
            );

            // middle (all water)
            DefineTile(
                DawnLike.Animation("Objects/Pit", $"{assetBase}_Middle"),
                new NeighborSpecification(
                    Yes, Yes, Yes,
                    Yes, /**/ Yes,
                    Any, Any, Any
                )
            );

            // thin top
            DefineTile(
                DawnLike.Animation("Objects/Pit", $"{assetBase}_TopNarrow"),
                new NeighborSpecification(
                    Any, Off, Any,
                    Off, /**/ Off,
                    Any, Any, Any
                )
            );

            // thin
            DefineTile(
                DawnLike.Animation("Objects/Pit", $"{assetBase}_Narrow"),
                new NeighborSpecification(
                    Any, Yes, Any,
                    Off, /**/ Off,
                    Any, Any, Any
                )
            );

            // thinleft
            DefineTile(
                DawnLike.Animation("Objects/Pit", $"{assetBase}_ReverseTopLeft"),
                new NeighborSpecification(
                    Off, Yes, Any,
                    Yes, /**/ Yes,
                    Any, Any, Any
                )
            );

            // thinright
            DefineTile(
                DawnLike.Animation("Objects/Pit", $"{assetBase}_ReverseTopRight"),
                new NeighborSpecification(
                    Any, Yes, Off,
                    Yes, /**/ Yes,
                    Any, Any, Any
                )
            );

            // thinlet
            DefineTile(
                DawnLike.Animation("Objects/Pit", $"{assetBase}_ReverseMiddle"),
                new NeighborSpecification(
                    Off, Yes, Off,
                    Yes, /**/ Yes,
                    Any, Any, Any
                )
            );


            // thinledit
            DefineTile(
                DawnLike.Animation("Objects/Pit", $"{assetBase}_TopLeft"),
                new NeighborSpecification(
                    Off, Yes, Any,
                    Yes, /**/ Off,
                    Any, Any, Any
                )
            );

            // thinredit
            DefineTile(
                DawnLike.Animation("Objects/Pit", $"{assetBase}_TopRight"),
                new NeighborSpecification(
                    Any, Yes, Off,
                    Off, /**/ Yes,
                    Any, Any, Any
                )
            );

        }

        protected override int GetBitfield(Map map, Point point) {
            var calculator = new MapTileNeighborCalculator(map, CheckTile);
            return calculator.GetBitfield(point);
        }
    }
}
