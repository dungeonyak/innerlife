﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Application {
    public class Animation : IAnimation {
        private IEnumerable<Sprite> Frames;
        private Sprite[] sprite;
        private int v;
        public readonly double FramesPerSecond;

        public Animation(IEnumerable<Sprite> frames, double framesPerSecond) {
            Frames = frames;
            FramesPerSecond = framesPerSecond;
        }

        public Sprite GetCurrentFrame() {
            var elapsedSeconds = Time.realtimeSinceStartup;
            var frameIndex = (int)(elapsedSeconds * FramesPerSecond) % Frames.Count();
            return Frames.ElementAt(frameIndex);
        }
    }
}
