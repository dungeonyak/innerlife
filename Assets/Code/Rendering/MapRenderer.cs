﻿using System;
using System.Linq;
using UnityEngine;

namespace Application {
    public class MapRenderer {
        [Inject] public EntityFinder EntityFinder;
        [Inject] public SpritePool SpritePool;

        private TileRenderer TileRenderer = new TerrainTileRenderer();

        public void Render(
            Map map,
            GameObject camera,
            Entity centerEntity = null,
            Point? direction = null,
            bool isOuter = false
        ) {
            var fade = isOuter ? 0.5f : 1.0f;
            DrawTiles(map, camera, fade: fade);
            DrawEntities(map, camera, fade: fade);
        }

        private void DrawTiles(Map map, GameObject camera, float fade) {
            var cullingMask = camera.GetComponent<Camera>().cullingMask;
            var layer = (int)Math.Log(cullingMask, 2);

            var cameraRect = Cameras.GetViewRectangle(camera);

            var minX = (int) Math.Max(0, cameraRect.xMin / RenderUtils.TileSize);
            var minY = (int) Math.Max(0, cameraRect.yMin / RenderUtils.TileSize);
            var maxX = (int) Math.Min(map.Width, cameraRect.xMax / RenderUtils.TileSize + 2);
            var maxY = (int) Math.Min(map.Height, cameraRect.yMax / RenderUtils.TileSize + 2);

            for (var x = minX; x < maxX; x++) {
                for (var y = minY; y < maxY; y++) {
                    var position = new Point(x, y);

                    var order = -maxY * RenderUtils.TileSize * 10 + 9 + 1000;
                    TileRenderer.Draw(map, position, layer, order);
                }
            }
        }

        private void DrawEntities(Map map, GameObject camera, float fade) {
    
            foreach (var entity in map.PlacedEntities) {
                var worldPosition = entity.Get<PlacementComponent>().Position;

                if (IsOnCamera(map, camera, worldPosition)) {
                    DrawEntity(map, entity, camera, fade);
                }
            }
            
            var leavers = EntityFinder.WhereHas<TravelTweenComponent>()
                .Where(e => e.Get<TravelTweenComponent>().OldPlacement.Map == map);

            foreach (var entity in leavers) {
                var travelTween = entity.Get<TravelTweenComponent>();

                if (IsOnCamera(map, camera, travelTween.OldPlacement.Position)) {
                    DrawEntity(map, entity, camera, fade);
                }
            }
        }

        private bool IsOnCamera(Map map, GameObject camera, Point worldPosition) {
            var viewRect = Cameras.GetViewRectangle(camera);
            var worldViewRect = new Rect(
                viewRect.position / RenderUtils.TileSize,
                viewRect.size / RenderUtils.TileSize
            );
            
            return worldPosition.Y >= worldViewRect.yMin - 2 &&
                worldPosition.Y <= worldViewRect.yMax + 2 &&
                worldPosition.X >= worldViewRect.xMin - 2 &&
                worldPosition.X <= worldViewRect.xMax + 2;
        }

        private void DrawEntity(Map map, Entity entity, GameObject camera, float fade) {
            var cullingMask = camera.GetComponent<Camera>().cullingMask;
            var layer = (int)Math.Log(cullingMask, 2);

            var worldPosition = entity.Get<PlacementComponent>().Position;

            var isMouseOver = IsMouseOver(worldPosition, camera);

            if (entity.MaybeGet<BodySpriteComponent>() is BodySpriteComponent bodySprite) {
                var flatPosition = GetFlatPosition(entity, map);
                var opacity = GetOpacity(entity, map);

                if (flatPosition != null) {
                    var order = -flatPosition.Value.Y + 5;

                    if (entity.Has<HnauComponent>()) {
                        order += 1;
                    }

                    var inWater = ShouldShowInWater(entity);

                    var animation = Utils.IsDead(entity)
                        ? DawnLike.Animation("Characters/Undead", 30)
                        : bodySprite.Animation;
                        
                    var shakeOffset = GetShakeOffset(entity);

                    var showCrown = entity.Has<CrownComponent>()
                        && Utils.GetAvatar().HasAbility(Ability.Compass)
                        && !Utils.GetAvatar().Has<VictoryComponent>();
                    
                    if (inWater) {
                        SpritePool.Draw(
                            Sprites.Load("shadow"),
                            flatPosition.Value,
                            layer: layer,
                            order: order - 1,
                            color: DawnLike.White,
                            fade: fade,
                            alpha: (float) opacity
                        );
                    } else if (ShouldShowShadow(entity)) {
                        SpritePool.Draw(
                            Sprites.Load("shadow"),
                            flatPosition.Value,
                            layer: layer,
                            order: order - 1,
                            color: DawnLike.Black,
                            fade: fade,
                            alpha: (float) opacity
                        );
                    }

                    SpritePool.Draw(
                        animation.GetCurrentFrame(),
                        flatPosition.Value
                            + (inWater ? Point.Vertical(2) : Point.Vertical(GetHopHeight(entity)))
                            + shakeOffset,
                        layer: layer,
                        order: order,
                        flipped: entity.MaybeGet<FacingComponent>() is FacingComponent facingComponent
                            && facingComponent.Facing == Facing.Right,
                        outlineColor: isMouseOver
                            ? DawnLike.White
                            : (showCrown ? GetCrownOutlineColor() : (Color?) null),
                        sourceRect: inWater
                            ? new Rect(0, 8, 16, 8)
                            : (Rect?) null,
                        fade: fade,
                        alpha: (float) opacity
                    );

                    if (entity.Has<WebbedComponent>()) {
                        var webAnimation = DawnLike.Animation("Objects/Decor", "SpiderWeb");

                        SpritePool.Draw(
                            webAnimation.GetCurrentFrame(),
                            flatPosition.Value,
                            layer: layer,
                            order: order + 1,
                            alpha: 0.8f
                        );
                    }

                    if (showCrown) {
                        DrawDownArrow(flatPosition.Value, layer, order);
                    }
                }
            } else if (entity.MaybeGet<DoorComponent>() is DoorComponent door) {
                var isLocked = map.Holder.MaybeGet<DoorLockComponent>() is DoorLockComponent doorLock && doorLock.IsLocked;

                var doorAnimation = GetDoorAnimation(door.Direction, isLocked);

                var flatPosition = entity.Get<PlacementComponent>().Position * RenderUtils.TileSize;    
                var order = -flatPosition.Y + 5;

                var showCrown = !map.Holder.Has<CrownComponent>()
                    && !map.Holder.IsAvatar()
                    && Utils.GetAvatar().HasAbility(Ability.Compass)
                    && !Utils.GetAvatar().Has<VictoryComponent>();

                SpritePool.Draw(
                    doorAnimation.GetCurrentFrame(),
                    flatPosition,
                    layer: layer,
                    order: order - 2,
                    fade: fade,
                    outlineColor: showCrown ? GetCrownOutlineColor() : (Color?) null
                );

                if (showCrown) {
                    DrawUpArrow(flatPosition, layer, order - 2);
                }
            }

            if (isMouseOver) {
                Container.Resolve<Inspector>().Inspect(entity);
            }
        }

        private void DrawUpArrow(Point position, int layer, int order) {
            DrawArrow(position, Characters.ArrowUp, layer, order);
        }

        private void DrawDownArrow(Point position, int layer, int order) {
            DrawArrow(position, Characters.ArrowDown, layer, order);
        }

        private void DrawArrow(Point position, int arrowCode, int layer, int order) {
            var font = Container.Resolve<SpriteFont>();

            var arrowText = new RichText($"<gold><char code=\"{arrowCode}\" /></gold>");
            var arrowPos = position
                    + Point.Vertical(RenderUtils.TileSize)
                    + Point.Horizontal(RenderUtils.TileSize / 2)
                    - Point.Horizontal(font.Width / 2)
                    + Point.Vertical(1);

            arrowPos += Point.Vertical((int) (4 * Math.Abs(Math.Sin(Time.realtimeSinceStartup * 4))));

            RichTextWriter.Write(
                font,
                arrowText,
                arrowPos,
                layer: layer,
                order: order - 2,
                anchor: Anchor.BottomLeft
            );
        }

        private Color GetCrownOutlineColor() {
            return DawnLike.Yellow.Darken(0.75f + 0.25f * (float) Math.Sin(Time.realtimeSinceStartup * 8));
        }

        private IAnimation GetDoorAnimation(Point direction, bool isLocked) {
            return isLocked
                ? DawnLike.Still("Objects/Door0", 2)
                : DawnLike.Still("Objects/Door0", 0);
        }

        private bool ShouldShowInWater(Entity entity) {
            var p = entity.Get<PlacementComponent>();
            var map = p.Map;
            var position = p.Position;

            if (Utils.IsFlying(entity)) {
                return false;
            } else if (entity.MaybeGet<WalkTweenComponent>() is WalkTweenComponent walkTween) {
                var originPos = walkTween.WorldOrigin;

                if (map.InBounds(originPos)) {
                    var originTile = map.GetTileAt(originPos);
                    var destinationTile = map.GetTileAt(position);

                    return originTile == Tile.Water && destinationTile == Tile.Water;
                }

                return false;
            } else {
                var tile = map.GetTileAt(position);
                
                return tile == Tile.Water;
            }
        }

        private bool ShouldShowShadow(Entity entity) {
            return entity.Has<HnauComponent>()
                || entity.Has<ItemComponent>()
                || entity.Has<WalkTweenComponent>();
        }

        private bool IsMouseOver(Point position, GameObject camera) {
            if (Cameras.GetMousePosition(camera) is Point mousePosition) {
                var worldPosition = mousePosition / RenderUtils.TileSize;
                return worldPosition == position;
            }

            return false;
        }

        private int GetHopHeight(Entity entity) {
            if (Utils.IsFlying(entity)) {
                return 6;
            } else if (entity.MaybeGet<SpeciesComponent>()?.Species == Species.Slime) { // TODO: do better
                return 0;
            } else if (entity.MaybeGet<WalkTweenComponent>() is WalkTweenComponent walkTween) {
                if (!ShouldShowInWater(entity)) {
                    var maxHeight = 6;
                    return maxHeight - (int) (maxHeight * Easings.QuadraticEaseIn(2 * Math.Abs(walkTween.Progress - 0.5)));
                }
            } else if (entity.MaybeGet<PickedUpTweenComponent>() is PickedUpTweenComponent pickedUpTween) {
                var maxHeight = 24;
                return maxHeight - (int) (maxHeight * Easings.Linear(pickedUpTween.Progress));
            }

            return 0;
        }

        private Point GetShakeOffset(Entity entity) {
            if (entity.Has<ShakeTweenComponent>()) {
                var random = Container.Resolve<Randomizer>();
                return new Point(random.Range(-1, 2), random.Range(-1, 2));
            }

            return Point.Zero;
        }

        public double GetOpacity(Entity entity, Map drawMap) {
            var entityMap = entity.Get<PlacementComponent>().Map;

            if (drawMap != entityMap) {
                if (entity.MaybeGet<TravelTweenComponent>() is TravelTweenComponent travelTween
                    && travelTween.OldPlacement.Map == drawMap) {
                    return Math.Max(0, 1.0 - 2 * Easings.Linear(travelTween.Progress));
                } else {
                    throw new Exception("Shouldn't be drawing this entity");
                }
            } else {
                if (entity.MaybeGet<TravelTweenComponent>() is TravelTweenComponent travelTween) {
                    return Math.Min(1, Easings.Linear(2 * travelTween.Progress - 0.5));
                }
            }

            return 1.0;
        }

        public Point? GetFlatPosition(Entity entity, Map drawMap) {
            var entityMap = entity.Get<PlacementComponent>().Map;

            if (drawMap != entityMap) {
                if (entity.MaybeGet<TravelTweenComponent>() is TravelTweenComponent travelTween
                    && travelTween.OldPlacement.Map == drawMap) {
                    if (travelTween.Progress > 0.5) {
                        return null;
                    } else {
                        var originalPosition = travelTween.OldPlacement.Position * RenderUtils.TileSize;
                        var gatePosition = GetFlatPosition(travelTween.EmbarkGate);

                        return originalPosition + (gatePosition - originalPosition) * Easings.Linear(2 * travelTween.Progress);
                    }
                } else {
                    throw new Exception("Shouldn't be drawing this entity");
                }
            } else {
                return GetFlatPosition(entity);
            }
        }

        public Point GetBasePosition(Entity entity) {
            var currentPosition = entity.Pos();

            if (entity.MaybeGet<TravelTweenComponent>() is TravelTweenComponent travelTween) {
                var gatePosition = GetFlatPosition(travelTween.DisembarkGate);
                var newPosition = currentPosition * RenderUtils.TileSize;

                var origin = gatePosition + (gatePosition - newPosition);
                return origin + (newPosition - origin) * Math.Min(1, Easings.Linear(travelTween.Progress));
            } else if (entity.MaybeGet<WalkTweenComponent>() is WalkTweenComponent walkTween) {
                return (
                    walkTween.WorldOrigin * RenderUtils.TileSize +
                    (currentPosition - walkTween.WorldOrigin) * RenderUtils.TileSize * walkTween.Progress
                );
            } else {
                return currentPosition * RenderUtils.TileSize;
            }
        }

        public Point GetFlatPosition(Entity entity) {
            var basePosition = GetBasePosition(entity);
            
            if (entity.MaybeGet<AttackTweenComponent>() is AttackTweenComponent tween) {
                var magnitude = 8;
                var pivot = 0.35;
                var offset = tween.Progress < pivot
                    ? tween.Direction * magnitude * Easings.Linear(tween.Progress * 1/pivot)
                    : tween.Direction * magnitude * (1 - Easings.QuadraticEaseOut((tween.Progress - pivot) * 1/(1 - pivot)));

                return basePosition + offset;
            } else {
                return basePosition;
            }
        }        
    }
}
