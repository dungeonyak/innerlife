﻿using System;
using UnityEngine;

namespace Application {
    public class Still : IAnimation {
        private Sprite Sprite;

        public Still(Sprite sprite) {
            Sprite = sprite;
        }

        public Sprite GetCurrentFrame() {
            return Sprite;
        }
    }
}
