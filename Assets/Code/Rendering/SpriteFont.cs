﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Application {
    public class SpriteFont {
        private readonly IEnumerable<Sprite> Sprites;
        public readonly int Width;
        public readonly int Height;

        private SpritePool SpritePool;

        public SpriteFont(IEnumerable<Sprite> sprites) {
            Sprites = sprites;
            Width = (int) sprites.First().rect.width;
            Height = (int) sprites.First().rect.height;
            SpritePool = Container.Resolve<SpritePool>();
        }

        public void Write(
            string text,
            Point position,
            int layer,
            Color? color = null,
            int? order = null) {
            for (int i = 0; i < text.Length; i++) {
                char character = text[i];
                var charPosition = position + Point.Horizontal(i * Width);
                Write(character, charPosition, color: color, layer: layer, order: order);
            }
        }

        public void Write(
            char character,
            Point position,
            int layer,
            Color? color = null,
            int? order = null) {
            var sprite = GetCharacterSprite(character);
            SpritePool.Draw(sprite, position, color: color, layer: layer, order: order);
        }
    
        public int Measure(string text) {
            return Width * text.Length;
        }

        private Sprite GetCharacterSprite(char character) {
            return Sprites.ElementAt(character);
        }
    }
}
