using System;
using System.Linq;
using UnityEngine;

namespace Application {
    public class CameraPositioner {
        private int ViewRange = 3 * RenderUtils.TileSize;
        private int Scale = 2;

        [Inject] public EntityFinder EntityFinder;

        public void PositionCamera(Map map, GameObject camera, Entity centerEntity = null, Point? direction = null) {
            if (direction.HasValue) {
                PositionSideCamera(map, camera, centerEntity, direction.Value);
            } else if (centerEntity != null) {
                var pos = Container.Resolve<MapRenderer>().GetBasePosition(centerEntity);
                camera.transform.position = new Vector3(
                    (int) (pos.X + RenderUtils.TileSize / 2),
                    (int) (pos.Y + RenderUtils.TileSize / 2 + RenderUtils.BodyOffset),
                    camera.transform.position.z
                );
            } else {
                camera.transform.position = new Vector3(
                    (int) (RenderUtils.TileSize * map.Width / 2),
                    (int) (RenderUtils.TileSize * map.Height / 2),
                    camera.transform.position.z
                );
            }
        }

        public void PositionSideCamera(Map map, GameObject camera, Entity centerEntity, Point direction) {
            var avatar = EntityFinder.WhereHas<PlayerControlledComponent>().First();
            var mainMap = avatar.Get<PlacementComponent>().Map;
            
            var mainCameraScreenRect = GetMainCameraScreenRect();

            var trueRect = new Rect(
                GetBottomLeft(direction).ToVector(),
                GetDimensions(direction).ToVector()
            );

            var clippedRectPos = new Vector2(
                Math.Max(trueRect.xMin, mainCameraScreenRect.xMin),
                Math.Max(trueRect.yMin, mainCameraScreenRect.yMin)
            );

            var tentativeWidth = trueRect.width - (clippedRectPos.x - trueRect.xMin);
            var tentativeHeight = trueRect.height - (clippedRectPos.y - trueRect.yMin);

            var clippedRectDimensions = new Vector2(
                Math.Max(0, Math.Min(tentativeWidth, mainCameraScreenRect.xMax - clippedRectPos.x)),
                Math.Max(0, Math.Min(tentativeHeight, mainCameraScreenRect.yMax - clippedRectPos.y))
            );

            var clippedRect = new Rect(clippedRectPos, clippedRectDimensions);

            Cameras.Configure(
                camera,
                dimensions: clippedRectDimensions.ToPoint() / Scale,
                position: clippedRectPos.ToPoint(),
                scale: Scale
            );

            var center = Container.Resolve<MapRenderer>().GetFlatPosition(centerEntity)
                //+ Point.Vertical(-RenderUtils.BodyOffset)
                ;

            //Debug.Log($"{center.X % 16}, {center.Y % 16}");

            camera.transform.position = new Vector3(
                (int) (
                    center.X
                    + RenderUtils.TileSize / 2
                    + direction.X * clippedRect.width / 2 / Scale
                    + direction.X * RenderUtils.TileSize / 2
                ),
                (int) (
                    center.Y
                    + RenderUtils.TileSize / 2
                    + direction.Y * clippedRect.height / 2 / Scale
                    + direction.Y * RenderUtils.TileSize / 2
                ),
                camera.transform.position.z
            );

            /*
            var doorPosition = map.PlacedEntities
                .Where(e => e.Has<DoorComponent>())
                .Where(e => e.Get<DoorComponent>().Direction == direction)
                .FirstOrDefault();

            if (direction.Y != 0) {
                basePosition += new Vector3(
                    avatar.Get<PlacementComponent>().Position.X - basePosition.x,
                    0,
                    0
                ) * RenderUtils.TileSize;
            } else if (direction.X != 0) {
                basePosition += new Vector3(
                    0,
                    avatar.Get<PlacementComponent>().Position.Y - basePosition.y,
                    0
                ) * RenderUtils.TileSize;
            }
            */

/*
            Debug.Log($"Side: {direction}");
            Debug.Log($"True: {trueRect.position}, {trueRect.size}");
            Debug.Log($"Clipped: {clippedRect.position}, {clippedRect.size}");
*/
            camera.transform.position += new Vector3(
                (int) ((clippedRect.xMin - trueRect.xMin) / Scale),
                (int) ((clippedRect.yMin - trueRect.yMin) / Scale),
                0
            );

            camera.transform.position += new Vector3(
                (int) ((clippedRect.width - trueRect.width) / Scale) / 1 * -direction.X,
                (int) ((clippedRect.height - trueRect.height) / Scale) / 1 * -direction.Y,
                0
            );
        }

        private Rect GetMainCameraScreenRect() {
            var cameraComponent = Cameras.MainMap.GetComponent<Camera>();
            return cameraComponent.pixelRect;
        }

        private Point GetBottomLeft(Point direction) {
            var mainMapRect = GetMainMapScreenRect();

            if (direction == Point.Left) {
                return new Point(
                    (int) (mainMapRect.xMin - ViewRange * 2),
                    (int) mainMapRect.yMin
                );
            } else if (direction == Point.Right) {
                return new Point(
                    (int) mainMapRect.xMax,
                    (int) mainMapRect.yMin
                );
            } else if (direction == Point.Up) {
                return new Point(
                    (int) mainMapRect.xMin,
                    (int) mainMapRect.yMax
                );
            } else if (direction == Point.Down) {
                return new Point(
                    (int) mainMapRect.xMin,
                    (int) mainMapRect.yMin - ViewRange * 2
                );
            }

            throw new Exception("bah");
        }

        private Rect GetMainMapScreenRect() {
            var mainMap = GetMainMap();
            var mainMapOffset = Cameras.GetViewRectangle(Cameras.MainMap).position * 2;

            return new Rect(
                GetMainCameraScreenRect().position - mainMapOffset,
                new Vector2(
                    mainMap.Width * RenderUtils.TileSize * 2,
                    mainMap.Height * RenderUtils.TileSize * 2
                )
            );
        }

        private Map GetMainMap() {
            var avatar = EntityFinder.WhereHas<PlayerControlledComponent>().First();
            return avatar.Get<PlacementComponent>().Map;
        }

        private Point GetDimensions(Point direction) {
            var avatar = EntityFinder.WhereHas<PlayerControlledComponent>().First();
            var mainMap = avatar.Get<PlacementComponent>().Map;

            if (direction.X != 0) {
                return new Point(ViewRange * Scale, mainMap.Height * RenderUtils.TileSize * Scale);
            } else {
                return new Point(mainMap.Width * RenderUtils.TileSize * Scale, ViewRange * Scale);
            }
        }
    }
}