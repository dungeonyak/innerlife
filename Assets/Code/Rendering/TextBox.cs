using UnityEngine;

namespace Application {
    public class TextBox {
        public readonly SpriteFont Font;
        public readonly RichText Text;
        public readonly int Padding = 8;
    
        public Point Size => new Point(Width, Height);
        private int TextWidth => RichTextWriter.MeasureWidth(Text, Font, FixedTextWidth);
        private int? FixedTextWidth => FixedWidth - Padding * 2;
        private int TextHeight => RichTextWriter.MeasureHeight(Text, Font, FixedTextWidth, TextSpacing);
        public int Width => FixedWidth ?? (TextWidth + Padding * 2);
        public int Height => TextHeight + Padding * 2;
        private readonly int TextSpacing = 2;

        public readonly int? FixedWidth;
    
        public TextBox(SpriteFont font, RichText text) {
            Font = font;
            Text = text;
        }

        public TextBox(SpriteFont font, RichText text, int fixedWidth) {
            Font = font;
            Text = text;
            FixedWidth = fixedWidth;
        }

        public void Draw(Point position, int layer, int order = 1) {
            Container.Resolve<SpritePool>().Draw(
                Sprites.Load("dialogue"),
                position,
                sliceSize: Size,
                layer: layer,
                order: order
            );

            var textPosition = position + new Point(
                Padding,
                Height - Padding
            );

            RichTextWriter.Write(
                Font,
                Text,
                textPosition,
                layer: LayerMask.NameToLayer("UI"),
                order: order + 1,
                wrapWidth: FixedTextWidth,
                spacing: TextSpacing
            );
        }
    }
}