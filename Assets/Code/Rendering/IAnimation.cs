﻿using System;
using UnityEngine;

namespace Application {
    public interface IAnimation {
        Sprite GetCurrentFrame();
    }
}
