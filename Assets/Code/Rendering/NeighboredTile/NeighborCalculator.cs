﻿using System;
namespace Application {
    public abstract class NeighborCalculator {
        private Point[] Offsets = new Point[] {
            new Point(-1, 1),
            new Point(0, 1),
            new Point(1, 1),
            new Point(-1, 0),
            new Point(1, 0),
            new Point(-1, -1),
            new Point(0, -1),
            new Point(1, -1),
        };

        public int GetBitfield(Point point) {
            var bitfield = 0;
            var pos = 0;

            foreach (var offset in Offsets) {
                var neighbor = point + offset;
                if (Check(neighbor)) {
                    bitfield |= (1 << pos);
                }

                pos++;
            }

            return bitfield;
        }

        protected abstract bool Check(Point point);
    }
}
