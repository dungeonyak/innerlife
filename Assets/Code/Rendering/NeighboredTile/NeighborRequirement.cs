﻿using System;

namespace Application {
    public enum NeighborRequirement {
        Any,
        Yes,
        Off,
    }
}
