﻿using System;
using System.Collections.Generic;
using UnityEngine;
using static Application.NeighborRequirement;

namespace Application {
    public abstract class NeighboredTileRenderer {
        private readonly Dictionary<int, IAnimation> Mapping = new Dictionary<int, IAnimation>();

        public void DefineTile(IAnimation animation, NeighborSpecification spec) {
            foreach (var bitfield in spec.Bitfields) {
                Mapping[bitfield] = animation;
            }
        }

        public IAnimation GetAnimation(Map map, Point point) {
            var bitfield = GetBitfield(map, point);

            if (!Mapping.ContainsKey(bitfield)) {
                Debug.Log("waaaagh " + Convert.ToString(bitfield, 2));
            }
            return Mapping[bitfield];
        }

        protected abstract int GetBitfield(Map map, Point point);
    }
}
