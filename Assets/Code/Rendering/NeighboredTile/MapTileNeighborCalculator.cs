﻿using System;
namespace Application {
    public class MapTileNeighborCalculator : NeighborCalculator {
        private readonly Map Map;
        private readonly Func<Tile, bool> CheckTile;

        public MapTileNeighborCalculator(Map map, Func<Tile, bool> checkTile) {
            Map = map;
            CheckTile = checkTile;
        }

        protected override bool Check(Point point) {
            return Map.InBounds(point) ? CheckTile(Map.GetTileAt(point)) : true;
        }
    }
}
