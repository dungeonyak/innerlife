﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Application {
    public class NeighborSpecification {
        public readonly int[] Bitfields;

        public NeighborSpecification(params NeighborRequirement[] neighborRequirements) {
            Bitfields = CreateBitfields(neighborRequirements).ToArray();
        }

        private IEnumerable<int> CreateBitfields(NeighborRequirement[] requirements, int index = 0) {
            if (index >= requirements.Length) {
                return new int[] { 0 };
            }

            var req = requirements[index];
            var subfields = CreateBitfields(requirements, index + 1);
            var bit = 1 << index;

            if (req == NeighborRequirement.Any) {
                var with = subfields.Select(sub => sub | bit);
                var without = subfields.Select(sub => sub & ~bit);

                return with.Union(without);
            } else if (req == NeighborRequirement.Yes) {
                return subfields.Select(sub => sub | bit);
            } else {
                return subfields.Select(sub => sub & ~bit);
            }
        }
    }
}
