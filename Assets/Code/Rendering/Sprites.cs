﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace Application {
    public static class Sprites {
        private static Dictionary<string, Sprite> SingleSprites = new Dictionary<string, Sprite>();
        private static Dictionary<string, Sprite[]> Sheets = new Dictionary<string, Sprite[]>();

        public static Sprite Load(string path) {
            return SingleSprites.SetDefault(path, () => {
                var sprite = Resources.Load<Sprite>("Sprites/" + path);
                PrepareTexture(sprite.texture);
                return sprite;
            });
        }

        public static Sprite LoadFromSheet(string path, int index) {
            return LoadSheet(path)[index];
        }

        public static Sprite LoadFromSheet(string path, string name) {
            var sprite = LoadSheet(path).FirstOrDefault(s => s.name == name);

            if (sprite == null) {
                throw new Exception($"Can't find sprite with name {name}");
            }

            return sprite;
        }

        public static Sprite[] LoadSheet(string path) {
            return Sheets.SetDefault(path, () => {
                var sprites = Resources.LoadAll<Sprite>("Sprites/" + path);
                PrepareTexture(sprites[0].texture);
                return sprites;
            });
        }

        private static void PrepareTexture(Texture2D texture) {
            texture.filterMode = FilterMode.Point;
        }
    /*
        public static Sprite Load(string path) {
            var fullPath = path + "/" + path;
            return SingleSprites.SetDefault(fullPath, () => LoadFromPrefab(fullPath));
        }

        public static Sprite LoadFromSheet(string path, int index) {
            var fullPath = path + "/" + Path.GetFileName(path) + "_" + index;
        
            return SingleSprites.SetDefault(fullPath, () => LoadFromPrefab(fullPath));
        }

        public static Sprite LoadFromSheet(string path, string name) {
            var fullPath = path + "/" + name;
        
            return SingleSprites.SetDefault(fullPath, () => LoadFromPrefab(fullPath));
        }

        private static Sprite LoadFromPrefab(string path) {
            var prefabPath = "AutoPrefabs/" + path;
            var prefab = Resources.Load(prefabPath);
            var obj = (GameObject) GameObject.Instantiate(prefab);
            var sprite = obj.GetComponent<SpriteRenderer>().sprite;
            GameObject.DestroyImmediate(obj);
            return sprite;
        }

        private static void PrepareTexture(Texture2D texture) {
            texture.filterMode = FilterMode.Point;
        }
*/
    }
}
