﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using UnityEngine;
namespace Application {
    public static class Container {
        private static Dictionary<Type, object> Bindings = new Dictionary<Type, object>();

        public static void Register<T>(T value) {
            Bindings[typeof(T)] = value;
        }

        public static T Resolve<T>() {
            return (T)Resolve(typeof(T));
        }

        public static object Resolve(Type type) {
            return Bindings.SetDefault(type, () => CreateInstance(type));
        }

        public static void Clear() {
            Bindings.Clear();
        }

        private static object CreateInstance(Type type) {
            var obj = FormatterServices.GetUninitializedObject(type);

            foreach (var field in GetInjectableFields(type)) {
                var injectedType = field.FieldType;
                var resolvedDependency = Resolve(injectedType);
                field.SetValue(obj, resolvedDependency);
            }

            var constructor = obj.GetType().GetConstructors().FirstOrDefault();

            if (constructor != null) {
                var constructorParams = constructor.GetParameters()
                    .Select(param => Resolve(param.ParameterType));
                constructor.Invoke(obj, constructorParams.ToArray());
            }

            return obj;
        }

        private static IEnumerable<FieldInfo> GetInjectableFields(Type type) {
            var fields = type.GetFields(
                BindingFlags.Instance |
                BindingFlags.Public |
                BindingFlags.NonPublic
            );

            return fields.Where(field => field.GetCustomAttributes<InjectAttribute>(true).Count() > 0);

        }
    }
}
