using System;

namespace Application {
    public class EntityBuilder {
        [Inject] public Randomizer Random;
        public Entity Animal(Species species) {
            var guy = new Entity();

            guy.Name = species.Name;

            guy.Add(new HnauComponent());
            guy.Add(new BodySpriteComponent(GetBodyAnimation(species)));
            guy.Add(new SpeciesComponent(species));

            if (species.SeekFood) {
                guy.Add(new FoodSeekingComponent());
            }

            if (species == Species.Dog) {
                guy.Add(new ScaryComponent());
            }

            guy.Add(new DietComponent(species.Diet));

            guy.Add(new TimeOnMapComponent());
            guy.Add(new FacingComponent());

            if (species.Scarable) {
                guy.Add(new ScarableComponent());
            }

            var intrinsics = guy.Add(new IntrinsicAbilityComponent());

            foreach (var ability in species.Intrinsics) {
                intrinsics.Abilities.Add(ability);
            }

            Utils.ApplyIntrinsics(guy);

            return guy;
        }

        public Entity Food(FoodType foodType = null) {
            var food = new Entity();

            food.Add(new ItemComponent());

            var type = foodType ?? Random.Choice(new[] {
                FoodType.Apple,
                FoodType.Orange,
                FoodType.Tomato,
                FoodType.Gourd,
                FoodType.Pepper,
                FoodType.Lettuce,
                FoodType.Radish,
                FoodType.Carrot,
                FoodType.Banana,
                FoodType.Meat,
            });

            food.Add(new FoodComponent(type));

            if (type == FoodType.Victory) {
                food.Add(new CrownComponent());
                food.Add(new VictoryFoodComponent());
            }

            food.Name = type.Name.ToUpper();
            food.Add(new BodySpriteComponent(type.Animation));

            return food;
        }

        public Entity Decor(string name, IAnimation animation, bool solid = true) {
            var decor = new Entity();

            decor.Name = name;

            decor.Add(new BodySpriteComponent(animation));

            if (solid) {
                decor.Add(new SolidDecorComponent());
            }
            
            return decor;
        }

        private IAnimation GetBodyAnimation(Species species) {
            return species.Animation;
        }
    }
}