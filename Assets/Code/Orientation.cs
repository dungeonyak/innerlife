namespace Application
{
    public class Orientation
    {
        public Orientation Opposite;

        public static Orientation Horizontal;
        public static Orientation Vertical;

        static Orientation()
        {
            Horizontal = new Orientation();
            Vertical = new Orientation();

            Horizontal.Opposite = Vertical;
            Vertical.Opposite = Horizontal;
        }
    }
}