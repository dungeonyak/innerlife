using System.Text.RegularExpressions;

namespace Application {
    public static class StringUtils {
        public static string Smush(string text) {
            return Regex.Replace(text.Trim(), "\\s+", " ");
        }
    }
}