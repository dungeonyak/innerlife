using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Application {
    public class Species {
        public string Name;
        public IAnimation Animation;
        public Func<Entity, RichText> Description;
        public Diet Diet = Diet.Omnivore;
        public bool SeekFood = true;
        public IEnumerable<Ability> Intrinsics = Enumerable.Empty<Ability>();
        public bool Scarable = false;

        public static Species Dog = new Species() {
            Name = "dog",
            Animation = DawnLike.Animation("Characters/Dog", 3),
            Description = entity => new RichText($@"
                It's <brown>{entity.Describe()}</brown>.
                Its hungry teeth look terrifying!
                However, it won't harm a <brown>FRIEND</brown>.
            "),
            Diet = Diet.Carnivore,
            SeekFood = false,
        };

        public static Species Cow = new Species() {
            Name = "cow",
            Animation = DawnLike.Animation("Characters/Quadraped", 11),
            Description = entity => new RichText($@"
                It's <brown>{entity.Describe()}</brown>.
                Even though it's simple, it's happy.
                As well it's a bountiful source of <brown>MEAT</brown>.
            "),
            Diet = Diet.Herbivore,
            SeekFood = false,
            Scarable = true,
        };

        public static Species Snake = new Species() {
            Name = "snake",
            Animation = DawnLike.Animation("Characters/Reptile", 37),
            Description = entity => new RichText($@"
                It's <green>{entity.Describe()}</green>.
                Who knows where its body ends?
                It has an evil spirit in its eye.
            "),
            Diet = Diet.Carnivore,
        };

        public static Species Owl = new Species() {
            Name = "owl",
            Animation = DawnLike.Animation("Characters/Avian", 71),
            Description = entity => new RichText($@"
                It's <brown>{entity.Describe()}</brown>.
                It seems like melancholy bird.
                However, it isn't just as simple as it appears.
            "),
            Diet = Diet.Omnivore,
            SeekFood = false,
        };

        public static Species Rabbit = new Species() {
            Name = "rabbit",
            Animation = DawnLike.Animation("Characters/Rodent", 3),
            Description = entity => new RichText($@"
                It's <blue>{entity.Describe()}</blue>.
                It's faster than a bullet!
                Its ears are too much longer than its body.
            "),
            Diet = Diet.Herbivore,
            Scarable = true,
            Intrinsics = new[] { Ability.Speed },
        };

        public static Species Ram = new Species() {
            Name = "ram",
            Animation = DawnLike.Animation("Characters/Quadraped", 16),
            Description = entity => new RichText($@"
                It's <gold>{entity.Describe()}</gold>.
                Its horn looks strong!
                It has a majestic feeling.
            "),
            Diet = Diet.Herbivore,
            Intrinsics = new[] { Ability.Bash },
            Scarable = true,
        };

        public static Species Spider = new Species() {
            Name = "spider",
            Animation = DawnLike.Animation("Characters/Pest", 16),
            Description = entity => new RichText($@"
                It's <grey>{entity.Describe()}</grey>.
                It's more afraid than you are.
                Even still, it won't hesitate to protect its nest.
            "),
            Diet = Diet.Carnivore,
            Intrinsics = new[] { Ability.Web },
        };

        public static Species Hawk = new Species() {
            Name = "hawk",
            Animation = DawnLike.Animation("Characters/Avian", 21),
            Description = entity => new RichText($@"
                It's <brown>{entity.Describe()}</brown>.
                What is it thinking in its birdy head?
                Only about the skies!
            "),
            Diet = Diet.Carnivore,
            Intrinsics = new[] { Ability.Flight },
        };

        public static Species Gadfly = new Species() {
            Name = "gadfly",
            Animation = DawnLike.Animation("Characters/Pest", 13),
            Description = entity => new RichText($@"
                It's <grey>{entity.Describe()}</grey>.
                It's buzzing furiously!!
                Though it might be angry, it won't hurt you.
            "),
            Diet = Diet.Omnivore,
            SeekFood = false,
            Intrinsics = new[] { Ability.Speed, Ability.Flight },
        };

        public static Species Rat = new Species() {
            Name = "rat",
            Animation = DawnLike.Animation("Characters/Rodent", 10),
            Description = entity => new RichText($@"
                It's <brown>{entity.Describe()}</brown>.
                Although it's cute, it's a danger.
                Its tail looks like a whip.
            "),
            Diet = Diet.Omnivore,
            SeekFood = true,
        };

        public static Species Elephant = new Species() {
            Name = "elephant",
            Animation = DawnLike.Animation("Characters/Quadraped", 44),
            Description = entity => new RichText($@"
                It's <grey>{entity.Describe()}</grey>.
                Its legs are as strong as tree trunks!
                It flaps its big ears like a fan.
            "),
            Diet = Diet.Herbivore,
            SeekFood = true,
            Intrinsics = new[] { Ability.Shove },
        };

        public static Species Slime = new Species() {
            Name = "mouldering slime",
            Animation = DawnLike.Animation("Characters/Slime", 15),
            Description = entity => new RichText($@"
                It's <green>{entity.Describe()}</green>.
                It's impossible to tell where the face is!
                It has an unkind stench.
            "),
            Diet = Diet.HardcoreOmnivore,
            SeekFood = true,
            Intrinsics = new[] { Ability.Grab },
        };
    }
}