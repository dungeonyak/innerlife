﻿using System;
using UnityEngine;

namespace Application {
    public static class RenderUtils {
        public const int TileSize = 16;
        public const int BodyOffset = 0;

        public static float ClampToPixel(double fraction) {
            var subdivisions = TileSize;
            return Mathf.Round((float) (fraction * subdivisions)) / subdivisions;
        }
    }
}
