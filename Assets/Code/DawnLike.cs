﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Application {
    public static class DawnLike {
        public static Color White  = ColorUtils.FromHex("dfefd7");
        public static Color Black  = ColorUtils.FromHex("140c1c");
        public static Color Brown  = ColorUtils.FromHex("d37d2c");
        public static Color Blue   = ColorUtils.FromHex("597dcf");
        public static Color Green  = ColorUtils.FromHex("6daa2c");
        public static Color Slate  = ColorUtils.FromHex("4d494d");
        public static Color Red    = ColorUtils.FromHex("d34549");
        public static Color Yellow = ColorUtils.FromHex("dbd75d");
        public static Color Grey   = ColorUtils.FromHex("757161");

        public static Animation Animation(string path, int index) {
            return new Animation(new Sprite[] {
                Sprites.LoadFromSheet($"DawnLike/{path}0", index),
                Sprites.LoadFromSheet($"DawnLike/{path}1", index),
            }, 2);
        }

        public static Animation Animation(string path, string name) {
            return new Animation(new Sprite[] {
                Sprites.LoadFromSheet($"DawnLike/{path}0", name + "0"),
                Sprites.LoadFromSheet($"DawnLike/{path}1", name + "1"),
            }, 2);
        }

        public static Still Still(string path, int index) {
            var sprite = Sprites.LoadFromSheet($"DawnLike/{path}", index);
            return new Still(sprite);
        }

        public static Still Still(string path, string name) {
            var sprite = Sprites.LoadFromSheet($"DawnLike/{path}", name);
            return new Still(sprite);
        }
    }
}
