using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Application {
    [Listener]
    public class WorldActs {
        [Inject] public EntityFinder EntityFinder;
        [Inject] public Randomizer Random;
        [Inject] public EventBus EventBus;
        
        [Handler]
        public void OnReady(Ready e) {
            if (Utils.IsDead(Utils.GetAvatar()) || Utils.CannotAct(Utils.GetAvatar())) {
                Container.Resolve<Ai>().RunActive();

                if (!Utils.AnyTweensExist()) {
                    EventBus.Dispatch(new Ready());
                }
            }
        }
    }
}