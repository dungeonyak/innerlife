using System.Linq;
using UnityEngine;

namespace Application {
    [Listener]
    public class UpdateTimeOnMap {
        [Inject] public EntityFinder EntityFinder;
    
        [Handler]
        public void OnEntityChangedMap(EntityChangedMap e) {
            if (e.Entity.MaybeGet<TimeOnMapComponent>() is TimeOnMapComponent c) {
                c.Turns = 0;
            }
        }

        [Handler]
        public void OnReady(Ready e) {
            EntityFinder.WhereHas<TimeOnMapComponent>()
                .Select(entity => entity.Get<TimeOnMapComponent>())
                .ForEach(c => c.Turns++);
        }
    }
}