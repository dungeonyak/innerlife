using UnityEngine;

namespace Application {
    [Listener]
    public class CreateDoorLock {
        [Handler]
        public void OnGainAbility(GainedAbility e) {
            if (e.Ability == Ability.Armor) {
                e.Entity.Add(new DoorLockComponent());
            }
        }

        [Handler]
        public void OnLoseAbility(LostAbility e) {
            if (e.Ability == Ability.Armor) {
                e.Entity.Remove<DoorLockComponent>();
            }
        }
    }
}