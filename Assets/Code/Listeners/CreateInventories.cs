using UnityEngine;

namespace Application {
    [Listener]
    public class CreateInventories {
        //[Handler]
        public void OnEntityChangedMap(EntityChangedMap e) {
            if (e.Entity.Has<PlayerControlledComponent>()) { // TODO: if it moved near player {
                foreach (var entity in e.NewMap.PlacedEntities) {
                    if (entity.MaybeGet<HnauComponent>() != null &&
                        entity.MaybeGet<InventoryComponent>() == null) {
                        var innerMap = MapBuilderFactory.GetBuilder(entity).Build();

                        entity.Add(new InventoryComponent(innerMap));
                    }
                }
            }
        }
    }
}