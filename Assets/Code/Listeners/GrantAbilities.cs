using System.Linq;
using UnityEngine;

namespace Application {
    [Listener]
    public class GrantAbilities {
        [Handler]
        public void OnEntityChangedMap(EntityChangedMap e) {
            if (e.Entity.MaybeGet<AbilityGrantingComponent>() is AbilityGrantingComponent c) {
                var ability = c.Ability;

                if (e.OldMap?.Holder is Entity oldHolder) {
                    var hasIntrinsicAbility = oldHolder.MaybeGet<IntrinsicAbilityComponent>() is IntrinsicAbilityComponent ia
                        && ia.Abilities.Contains(ability);
    
                    var hasOtherProvider = oldHolder.Get<InventoryComponent>().Map.PlacedEntities
                        .Where(o => o.MaybeGet<AbilityGrantingComponent>() is AbilityGrantingComponent agc && agc.Ability == ability)
                        .Any();

                    if (!hasIntrinsicAbility && !hasOtherProvider) {
                        oldHolder.RemoveAbility(ability);
                    }
                }

                if (e.NewMap?.Holder is Entity newHolder) {
                    newHolder.EnsureAbility(ability);
                }
            }
        }
    }
}