using System.Linq;
using UnityEngine;

namespace Application {
    [Listener]
    public class UpdateCrowns {
        [Handler]
        public void OnEntityChangedMap(EntityChangedMap e) {
            if (e.Entity.Has<CrownComponent>()) {
                if (e.NewMap?.Holder is Entity newCrowned) {
                    if (!newCrowned.Has<CrownComponent>()) {
                        newCrowned.Add(new CrownComponent());
                    }
                }

                if (e.OldMap?.Holder is Entity oldCrowned) {
                    var crownedContents = e.OldMap.PlacedEntities.Where(ent => ent.Has<CrownComponent>());

                    if (!crownedContents.Any()) {
                        oldCrowned.Remove<CrownComponent>();
                    }
                }
            }
        }
    }
}