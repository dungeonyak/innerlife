﻿using System;
using System.Linq;
using UnityEngine;

namespace Application {
    [Listener]
    public class UpdateTweens   {
        [Inject] public EntityFinder EntityFinder;

        [Handler]
        public void OnUpdate(Update update) {
            var rate = 6.0;

            var existed = Utils.AnyTweensExist();

            foreach (var entity in EntityFinder.WhereHas<WalkTweenComponent>().ToList()) {
                var tween = entity.Get<WalkTweenComponent>();

                tween.Progress += rate * Time.deltaTime;

                if (tween.Progress >= 1.0) {
                    entity.Remove<WalkTweenComponent>();
                }
            }

            foreach (var entity in EntityFinder.WhereHas<PickedUpTweenComponent>().ToList()) {
                var tween = entity.Get<PickedUpTweenComponent>();

                tween.Progress += rate * Time.deltaTime;

                if (tween.Progress >= 1.0) {
                    entity.Remove<PickedUpTweenComponent>();
                }
            }

            foreach (var entity in EntityFinder.WhereHas<TravelTweenComponent>().ToList()) {
                var tween = entity.Get<TravelTweenComponent>();

                tween.Progress += rate * Time.deltaTime;

                if (tween.Progress >= 1.0) {
                    entity.Remove<TravelTweenComponent>();
                }
            }

            foreach (var entity in EntityFinder.WhereHas<ShakeTweenComponent>().ToList()) {
                var tween = entity.Get<ShakeTweenComponent>();

                tween.Progress += rate * Time.deltaTime;

                if (tween.Progress >= 1.0) {
                    entity.Remove<ShakeTweenComponent>();
                }
            }

            foreach (var entity in EntityFinder.WhereHas<AttackTweenComponent>().ToList()) {
                var tween = entity.Get<AttackTweenComponent>();

                tween.Progress += rate * Time.deltaTime;

                if (tween.Progress >= 1.0) {
                    entity.Remove<AttackTweenComponent>();
                }
            }

            if (existed && !Utils.AnyTweensExist()) {
                Container.Resolve<EventBus>().Dispatch(new Ready());
            }
        }
    }
}
