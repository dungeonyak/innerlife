using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Application {
    [Listener]
    public class ResetCooldowns {
        [Inject] public EntityFinder EntityFinder;
        
        [Handler]
        public void OnReady(Ready e) {
            EntityFinder.WhereHas<DoorLockComponent>()
                .Select(entity => entity.Get<DoorLockComponent>())
                .ForEach(doorLock => {
                    if (doorLock.JustUnlocked) {
                        doorLock.JustUnlocked = false;
                    } else {
                        doorLock.IsLocked = true;                        
                    }
                });

            EntityFinder.WhereHas<StunComponent>()
                .ToList()
                .ForEach(entity => entity.Remove<StunComponent>());

            EntityFinder.WhereHas<BoredomComponent>()
                .ToList()
                .ForEach(entity => {
                    var boredom = entity.Get<BoredomComponent>();

                    if (boredom.RemainingDuration-- <= 0) {
                        entity.Remove(boredom);
                    }
                });
        }
    }
}