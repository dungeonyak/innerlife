using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Application {
    [Listener]
    public class WorldReacts {
        [Inject] public EntityFinder EntityFinder;
        [Inject] public Randomizer Random;
        
        [Handler]
        public void OnPlayerMoved(PlayerMoved e) {
            Container.Resolve<Ai>().RunActive();
        }
    }
}