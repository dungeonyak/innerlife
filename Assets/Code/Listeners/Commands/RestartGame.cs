using System;
using System.Linq;
using UnityEngine;

namespace Application {
    [Listener]
    public class RestartGame {
        [Inject] public EntityFinder EntityFinder;

        [Handler(Priority = 100000)]
        public void OnKeyPressed(KeyPressed e) {
            var key = e.Key;
    
            if (key == KeyCode.R && Utils.GetAvatar() is Entity avatar && avatar.IsDead()) {
                GameState.IsRestarting = true;
            }
        }
    }
}
