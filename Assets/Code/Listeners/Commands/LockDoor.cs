using System;
using System.Linq;
using UnityEngine;

namespace Application {
    [Listener]
    public class LockDoor {
        [Inject] public EntityFinder EntityFinder;
        [Inject] public EventBus EventBus;

        //[Handler]
        public void OnKeyPressed(KeyPressed e) {
            var key = e.Key;
    
            if (key == KeyCode.Period) {
                if (Utils.ReadyForCommand()) {
                    var avatar = Utils.GetAvatar();

                    if (avatar.IsInWater()) {
                        Messages.Write("You can't do this in water!");
                    } else {
                        if (avatar.MaybeGet<DoorLockComponent>() is DoorLockComponent doorLock) {
                            if (!doorLock.IsLocked) {
                                if (doorLock.JustUnlocked) {
                                    Messages.Write("You can't lock your doors again yet.");
                                } else {
                                    Actions.LockDoor(avatar);
                                    // TODO: play sound
                                    EventBus.Dispatch(new PlayerMoved());
                                }
                            } else {
                                Messages.Write("Your doors are already locked.");
                            }
                        }
                    }

                    Utils.MarkCommandRun();
                }
            }
        }
    }
}
