using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Application {
    [Listener]
    public class Bash {
        [Inject] public EntityFinder EntityFinder;
        [Inject] public EventBus EventBus;

        [Handler]
        public void StartTargeting(KeyPressed e) {
            var key = e.Key;
    
            var avatar = Utils.GetAvatar();

            if (avatar != null) {
                var commandMap = new Dictionary<KeyCode, TargetingIntent>();
                if (avatar.HasAbility(Ability.Bash)) commandMap.Add(KeyCode.B, TargetingIntent.Bash);
                if (avatar.HasAbility(Ability.Grab)) commandMap.Add(KeyCode.G, TargetingIntent.Grab);
                if (avatar.HasAbility(Ability.Eject)) commandMap.Add(KeyCode.E, TargetingIntent.Eject);

                TargetingIntent intent;

                if (commandMap.TryGetValue(key, out intent)) {
                    if (Utils.ReadyForCommand()) {
                        if (avatar.IsInWater()) {
                            Messages.Write("You can't do this in water!");
                        } else {
                            avatar.Add(new TargetingComponent(intent));
                            Utils.MarkCommandRun();
                        }
                    }
                }
            }
        }

        [Handler]
        public void CancelTargeting(KeyPressed e) {
            var key = e.Key;

            if (Utils.GetAvatar() is Entity avatar
                && avatar.Has<TargetingComponent>()) {
                if (key == KeyCode.Escape) {
                    avatar.Remove<TargetingComponent>();
                    Utils.MarkCommandRun();
                }
            }
        }

        [Handler]
        public void ConfirmDirection(KeyPressed e) {
            var key = e.Key;

            if (Utils.GetAvatar() is Entity avatar
                && avatar.MaybeGet<TargetingComponent>() is TargetingComponent targeting) {
                if (Utils.TranslateDirectionKey(key) is Point moveDirection) {
                    avatar.Remove<TargetingComponent>();

                    var success = AttemptManeuver(targeting.Intent, moveDirection);

                    if (success) {
                        EventBus.Dispatch(new PlayerMoved());
                    }

                    Utils.MarkCommandRun();
                }
            }
        }

        private bool AttemptManeuver(TargetingIntent intent, Point direction) {
            switch (intent) {
                case TargetingIntent.Bash: return AttemptBash(direction);
                case TargetingIntent.Eject: return AttemptEject(direction);
                case TargetingIntent.Grab: return AttemptGrab(direction);
            }

            throw new Exception("Don't know how to enact this intent");
        }

        private bool AttemptBash(Point direction) {
            var avatar = Utils.GetAvatar();

            var bashTarget = avatar.GetMap().GetEntitiesAt(avatar.Pos() + direction)
                .Where(entity => Utils.IsBashable(entity))
                .FirstOrDefault();

            if (bashTarget != null) {
                Actions.Bash(avatar, bashTarget);
                return true;
            } else {
                Messages.Write("There's nothing to bash there.");
                return false;
            }
        }

        private bool AttemptEject(Point direction) {
            var avatar = Utils.GetAvatar();

            var door = Utils.FindDoor(avatar, direction);

            var target = avatar.Get<InventoryComponent>().Map.GetEntitiesAt(door.Pos() - direction)
                .FirstOrDefault();

            if (target == null) {
                Messages.Write("There's nothing next to that door.");
                return false;
            } else if (target.Has<HnauComponent>()) {
                Messages.Write("You can't eject a living person.");
                return false;
            } else {
                Actions.Eject(avatar, target, door);
                return true;
            }
        }

        private bool AttemptGrab(Point direction) {
            var avatar = Utils.GetAvatar();

            var target = avatar.GetMap().GetEntitiesAt(avatar.Pos() + direction).FirstOrDefault();

            if (target != null) {
                Actions.Grab(avatar, target);
                return true;
            } else {
                Messages.Write("There's nothing to pick up there.");
                return false;
            }
        }
    }
}
