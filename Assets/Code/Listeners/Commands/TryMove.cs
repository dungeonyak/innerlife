﻿using System;
using System.Linq;
using UnityEngine;

namespace Application {
    [Listener]
    public class TryMove {
        [Inject] public EntityFinder EntityFinder;
        [Inject] public EventBus EventBus;

        [Handler]
        public void OnKeyPressed(KeyPressed e) {
            var key = e.Key;
    
            if (Utils.TranslateDirectionKey(key) is Point moveDirection) {
                if (Utils.ReadyForCommand()) {
                    var avatar = EntityFinder.WhereHas<PlayerControlledComponent>().FirstOrDefault();

                    if (avatar != null && avatar.MaybeGet<PlacementComponent>() is PlacementComponent p) {
                        var destination = p.Position + moveDirection;
                        AttemptMove(avatar, destination);
                    }

                    Utils.MarkCommandRun();
                }
            }
        }

        private void AttemptMove(Entity avatar, Point destination) {
            if (avatar.MaybeGet<WebbedComponent>() is WebbedComponent web) {
                if (web.RemainingDuration-- == 0) {
                    Messages.Write("You break free of the web!");
                    avatar.Remove(web);
                    // don't return, so they can still move on the same turn
                    // TODO: make this less shitty
                } else {
                    Messages.Write("You struggle against the web.");
                    avatar.Add(new ShakeTweenComponent());
                    EventBus.Dispatch(new PlayerMoved());
                    return;
                }
            }
            
            if (Movement.CanPushInto(avatar, destination)) {
                Movement.WalkTo(avatar, destination);
                EventBus.Dispatch(new PlayerMoved());
            } else {
                var occupants = Movement.GetEntitiesAt(new Placement(avatar.GetMap(), destination));

                if (occupants.Where(o => o.Has<HnauComponent>()).FirstOrDefault() is Entity hnau) {
                    if (hnau.MaybeGet<DoorLockComponent>() is DoorLockComponent doorLock && doorLock.IsLocked) {
                        Actions.UnlockDoor(avatar, hnau);
                        EventBus.Dispatch(new PlayerMoved());
                    } else {
                        Movement.WalkInto(avatar, hnau);
                    }
                } else if (occupants.Where(o => o.Has<DoorComponent>()).FirstOrDefault() is Entity door) {
                    Movement.WalkOutOf(avatar, door);
                }
            }
        }

        private void FaceEachOther(Entity a, Entity b) {

            if (a.MaybeGet<PlacementComponent>() is PlacementComponent aPlacement
                && b.MaybeGet<PlacementComponent>() is PlacementComponent bPlacement) {
                var aSprite = a.MaybeGet<FacingComponent>();
                var bSprite = b.MaybeGet<FacingComponent>();

                var deltaX = bPlacement.Position.X
                    - aPlacement.Position.X;

                if (deltaX > 0) {
                    if (aSprite != null) {
                        aSprite.Facing = Facing.Right;
                    }

                    if (bSprite != null) {
                        bSprite.Facing = Facing.Left;
                    }
                } else if (deltaX < 0) {
                    if (aSprite != null) {
                        aSprite.Facing = Facing.Left;
                    }

                    if (bSprite != null) {
                        bSprite.Facing = Facing.Right;
                    }
                }
            }
        }
    }
}
