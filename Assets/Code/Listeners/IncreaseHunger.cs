using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Application {
    [Listener]

    public class IncreaseHunger {
        [Inject] public EntityFinder EntityFinder;
        [Inject] public Randomizer Random;

        [Handler]
        public void OnReady(Ready ev) {
            var eaters = EntityFinder.WhereHas<HungerComponent>()
                .Where(e => !e.IsDead())
                .ToList();

            foreach (var eater in eaters) {
                var hunger = eater.Get<HungerComponent>();
                var turnsLeft = --hunger.TurnsUntilEat;

                if (turnsLeft <= 0) {
                    TryToEat(eater);
                } else if (eater.IsAvatar()) {
                    if (turnsLeft == Constants.MaxSatiety / 5) {
                        if (GetAvailableFood(eater).Any()) {
                            Messages.Write("<brown>You feel quite hungry. You might eat soon.</brown>");
                        } else {
                            Messages.Write("<red>You feel quite hungry. You need food now!</red>");
                        }
                    } else if (turnsLeft == Constants.MaxSatiety / 2) {
                        if (GetAvailableFood(eater).Any()) {
                            Messages.Write("<brown>You're starting to feel bit more hungry.</brown>");
                        } else {
                            Messages.Write("<red>You're starting to feel bit more hungry. Better find food!</red>");
                        }
                    }
                }
            }
        }

        private void TryToEat(Entity eater)
        {
            var availableFoods = GetAvailableFood(eater);

            if (availableFoods.Any()) {
                var eatenFood = Random.Choice(availableFoods);
                
                var satisfactionPhrase = Random.Choice(new[] {
                    "Yum!",
                    "Delicious!",
                    "It's tasteful!",
                });

                if (eater.IsAvatar()) {
                    Messages.Write($"<brown>You eat your {eatenFood.Describe()}. {satisfactionPhrase}</brown>");
                }

                Movement.Displace(eatenFood);

                if (eatenFood.Has<VictoryFoodComponent>()) {
                    eater.Add(new VictoryComponent());
                    eater.Remove<HungerComponent>();
                } else {
                    eater.Get<HungerComponent>().TurnsUntilEat = Constants.MaxSatiety;
                }
            } else {
                eater.Add(new DeadComponent());

                if (eater.IsAvatar()) {
                    Messages.Write("<red>Without food, you starve to death!</red>");
                }
            }
        }

        private IEnumerable<Entity> GetAvailableFood(Entity eater)
        {
            return eater.Get<InventoryComponent>().Map.PlacedEntities
                .Where(e => e.Has<FoodComponent>());
        }
    }
}