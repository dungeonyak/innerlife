using UnityEngine;

namespace Application {
    [Listener]
    public class EnactDeath {
        [Handler]
        public void OnEntityChangedMap(EntityChangedMap e) {
            if (e.Entity.Has<FoodComponent>()) {
                var foodLoser = e.OldMap?.Holder;

                if (foodLoser != null &&
                    !foodLoser.Has<HungerComponent>() &&
                    !foodLoser.Has<VictoryComponent>() &&
                    Utils.HasNoFood(foodLoser))
                {
                    foodLoser.Add(new DeadComponent());

                    if (foodLoser.IsAvatar()) {
                        Messages.Write("<red>Without food, you starve to death!</red>");
                    }
                }
            }
        }
    }
}