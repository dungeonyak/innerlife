using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Application {
    [Listener]
    public class DeteriorateWeb {
        [Inject] public EntityFinder EntityFinder;
        
        [Handler]
        public void OnReady(Ready e) {
            var webbedEntities = EntityFinder.WhereHas<WebbedComponent>().ToList();

            foreach (var entity in webbedEntities) {
                if (entity.IsAvatar()) {
                    // Player breaks free differently.
                    continue;
                }

                var webbedComponent = entity.Get<WebbedComponent>();

                if (webbedComponent.RemainingDuration-- == 0) {
                    entity.Remove<WebbedComponent>();
                }
            }
        }
    }
}