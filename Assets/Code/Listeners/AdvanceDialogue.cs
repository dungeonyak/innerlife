using System;
using System.Linq;
using UnityEngine;

namespace Application {
    [Listener]
    public class AdvanceDialogue {
        [Inject] public EntityFinder EntityFinder;

        [Handler]
        public void OnKeyPressed(KeyPressed e) {
            var key = e.Key;

            if (key == KeyCode.Space) {
                var speaker = EntityFinder.WhereHas<OngoingDialogueComponent>().FirstOrDefault();

                if (speaker != null) {
                    var dialogue = speaker.Get<OngoingDialogueComponent>();
                    var currentLine = dialogue.Lines.ElementAt(dialogue.CurrentLineIndex);
                    var fullLength = currentLine.TotalDuration;
                    
                    if (dialogue.CurrentLineProgress >= fullLength) {
                        if (dialogue.CurrentLineIndex >= dialogue.Lines.Count() - 1) {
                            speaker.Remove(dialogue);
                        } else {
                            dialogue.CurrentLineIndex++;
                            dialogue.CurrentLineProgress = 0;
                        }
                    } else {
                        dialogue.CurrentLineProgress = fullLength;
                    }
                }
            }
        }

        [Handler]
        public void OnFixedUpdate(FixedUpdate e) {
            var speakers = Container.Resolve<EntityFinder>().WhereHas<OngoingDialogueComponent>();

            foreach (var speaker in speakers) {
                var dialogue = speaker.Get<OngoingDialogueComponent>();

                dialogue.CurrentLineProgress += 8;
            }
        }
    }
}
