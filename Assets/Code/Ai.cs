using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Application {
    public class Ai {
        [Inject] public EntityFinder EntityFinder;
        [Inject] public Randomizer Random;
        [Inject] public ScentMapper ScentMapper;

        public void RunActive() {
            ScentMapper.Clear();

            var activeMaps = GetActiveMaps().ToList();

            var movers = activeMaps
                .SelectMany(map => map.PlacedEntities)
                .Where(entity => entity.Has<HnauComponent>())
                .Where(entity => !entity.Has<PlayerControlledComponent>() || Utils.IsDead(entity))
                .OrderBy(entity => entity.Has<ScaryComponent>() ? 0 : 1)
                .ToList();

            foreach (var mover in movers) {
                mover.RemoveIfPresent<ActedThisTurnComponent>();
                mover.RemoveIfPresent<MovedThisTurnComponent>();

                if (mover.IsDead() || !Utils.CannotAct(mover)) {
                    RunAi(mover);

                    if (mover.HasAbility(Ability.Speed) && !mover.Has<ActedThisTurnComponent>()) {
                        RunAi(mover);
                    }
                }
            }
        }

        private IEnumerable<Map> GetActiveMaps() {
            var player = EntityFinder.WhereHas<PlayerControlledComponent>().First();

            if (player.MaybeGet<PlacementComponent>() is PlacementComponent pc) {
                yield return pc.Placement.Map;

                if (pc.Placement.Map.Holder is Entity holder) {
                    yield return holder.Get<PlacementComponent>().Map;
                }
            }

            if (player.MaybeGet<InventoryComponent>() is InventoryComponent inventory) {
                yield return inventory.Map;

                foreach (var entity in inventory.Map.PlacedEntities) {
                    if (entity.MaybeGet<InventoryComponent>() is InventoryComponent nestedInventory) {
                        yield return nestedInventory.Map;
                    }
                }
            }
        }

        private void RunAi(Entity me) {
            if (Utils.IsDead(me)) {
                if (SeekExit(me) || Wander(me));
            } else if (ShouldMoseyOut(me)) {
                if (SeekExit(me) || Wander(me));

                if (me.Has<BoredomComponent>()) {
                    me.Get<BoredomComponent>().RemainingDuration = 5;
                } else {
                    me.Add(new BoredomComponent());
                }
            } else if (me.Has<BoredomComponent>()) {
                Wander(me);
            } else if (me.Has<ScaryComponent>()) {
                if (SeekShelter(me) || ScareSomeone(me) || Wander(me));
            } else if (me.MaybeGet<ScaredComponent>() is ScaredComponent scaredComponent) {
                if (RunAway(me, scaredComponent.Scarer) || SeekExit(me) || Wander(me));
                me.Remove<ScaredComponent>();
            } else if (me.Has<FoodSeekingComponent>()) {
                if (SeekFood(me) || TryAbility(me) || Wander(me));
            } else {
                Wander(me);
            } 
        }

        private bool RunAway(Entity me, Entity scarer)
        {
            if (me.GetMap() == null || me.GetMap() != scarer.GetMap()) {
                return false;
            }

            var doorsByDesirability = me.GetMap().PlacedEntities
                .Where(e => e.Has<DoorComponent>())
                .OrderBy(door => {
                    // TODO: Run away better.
                    return door.Pos().Manhattan(me.Pos());
                });

            foreach (var door in doorsByDesirability) {
                if (TryToExitThroughDoor(me, door)) {
                    return true;
                }
            }

            return false;
        }

        private bool SeekShelter(Entity me) {
            var shelterEntity = Utils.GetAvatar();

            if (me.GetMap()?.Holder == shelterEntity) {
                return false;
            }

            if (me.GetMap() == shelterEntity.GetMap()) {
                // TODO: IsAdjacent
                if (me.Pos().Manhattan(shelterEntity.Pos()) == 1) {
                    if (TryToPeacefullyEnter(me, shelterEntity)) {
                        return true;
                    }
                } else if (me.Pos().Chebyshev(shelterEntity.Pos()) < 8) {
                    if (PathToward(me, shelterEntity)) {
                        return true;
                    }
                }
            }

            return false;
        }

        private bool TryToPeacefullyEnter(Entity me, Entity target) {
            if (CanAct(me)) {
                if (me.Pos().Manhattan(target.Pos()) == 1) {
                    if (target.MaybeGet<DoorLockComponent>() is DoorLockComponent doorLock && doorLock.IsLocked) {
                        Actions.UnlockDoor(me, target);
                        MarkActed(me);
                        return true;
                    } else if (!Utils.CannotMove(me)) {
                        Movement.WalkInto(me, target);
                        MarkActed(me);
                        return true;
                    }
                }
            }

            return false;
        }

        private bool ScareSomeone(Entity me)
        {
            // TODO: only inside avatar!
            var target = me.GetMap().PlacedEntities
                .Where(e => e != me)
                .Where(e => !e.IsAvatar())
                .Where(e => e.Has<HnauComponent>())
                .Where(e => e.Has<ScarableComponent>())
                .OrderBy(e => e.Pos().Manhattan(me.Pos()))
                .FirstOrDefault();

            if (target != null) {
                var initialPosition = me.Pos();
                var wasAdjacent = me.Pos().Manhattan(target.Pos()) == 1;

                if (wasAdjacent || PathToward(me, target)) {
                    var newPosition = me.Pos();

                    if (wasAdjacent || newPosition.Manhattan(target.Pos()) < initialPosition.Manhattan(target.Pos())) {
                        if (!target.Has<ScaredComponent>()) {
                            target.Add(new ScaredComponent(me));

                            if (!target.Has<PreviouslyScaredComponent>()) {
                                target.Add(new PreviouslyScaredComponent());

                                Messages.Write($"<blue>{target.Describe()} is scared of {me.Describe()}!</blue>");
                            }
                        }
                    }

                    return true;
                }
            }

            return false;
        }

        private bool TryAbility(Entity me) {
            if (!CanAct(me)) {
                return false;
            }

            if (me.HasAbility(Ability.Grab) && Utils.AvatarCanSee(me) && Random.PercentChance(100)) {
                var targets = me.Pos().Neighbors
                    .SelectMany(n => me.GetMap().GetEntitiesAt(n))
                    .Where(n => n.Has<HnauComponent>() || n.Has<SolidDecorComponent>())
                    .Where(n => !n.IsAvatar())
                    .OrderBy(n => n.Has<DecorComponent>() ? 0 : 1);

                if (targets.Any()) {
                    var target = targets.First();

                    Actions.Grab(me, target);
                    MarkActed(me);
                    return true;
                }
            }

            if (me.HasAbility(Ability.Bash) && Random.PercentChance(70)) {
                var map = me.GetMap();

                var bashTargets = me.Pos().Neighbors
                    .SelectMany(n => map.GetEntitiesAt(n))
                    .Where(n => n.Has<HnauComponent>() || n.Has<SolidDecorComponent>())
                    .OrderBy(n => n.Has<HnauComponent>() ? 0 : 1);

                if (bashTargets.Any()) {
                    var target = Random.Choice(bashTargets);
                    Actions.Bash(me, target);
                    MarkActed(me);
                    return true;
                }
            }

            if (me.HasAbility(Ability.Web) && Random.PercentChance(90)) {
                var map = me.GetMap();

                var webTargets = me.Pos().Neighbors
                    .SelectMany(n => map.GetEntitiesAt(n))
                    .Where(n => n.Has<HnauComponent>())
                    .Where(n => !n.Has<WebbedComponent>())
                    .Where(n => !n.HasAbility(Ability.Web))
                    .OrderBy(n => n.IsAvatar() ? 0 : 1);

                if (webTargets.Any()) {
                    var target = webTargets.First();
                    Actions.Web(me, target);
                    MarkActed(me);
                    return true;
                }
            }

            return false;
        }

        private bool ShouldMoseyOut(Entity me) {
            return me.GetMap()?.Holder is Entity holder && holder.IsAvatar()
                && me.MaybeGet<TimeOnMapComponent>()?.Turns > Constants.BoredomThreshold;
        }

        private bool SeekExit(Entity me) {
            var myPlacement = me.Get<PlacementComponent>();

            var nearestDoor = myPlacement.Map.PlacedEntities
                .Where(e => e.Has<DoorComponent>())
                .OrderBy(door => myPlacement.Position.Chebyshev(door.Pos()))
                .FirstOrDefault();

            if (nearestDoor != null) {
                TryToExitThroughDoor(me, nearestDoor);
            }

            return false;
        }

        private bool TryToExitThroughDoor(Entity me, Entity door) {
            // TODO: IsAdjacent
            var distance = me.Pos().Manhattan(door.Pos());
            if (distance == 1 && CanAct(me) && !Utils.CannotMove(me)) {
                Movement.WalkOutOf(me, door);
                MarkActed(me);
                return true;
            } else {
                if (PathToward(me, door)) {
                    return true;
                }
            }

            return false;
        }

        private bool SeekFood(Entity me) {
            var myPlacement = me.Get<PlacementComponent>();

            var target = EntityFinder
                .WhereHas<PlayerControlledComponent>()
                .Where(e => !e.IsDead())
                .FirstOrDefault();

            if (target != null) {
                var targetPlacement = target.Get<PlacementComponent>();
                var targetInventory = target.Get<InventoryComponent>();

                if (myPlacement.Map == targetInventory.Map) {
                    var interestingFood = targetInventory.Map.PlacedEntities
                        .Where(food => food != me)
                        .Where(food => Utils.DietIncludes(me, food))
                        .OrderBy(e => e.Get<PlacementComponent>().Position.Manhattan(myPlacement.Position))
                        .FirstOrDefault();

                    if (interestingFood != null) {
                        if (myPlacement.Position.Manhattan(interestingFood.Pos()) == 1
                            && (interestingFood.Has<SolidDecorComponent>() || interestingFood.Has<HnauComponent>()))
                        {
                            Debug.Log($"Grabbing {interestingFood.Describe()}!");
                            Actions.Grab(me, interestingFood);
                            MarkActed(me);
                            return true;
                        } else if (PathToward(me, interestingFood)) {
                            return true;
                        }
                    }
                } else if (myPlacement.Map == targetPlacement.Map) {
                    // TODO: IsAdjacent
                    if (myPlacement.Position.Manhattan(targetPlacement.Position) == 1) {
                        if (Assault(me, target)) {
                            return true;
                        }
                    } else if (myPlacement.Position.Chebyshev(targetPlacement.Position) < 8) {
                        if (PathToward(me, target)) {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private bool Assault(Entity me, Entity victim) {
            if (CanAct(me)) {
                if (me.Pos().Manhattan(victim.Pos()) == 1) {
                    if (TryAbility(me)) {
                        return true;
                    } else if (victim.MaybeGet<DoorLockComponent>() is DoorLockComponent doorLock && doorLock.IsLocked) {
                        Actions.UnlockDoor(me, victim);
                        MarkActed(me);
                        return true;
                    } else if (!Utils.CannotMove(me)) {
                        Movement.WalkInto(me, victim);
                        MarkActed(me);
                        return true;
                    }
                }
            }

            return false;
        }

        private bool Wander(Entity me) {
            if (Utils.CannotMove(me)) {
                return true;
            }

            if (me.Has<ActedThisTurnComponent>()) {
                return true;
            }

            var wanderChance = me.MaybeGet<SpeciesComponent>()?.Species == Species.Gadfly ? 100 : 30; // TODO: do better

            if (Random.PercentChance(wanderChance)) {
                var pc = me.Get<PlacementComponent>();
                var currentPosition = pc.Position;
                var candidates = currentPosition.Neighbors.Where(dest => {
                    return pc.Map.InBounds(dest) && Movement.CanStepInto(me, dest);
                });

                if (candidates.Any()) {
                    var destination = Random.Choice(candidates);
                    Movement.WalkTo(me, destination);
                    MarkMoved(me);
                }
            }

            return true;
        }

        private bool PathToward(Entity me, Entity target) {
            if (Utils.CannotMove(me)) {
                return false;
            }

            if (me.Has<ActedThisTurnComponent>()) {
                return false;
            }

            var myPosition = me.Pos();
            
            if (myPosition.Manhattan(target.Pos()) == 1) {
                if (Movement.CanPushInto(me, target.Pos())) {
                    Movement.WalkTo(me, target.Pos());
                    MarkMoved(me);
                }

                return true;
            }

            var map = me.GetMap();

            PathFlags pathFlags = 0;

            if (me.IsFlying()) {
                pathFlags |= PathFlags.Flying;
            }

            if (me.HasAbility(Ability.Shove)) {
                pathFlags |= PathFlags.Shove;
            }

            var scentMap = ScentMapper.Get(target, pathFlags, point => {
                return map.InBounds(point) && Movement.CanPushInto(me, new Placement(map, point));
            });

            var comparer = Comparer<int>.Create((scent1, scent2) => {
                return scent1 > scent2 ? 1 :
                    scent2 > scent1 ? -1 :
                    Random.PercentChance(50) ? 1 : -1;
            });

            var bestNeighbors = myPosition.Neighbors
                .Where(n => map.InBounds(n))
                .Where(n => Movement.CanPushInto(me, n))
                .Where(n => scentMap[n] != null)
                .OrderBy(n => scentMap[n].Value, comparer);

            if (bestNeighbors.Any()) {
                var chosenNeighbor = bestNeighbors.First();

                Movement.WalkTo(me, chosenNeighbor);
                MarkMoved(me);
                return true;
            }

            return false;
        }

        private bool WalkStraightToward(Entity me, Entity target) {
            var myPosition = me.Get<PlacementComponent>().Position;
            var delta = target.Get<PlacementComponent>().Position - myPosition;
            var step = delta.Sign();

            if (Math.Abs(delta.X) > Math.Abs(delta.Y)) {
                step = new Point(step.X, 0);
            } else {
                step = new Point(0, step.Y);
            }

            var dest = myPosition + step;

            if (Movement.CanPushInto(me, dest)) {
                Movement.WalkTo(me, dest);
                MarkMoved(me);
                return true;
            } else {
                return false;
            }
        }

        private void MarkMoved(Entity me) {
            if (!me.Has<MovedThisTurnComponent>()) {
                me.Add(new MovedThisTurnComponent());
            }
        }

        private void MarkActed(Entity me) {
            if (!me.Has<ActedThisTurnComponent>()) {
                me.Add(new ActedThisTurnComponent());
            }
        }

        private bool CanAct(Entity me) {
            return !me.Has<ActedThisTurnComponent>()
                && !me.Has<MovedThisTurnComponent>();
        }
    }
}