﻿using System;
using System.Collections.Generic;

namespace Application {
    public class Grid<T> {
        public readonly int Width;
        public readonly int Height;
        private T[,] Data;

        public Grid(int width, int height, Func<Point, T> populate) {
            Width = width;
            Height = height;
            Data = new T[width, height];

            for (var x = 0; x < width; x++) {
                for (var y = 0; y < height; y++) {
                    Data[x, y] = populate(new Point(x, y));
                }
            }
        }


        public Grid(int width, int height) {
            Width = width;
            Height = height;
            Data = new T[width, height];
        }

        public T this[Point point] {
            get {
                if (!InBounds(point)) {
                    throw new Exception($"Out of bounds: {point}");
                }

                return Data[point.X, point.Y];
            }
            set {
                if (!InBounds(point)) {
                    throw new Exception($"Out of bounds: {point}");
                }

                Data[point.X, point.Y] = value;
            }
        }

        public bool InBounds(Point point) {
            return point.X >= 0 && point.Y >= 0 && point.X < Width && point.Y < Height;
        }

        public bool IsOnEdge(Point point) {
            return point.X == 0 || point.Y == 0 || point.X == Width - 1 || point.Y == Height - 1;
        }

        public IEnumerable<Point> Points {
            get {
                for (var x = 0; x < Width; x++) {
                    for (var y = 0; y < Height; y++) {
                        yield return new Point(x, y);
                    }
                }
            }
        }
    }
}
