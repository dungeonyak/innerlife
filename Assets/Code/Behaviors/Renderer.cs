﻿using System;
using System.Linq;
using UnityEngine;

namespace Application {
    public class Renderer : MonoBehaviour {
        private SpriteFont Font;
        private Point LastScreenSize;

        public void Start() {
            var screenWidth = 960;
            var screenHeight = 600;
            Screen.SetResolution(screenWidth, screenHeight, false);

            var fontSprites = Enumerable.Range(0, 255).Select(i => Sprites.LoadFromSheet("acorn", i));
            Font = new SpriteFont(fontSprites);
            Container.Register<SpriteFont>(Font);

            LastScreenSize = new Point(Screen.width, Screen.height);
            SizeCameras();
        }

        private void SizeCameras() {
            Cameras.Configure(
                Cameras.MainMap,
                new Point(240, 240),
                new Point(0, Screen.height - 240 * 2),
                2
            );

            var mainMapRect = Cameras.ScreenRect(Cameras.MainMap);
            var inventorySize = Point.Square(9 * RenderUtils.TileSize);
            Cameras.Configure(
                Cameras.InventoryMap,
                inventorySize,
                mainMapRect.BottomRight
                    + (new Point(Screen.width, Screen.height) - mainMapRect.BottomRight) / 3
                    - inventorySize
                    + Point.Horizontal(8)
                    - Point.Vertical(2),
                3
            );

            Cameras.Configure(
                Cameras.Ui,
                new Point(Screen.width / 2, Screen.height / 2),
                new Point(0, 0),
                2
            );

            Cameras.Ui.transform.position = new Vector3(
                Screen.width / 4,
                Screen.height / 4,
                Cameras.Ui.transform.position.z
            );
        }

        private void UpdateSize() {
            var currentScreenSize = new Point(Screen.width, Screen.height);

            if (currentScreenSize != LastScreenSize) {
                SizeCameras();
            }

            LastScreenSize = currentScreenSize;
        }

        public void OnPreCull() {
            UpdateSize();

            var spritePool = Container.Resolve<SpritePool>();
            spritePool.Begin();

            Container.Resolve<Inspector>().Clear();

            RenderMaps();
            RenderDialogue();
            RenderDeathText();
            RenderVictory();
            RenderUiLabels();
            RenderSatiety();

            if (!RenderInspector()) {
                RenderMessages();
            }
            
            if (!RenderCommandPrompt()) {
                RenderActionBar();
            }

            spritePool.End();
        }

        private void RenderActionBar() {
            if (Utils.GetAvatar() is Entity avatar) {
                var actionBar = new ActionBar(avatar, Font);
                actionBar.Render(LayerMask.NameToLayer("UI"), order: 0);
            }
        }

        private void RenderMaps() {
            var avatar = Container.Resolve<EntityFinder>()
                .WhereHas<PlayerControlledComponent>()
                .FirstOrDefault();

            if (avatar != null) {
                var positioner = Container.Resolve<CameraPositioner>();
                var mapRenderer = Container.Resolve<MapRenderer>();

                Map mainMap = avatar.Get<PlacementComponent>().Map;
                positioner.PositionCamera(mainMap, Cameras.MainMap, avatar);
                mapRenderer.Render(mainMap, Cameras.MainMap, centerEntity: avatar);

                Map innerMap = avatar.Get<InventoryComponent>().Map;
                positioner.PositionCamera(innerMap, Cameras.InventoryMap);
                mapRenderer.Render(innerMap, Cameras.InventoryMap);

                var holder = mainMap.Holder;

                /*
                if (holder != null) {
                    Map outerMap = holder.Get<PlacementComponent>().Map;

                    positioner.PositionCamera(outerMap, Cameras.LeftMap, holder, Point.Left);
                    positioner.PositionCamera(outerMap, Cameras.RightMap, holder, Point.Right);
                    positioner.PositionCamera(outerMap, Cameras.UpMap, holder, Point.Up);
                    positioner.PositionCamera(outerMap, Cameras.DownMap, holder, Point.Down);

                    mapRenderer.Render(outerMap, Cameras.LeftMap, holder, Point.Left, isOuter: true);
                    mapRenderer.Render(outerMap, Cameras.RightMap, holder, Point.Right, isOuter: true);
                    mapRenderer.Render(outerMap, Cameras.UpMap, holder, Point.Up, isOuter: true);
                    mapRenderer.Render(outerMap, Cameras.DownMap, holder, Point.Down, isOuter: true);
                }
                */
            }
        }

        private void RenderUiLabels() {
            var inventoryRect = Cameras.ScreenRect(Cameras.InventoryMap);

            var text = new RichText("Inventory");
            RichTextWriter.Write(
                Font,
                text,
                inventoryRect.TopLeft / 2
                    + Point.Horizontal(inventoryRect.Width / 2) / 2
                    - Point.Horizontal(RichTextWriter.MeasureWidth(text, Font) / 2)
                    + Point.Vertical(Font.Height)
                    + Point.Vertical(4),
                layer: LayerMask.NameToLayer("UI"),
                order: 1
            );

            if (Utils.GetAvatar() is Entity avatar && avatar.GetMap()?.Holder is Entity holder) {
                var depth = Utils.GetDepth(avatar);

                var levelText = new RichText($"Inside {holder.Describe()}\nDepth: {depth}");

                if (!Utils.IsOnVictoryPath(avatar.GetMap()) && !avatar.Has<VictoryComponent>()) {
                    levelText += new RichText($"<gold><char code=\"{Characters.ArrowUp}\" /></gold>");
                }

                RichTextWriter.Write(
                    Font,
                    levelText,
                    new Point(0, Cameras.ScreenRect(Cameras.MainMap).Bottom / 2)
                        - Point.Vertical(2),
                    layer: LayerMask.NameToLayer("UI"),
                    order: 1
                );
            }
        }

        private void RenderSatiety() {
            if (Utils.GetAvatar()?.MaybeGet<HungerComponent>()?.TurnsUntilEat is int satiety) {
                var layer = LayerMask.NameToLayer("UI");
                var offset = new Point(0, 25);

                var text = new RichText("SATIETY");

                RichTextWriter.Write(Font, text, offset, layer, order: 1, anchor: Anchor.BottomLeft);

                var pipStart = offset + Point.Horizontal(RichTextWriter.MeasureWidth(text, Font) + 4);
                var pipCount = satiety / 2;

                var spritePool = Container.Resolve<SpritePool>();

                var pipSprite = Sprites.Load("pip");

                for (var i = 0; i < pipCount; i++) {
                    spritePool.Draw(pipSprite, pipStart + Point.Horizontal(3 * i), layer, 1, color: DawnLike.Brown);
                }
            }
        }

        private void RenderDeathText() {
            if (Utils.GetAvatar() is Entity avatar && avatar.IsDead()) {
                var textBox = new TextBox(Font, new RichText(@"
                    You are <red>DEAD GHAST</red>! It's terrible!
                    Press <blue>R</blue> to restart...
                "));

                var textPosition = (
                    new Point(Screen.width / 2, Screen.height / 2) / 2 +
                    new Point(-textBox.Width / 2, -textBox.Height / 2)
                );

                textBox.Draw(textPosition, LayerMask.NameToLayer("UI"), order: 10);
            }
        }

        private void RenderVictory() {
            if (Utils.GetAvatar() is Entity avatar && avatar.Has<VictoryComponent>()) {
                var textBox = new TextBox(Font, new RichText(@"
                    <gold>You won! It's incredible!
                    Thank you for playing!</gold>
                "));

                var textPosition = (
                    new Point(Screen.width / 2, Screen.height / 2) / 2 +
                    new Point(-textBox.Width / 2, -textBox.Height / 2)
                );

                textBox.Draw(textPosition, LayerMask.NameToLayer("UI"), order: 11);
            }
        }

        private void RenderDialogue() {
            var speaker = Container.Resolve<EntityFinder>()
                .WhereHas<OngoingDialogueComponent>()
                .FirstOrDefault();

            if (speaker != null) {
                var dialogue = speaker.Get<OngoingDialogueComponent>();
                var currentLine = dialogue.Lines.ElementAt(dialogue.CurrentLineIndex);

                var boxPosition = new Point(0, 0);
                var boxSize = new Point(240, 60);
                var padding = 2;
                var border = 8;

                Container.Resolve<SpritePool>().Draw(
                    Sprites.Load("dialogue"),
                    boxPosition,
                    layer: LayerMask.NameToLayer("UI"),
                    sliceSize: boxSize,
                    order: 1
                );

                RichTextWriter.Write(
                    Font,
                    currentLine,
                    boxPosition
                        + Point.Vertical(boxSize.Y)
                        + new Point(border, -border)
                        + new Point(padding, -padding),
                    layer: LayerMask.NameToLayer("UI"),
                    wrapWidth: boxSize.X - border * 2 - padding * 2,
                    progress: dialogue.CurrentLineProgress,
                    order: 1
                );
            }
        }

        private void RenderMessages() {
            var mainMapRect = Cameras.ScreenRect(Cameras.MainMap);
            var baseOffset = new Point(mainMapRect.Right, 0) / 2 + new Point(2, 2);
            var maxLines = 6;
            var lineCountSoFar = 0;

            foreach (var currentMessage in Messages.History.Reverse()) {
                var wrapWidth = (Screen.width - baseOffset.X * 2) / 2;
                var currentOffset = baseOffset + Point.Vertical(RichTextWriter.MeasureHeight(lineCountSoFar, Font) + 2);

                var maxLineCountToDraw = Math.Max(0, maxLines - lineCountSoFar);
                var lineCountToDraw = Math.Min(maxLineCountToDraw, RichTextWriter.CountLines(currentMessage, Font, wrapWidth));

                RichTextWriter.Write(
                    Font,
                    currentMessage,
                    currentOffset,
                    anchor: Anchor.BottomLeft,
                    layer: LayerMask.NameToLayer("UI"),
                    wrapWidth: wrapWidth,
                    maxLines: maxLineCountToDraw,
                    order: 1
                );

                lineCountSoFar += lineCountToDraw;
                
                if (lineCountSoFar >= maxLines) {
                    break;
                }
            }
        }

        private bool RenderInspector() {
            var inspector = Container.Resolve<Inspector>();

            if (inspector.InspectedEntity != null) {
                var entity = inspector.InspectedEntity;

                var description = GetDescription(entity);

                if (entity.MaybeGet<DietComponent>()?.Diet is Diet diet) {
                    if (diet == Diet.Carnivore) {
                        description += new RichText("<brown>\nIt eats only MEAT.</brown>");
                    } else if (diet == Diet.Herbivore) {
                        description += new RichText("<green>\nIt eats only PLANT.</green>");
                    }
                }

                if (Utils.GetAvatar() is Entity avatar && avatar.HasAbility(Ability.Compass) && !avatar.Has<VictoryComponent>()) {
                    if (entity.Has<CrownComponent>()) {
                        description += new RichText("<gold>\nVictory is within!</gold>");
                    } else if (entity.Has<DoorComponent>()
                        && entity.GetMap().Holder != avatar
                        && !entity.GetMap().Holder.Has<CrownComponent>()
                    ) {
                        description += new RichText("<gold>\nVictory is without!</gold>");
                    }
                }

                var textBox = new TextBox(Font, description, fixedWidth: 240);

                var position = new Point(
                    Cameras.ScreenRect(Cameras.MainMap).Right / 2,
                    0
                );

                textBox.Draw(position, LayerMask.NameToLayer("UI"), order: 1);

                return true;
            }

            return false;
        }

        private RichText GetDescription(Entity entity) {
            if (!entity.IsDead() && entity.MaybeGet<SpeciesComponent>() is SpeciesComponent speciesComponent) {
                return speciesComponent.Species.Description(entity);
            } else if (entity.MaybeGet<FoodComponent>() is FoodComponent foodComponent && foodComponent.FoodType.Description != null) {
                return foodComponent.FoodType.Description;
            } else {
                return new RichText($"It's just <blue>{entity.Describe()}</blue>.");
            }
        }

        private bool RenderCommandPrompt() {
            if (Utils.GetAvatar() is Entity avatar && avatar.MaybeGet<TargetingComponent>() is TargetingComponent targeting) {
                var intentDescription = Enum.GetName(typeof(TargetingIntent), targeting.Intent);
                var textBox = new TextBox(Font, new RichText($"{intentDescription} in what direction?"));

                textBox.Draw(new Point(0, 0), LayerMask.NameToLayer("UI"), order: 3);

                return true;
            }

            return false;
        }
    }
}
