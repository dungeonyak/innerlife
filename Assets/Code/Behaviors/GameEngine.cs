﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace Application {
    public class GameEngine : MonoBehaviour {
        private HashSet<Entity> Entities = new HashSet<Entity>();

        void Start() {
            GameState.Initialize();
        }

        void FixedUpdate() {
            Container.Resolve<EventBus>().Dispatch(new FixedUpdate());
        }

        void Update() {
            if (GameState.IsRestarting) {
                Container.Resolve<EntityFinder>().Clear();
                Container.Resolve<SpritePool>().Clear();
                Container.Resolve<EventBus>().Clear();
                Messages.Clear();
                GameState.Restart();
            } else {
                if (Utils.GetAvatar() is Entity avatar && avatar.Has<RanCommandComponent>()) {
                    avatar.Remove<RanCommandComponent>();
                }

                HandleInput();

                if (!GameState.IsRestarting) {
                    Container.Resolve<EventBus>().Dispatch(new Update());
                }
            }
        }

        private void HandleInput() {
            if (Input.anyKeyDown) {
                foreach(var k in Enum.GetValues(typeof(KeyCode))) {
                    var key = (KeyCode) k;
                    if (Input.GetKeyDown(key)) {
                        OnKeyDown(key);
                    }
                }
            }
        }

        private void OnKeyDown(KeyCode key) {
            Container.Resolve<EventBus>().Dispatch(new KeyPressed(key));
        }
    }
}
