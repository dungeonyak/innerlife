using System;
using System.Collections.Generic;

namespace Application {
    public class ActionBar {
        private Entity Actor;
        private SpriteFont Font;

        public ActionBar(Entity actor, SpriteFont font) {
            Actor = actor;
            Font = font;
        }

        public void Render(int layer, int order = 0) {
            var offset = 0;

            foreach (var action in GetAvailableActions()) {
                var label = GetActionLabel(action);
                var textBox = new TextBox(Font, label);
                textBox.Draw(new Point(offset, 0), layer, order);
                offset += textBox.Width;
            }
        }

        private RichText GetActionLabel(TargetingIntent action) {
            switch (action) {
                case TargetingIntent.Bash:  return new RichText("Bash:  <blue>b</blue>");
                case TargetingIntent.Grab:  return new RichText("Grab:  <blue>g</blue>");
                case TargetingIntent.Eject: return new RichText("Eject: <blue>e</blue>");
            }

            return new RichText(Enum.GetName(typeof(TargetingIntent), action));
        }

        private IEnumerable<TargetingIntent> GetAvailableActions() {
            if (Actor.HasAbility(Ability.Bash)) {
                yield return TargetingIntent.Bash;
            }

            if (Actor.HasAbility(Ability.Grab)) {
                yield return TargetingIntent.Grab;
            }

            if (Actor.HasAbility(Ability.Eject)) {
                yield return TargetingIntent.Eject;
            }
        }
    }
}