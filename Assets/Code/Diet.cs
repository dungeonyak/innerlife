namespace Application {
    public enum Diet {
        Carnivore,
        Herbivore,
        Omnivore,
        HardcoreOmnivore
    }
}