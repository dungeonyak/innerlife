using System;
using System.Collections.Generic;
using System.Linq;

namespace Application {
    public class ScentMapper {
        private Dictionary<ValueTuple<Entity, PathFlags>, ScentMap> ScentMaps = new Dictionary<ValueTuple<Entity, PathFlags>, ScentMap>();

        public ScentMap Get(Entity entity, PathFlags pathFlags, Predicate<Point> check) {
            return ScentMaps.SetDefault((entity, pathFlags), () => {
                var map = entity.Get<PlacementComponent>().Map;
                var scentMap = new ScentMap(map);

                FloodScent(map, scentMap, entity.Get<PlacementComponent>().Position, check);

                return scentMap;
            });
        }

        public void Clear() {
            ScentMaps.Clear();
        }

        private void FloodScent(Map map, ScentMap scentMap, Point origin, Predicate<Point> check) {
            var queue = new Queue<FloodNode>();
            var seen = new HashSet<Point>();

            queue.Enqueue(new FloodNode(origin, 0));

            var blah = 0;
            while (queue.Count > 0) {
                if (blah++ > map.Width * map.Height) {
                    throw new Exception("gagh!");
                }
                var next = queue.Dequeue();

                scentMap[next.Position] = next.ScentMagnitude;

                foreach (var neighbor in next.Position.Neighbors.Where(n => !seen.Contains(n))) {
                    if (check(neighbor)) {
                        queue.Enqueue(new FloodNode(neighbor, next.ScentMagnitude + 1));
                    }

                    seen.Add(neighbor);
                }
            }
        }
    }

    struct FloodNode {
        public Point Position;
        public int ScentMagnitude;

        public FloodNode(Point position, int scentMagnitude) {
            Position = position;
            ScentMagnitude = scentMagnitude;
        }
    }
}