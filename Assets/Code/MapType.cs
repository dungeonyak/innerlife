namespace Application {
    public enum MapType {
        Graveyard,
        House,
        Pasture,
        Snake,
        Wilderness,
        Nest,
        Mountain,
    }
}