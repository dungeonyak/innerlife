using System;
using System.Linq;
using UnityEngine;

namespace Application {
    public class SimpleBuilder : MapBuilder {
        protected Randomizer Random;
        protected Entity Holder;
        protected Map Map;
        protected MapType MapType;

        public SimpleBuilder(Entity holder, MapType mapType) {
            Random = Container.Resolve<Randomizer>();
            Holder = holder;
            MapType = mapType;
        }

        public Map Build() {
            var dimensions = ChooseDimensions();
            Map = new Map(Holder, MapType, dimensions.X, dimensions.Y);

            BuildMap();
            EnsureCrown();

            return Map;   
        }

        protected virtual void BuildMap() {
            BeforeTerrain();
            BuildTerrain();
            AfterTerrain();

            Populate();
            SprinkleFood();

            if (Holder != null) {
                PlaceDoors();
            }
        }

        private void EnsureCrown() {
            if (Holder == null || Holder.Has<CrownComponent>()) {
                var entityFinder = Container.Resolve<EntityFinder>();

                if (GameState.CrownsCreated >= Constants.MinVictoryDepth && Random.PercentChance(50)) {
                    var victoryFood = Container.Resolve<EntityBuilder>().Food(FoodType.Victory);
                    var foodPosition = MapUtils.FindPoint(Map, p => Movement.CanUneventfullyOccupy(victoryFood, new Placement(Map, p)));

                    if (foodPosition.HasValue) {
                        Movement.Place(victoryFood, new Placement(Map, foodPosition.Value));
                    } else {
                        // This is an unfair state, so just give it to them.
                        Utils.GetAvatar().Add(new VictoryComponent());
                    }
                } else {
                    var existingCrown = Map.PlacedEntities.Where(e => e.Has<CrownComponent>());

                    if (!existingCrown.Any()) {
                        var entityToCoronate = Map.PlacedEntities
                            .Where(e => e.Has<HnauComponent>())
                            .InRandomOrder()
                            .FirstOrDefault();

                        if (entityToCoronate == null) {
                            throw new Exception("Generated bad world!!"); // TODO: force one in there
                        }

                        entityToCoronate.Add(new CrownComponent());

                        GameState.CrownsCreated++;
                    }
                }
            }
        }

        protected virtual void BeforeTerrain() {
            //
        }

        protected virtual void AfterTerrain() {
            //
        }

        protected virtual Point ChooseDimensions() {
            return new Point(9, 9);
        }

        protected virtual void BuildTerrain() {
            foreach (var point in Map.Points) {
                var tile = Map.IsOnEdge(point)
                    ? Tile.Void
                    : SelectTile(point);

                Map.SetTileAt(point, tile);
            }
        }

        protected virtual int GetEntityCount() {
            return 0;
        }

        private void Populate() {
            int entityCount = GetEntityCount();

            for (int i = 0; i < entityCount; i++) {
                var guy = CreateThematicAnimal();

                var spot = MapUtils.FindPoint(Map, p => Movement.CanUneventfullyOccupy(guy, new Placement(Map, p)));

                if (spot.HasValue) {
                    Movement.Place(guy, new Placement(Map, spot.Value));
                } else {
                    Debug.LogWarning("Can't find place for guy!");
                }
            }
        }

        protected virtual Entity CreateThematicAnimal() {   
            var species = Random.Choice(new [] {
                Species.Cow,
            });

            return Container.Resolve<EntityBuilder>().Animal(species);
        }

        private void SprinkleFood() {
            var count = ChooseFoodCount();

            for (var i = 0; i < count; i++) {
                var food = Container.Resolve<EntityBuilder>().Food(ChooseFoodType());

                var foodSpot = MapUtils.FindPoint(Map, p => Movement.CanUneventfullyOccupy(food, new Placement(Map, p)));

                if (foodSpot.HasValue) {
                    Movement.Place(food, new Placement(Map, foodSpot.Value));
                } else {
                    Debug.LogWarning("Can't find place for food!");
                }
            }
        }

        protected virtual int ChooseFoodCount() {
            return 1;
        }

        protected virtual Tile SelectTile(Point point) {
            return Tile.Floor;
        }

        private void PlaceDoors() {
            PlaceDoor(ChooseDoorPosition(Point.Down), Point.Down);
            PlaceDoor(ChooseDoorPosition(Point.Up), Point.Up);
            PlaceDoor(ChooseDoorPosition(Point.Left), Point.Left);
            PlaceDoor(ChooseDoorPosition(Point.Right), Point.Right);
        }

        private void PlaceDoor(Point position, Point direction) {
            var door = new Entity();
            door.Name = "Door";
            door.Add(new DoorComponent(direction));
            Movement.Place(door, new Placement(Map, position));

            Map.SetTileAt(position, Tile.Void);
        }

        protected virtual Point ChooseDoorPosition(Point direction) {
            if (direction == Point.Right) {
                return new Point(Map.Width - 1, Map.Height / 2);
            } else if (direction == Point.Left) {
                return new Point(0, Map.Height / 2);
            } else if (direction == Point.Up) {
                return new Point(Map.Width / 2, Map.Height - 1);
            } else if (direction == Point.Down) {
                return new Point(Map.Width / 2, 0);
            }

            throw new Exception($"Bad door direction {direction}");
        }

        protected virtual FoodType ChooseFoodType() {
            return Random.Choice(new[] {
                FoodType.Apple,
                FoodType.Orange,
                FoodType.Tomato,
                FoodType.Gourd,
                FoodType.Pepper,
                FoodType.Lettuce,
                FoodType.Radish,
                FoodType.Carrot,
                FoodType.Banana,
                FoodType.Meat,
            });
        }
    }
}