using System;
using System.Linq;
using UnityEngine;

namespace Application {
    public interface MapBuilder {
        Map Build();
    }
}