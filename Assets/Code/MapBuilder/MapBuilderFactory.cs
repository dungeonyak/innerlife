using System;

namespace Application {
    public static class MapBuilderFactory {
        public static MapBuilder GetBuilder(Entity holder) {
            var type = ResolveType(holder);

            switch (type) {
                case MapType.Graveyard: return new GraveyardBuilder(holder);
                case MapType.Pasture: return new PastureBuilder(holder);
                case MapType.Snake: return new SnakeBuilder(holder);
                case MapType.House: return new HouseBuilder(holder);
                case MapType.Wilderness: return new WildernessBuilder(holder);
                case MapType.Nest: return new NestBuilder(holder);
                case MapType.Mountain: return new MountainBuilder(holder);
            }

            throw new Exception($"Don't know how to build map of type {Enum.GetName(typeof(MapType), type)}");
        }

        private static MapType ResolveType(Entity holder) {
            if (holder == null) {
                return MapType.Graveyard;
            } else if (holder.Has<PlayerControlledComponent>()) {
                return MapType.House;
            } else if (holder.MaybeGet<SpeciesComponent>() is SpeciesComponent sc) {
                var species = sc.Species;

                // TODO: put on species itself?
                if (species == Species.Cow || species == Species.Elephant) {
                    return MapType.Pasture;
                } else if (species == Species.Snake) {
                    return MapType.Snake;
                } else if (species == Species.Spider/* || species == Species.Gadfly*/) {
                    return MapType.Nest;
                } else if (species == Species.Ram/* || species == Species.Hawk*/) {
                    return MapType.Mountain;
                } else if (species == Species.Slime) {
                    return MapType.Graveyard;
                }
            }

            return MapType.Wilderness;
        }
    }
}