using System;
using System.Linq;
using UnityEngine;

namespace Application {
    public class PastureBuilder : SimpleBuilder {
        private Point PerlinOffset;

        public PastureBuilder(Entity holder)
            : base(holder, MapType.Pasture) {
            PerlinOffset = new Point(
                Random.Range(100000),
                Random.Range(100000)
            );
        }

        protected override void AfterTerrain() {
            PlaceTrees();
        }

        private void PlaceTrees() {
            var treeCount = (Map.Width * Map.Height) / 20;

            for (var i = 0; i < treeCount; i++) {
                var tree = Container.Resolve<EntityBuilder>().Decor("Tree", DawnLike.Still("Objects/Tree0", 19));

                var position = MapUtils.FindPoint(Map, p => Map.GetTileAt(p) == Tile.Grass);

                if (position.HasValue) {
                    Movement.Place(tree, new Placement(Map, position.Value));
                }
            }
        }

        protected override Point ChooseDimensions() {  
            return new Point(19, 19);
        }

        protected override int GetEntityCount() {
            return Random.Range(2, 5);
        }

        protected override Entity CreateThematicAnimal() {   
            var species = Random.Choice(new [] {
                Species.Cow,
                Species.Cow,
                Species.Cow,
                Species.Cow,
                Species.Cow,
                Species.Ram,
                Species.Ram,
                Species.Ram,
                Species.Ram,
                Species.Ram,
                Species.Elephant,
                Species.Elephant,
                Species.Rabbit,
                Species.Rabbit,
                Species.Snake,
                Species.Owl,
                Species.Rat,
                Species.Gadfly,
                Species.Gadfly,
                Species.Gadfly,
                Species.Gadfly,
            });

            return Container.Resolve<EntityBuilder>().Animal(species);
        }

        protected override int ChooseFoodCount() {
            return 10;
        }

        protected override Tile SelectTile(Point point) {
            var depth = Mathf.PerlinNoise(
                PerlinOffset.X + 0.2f * point.X,
                PerlinOffset.Y + 0.2f * point.Y
            );

            return depth < 0.1 ? Tile.Water : Tile.Grass;
        }

        protected override FoodType ChooseFoodType() {
            return FoodType.Meat;
        }
    }
}