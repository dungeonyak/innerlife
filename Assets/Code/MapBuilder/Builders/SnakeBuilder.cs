using System;
using System.Linq;
using UnityEngine;

namespace Application {
    public class SnakeBuilder : SimpleBuilder {
        private Point Orientation;

        public SnakeBuilder(Entity holder)
            : base(holder, MapType.Snake) {
        }

        protected override void AfterTerrain() {
            var entityBuilder = Container.Resolve<EntityBuilder>();

            for (var x = 1; x < Map.Width - 1; x++) {
                var progress = (float) x / Map.Width;

                int wavePosition = (int) (
                    Map.Height / 2 +
                    (Map.Height / 2) * Math.Sin(
                        -Math.PI / 2 + (Math.PI * 2 + Math.PI / 2) * 2 * progress
                    )
                );

                for (var y = 1; y < Map.Height - 1; y++) {
                    if (Math.Abs(y - wavePosition) > 1 && Random.PercentChance(80)) {
                        var rockAnimation = Random.Choice(new[] {
                            DawnLike.Still("Items/Rock", 6),
                            DawnLike.Still("Items/Rock", 7),
                        });
    
                        var rock = entityBuilder.Decor("Rock", rockAnimation);

                        Movement.Place(rock, new Placement(Map, new Point(x, y)));
                    } else if (Random.PercentChance(2)) {
                        var boneAnimation = DawnLike.Animation("Objects/Decor", Random.Choice(new[] {
                            94, 95, 100, 101
                        }));

                        var bones = entityBuilder.Decor("Bones", boneAnimation);

                        Movement.Place(bones, new Placement(Map, new Point(x, y)));
                    }
                }
            }
        }

        protected override Point ChooseDimensions() {  
            Orientation = Random.PercentChance(100)
                ? new Point(1, 0)
                : new Point(0, 1);

            var longDimension = Random.Range(24, 36) * 2 - 1;
            var shortDimension = Random.Range(5, 10) * 2 - 1;
            return Orientation.X != 0
                ? new Point(longDimension, shortDimension)
                : new Point(shortDimension, longDimension);
        }

        protected override int GetEntityCount() {
            return Random.Range(5, 10);
        }

        protected override Entity CreateThematicAnimal() {   
            var species = Random.Choice(new [] {
                Species.Rabbit,
                Species.Rabbit,
                Species.Rat,
                Species.Rat,
                Species.Snake,
                Species.Snake,
                Species.Spider,
                Species.Spider,
            });

            return Container.Resolve<EntityBuilder>().Animal(species);
        }

        protected override int ChooseFoodCount() {
            return 1;
        }

        protected override Tile SelectTile(Point point) {
            return Tile.Scales;
        }

        protected override Point ChooseDoorPosition(Point direction) {
            if (direction == Point.Left) {
                return new Point(0, 1);
            } else if (direction == Point.Down) {
                return new Point(1, 0);
            } else if (direction == Point.Up) {
                return new Point(Map.Width - 2, Map.Height - 1);
            } else if (direction == Point.Right) {
                return new Point(Map.Width - 1, Map.Height - 2);
            }

            throw new Exception($"Bad door direction {direction}");
        }
    }
}