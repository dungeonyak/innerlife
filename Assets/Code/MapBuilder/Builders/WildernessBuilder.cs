using System;
using System.Linq;
using UnityEngine;

namespace Application {
    public class WildernessBuilder : SimpleBuilder {
        private Point PerlinOffset;

        public WildernessBuilder(Entity holder)
            : base(holder, MapType.Wilderness) {
            PerlinOffset = new Point(
                Random.Range(100000),
                Random.Range(100000)
            );
        }

        protected override void AfterTerrain() {
            PlaceTrees();
        }

        private void PlaceTrees() {
            foreach (var point in Map.Points) {
                var depth = Mathf.PerlinNoise(
                    PerlinOffset.X + 0.2f * point.X,
                    PerlinOffset.Y + 0.2f * point.Y
                );

                if (depth > 0.7 && Map.GetTileAt(point) == Tile.Grass) {
                    var tree = Container.Resolve<EntityBuilder>().Decor("Tree", DawnLike.Still("Objects/Tree0", 19));
                    Movement.Place(tree, new Placement(Map, point));
                }
            }
        }

        protected override Point ChooseDimensions() {  
            return new Point(13, 13);
        }

        protected override int GetEntityCount() {
            return Random.Range(2, 5);
        }

        protected override Entity CreateThematicAnimal() {   
            var species = Random.Choice(new [] {
                Species.Cow,
                Species.Cow,
                Species.Cow,
                Species.Cow,
                Species.Hawk,
                Species.Rabbit,
                Species.Rabbit,
                Species.Snake,
                Species.Snake,
                Species.Spider,
                Species.Spider,
                Species.Dog,
                Species.Rat,
                Species.Rat,
                Species.Elephant,
                Species.Slime,
            });

            return Container.Resolve<EntityBuilder>().Animal(species);
        }
        protected override int ChooseFoodCount() {
            return Random.Range(1, 4);
        }

        protected override Tile SelectTile(Point point) {
            return GetDepth(point) < 0.4 ? Tile.Water : Tile.Grass;
        }

        private float GetDepth(Point point) {
            return Mathf.PerlinNoise(
                PerlinOffset.X + 0.2f * point.X,
                PerlinOffset.Y + 0.2f * point.Y
            );
        }
    }
}