using System;
using System.Linq;
using UnityEngine;

namespace Application {
    public class MountainBuilder : SimpleBuilder {
        private BspTree BspTree;
        private Point PerlinOffset;

        public MountainBuilder(Entity holder)
            : base(holder, MapType.Mountain)
        {
            PerlinOffset = new Point(
                Random.Range(100000),
                Random.Range(100000)
            );
        }

        protected override void BeforeTerrain() {
            BspTree = new BspTree(Map.Dimensions, 15);
        }

        protected override void BuildTerrain() {
            Map.Points.ForEach(p => Map.SetTileAt(p, Tile.Pit));

            BspTree.Traverse(node => {
                if (node.IsLeaf) {
                    HollowOutPass(node.Rectangle);
                }
            });

            EnsureDoorFloors();

            Map.Points.ForEach(p => {
                if (Map.IsOnEdge(p)) {
                    Map.SetTileAt(p, Tile.Void);
                }
            });
        }

        private void HollowOutPass(Rectangle rect) {
            Walk(rect.BottomLeft + Point.Square(1), rect.BottomRight + new Point(-1, 1));
            Walk(rect.BottomLeft + Point.Square(1), rect.TopLeft + new Point(1, 0));
        }

        private void Walk(Point start, Point end) {
            var step = (end - start).Sign();

            var magnitude = 0;
            for (var current = start; current != end + step; current += step) {
                if (Random.PercentChance(50)) {
                    magnitude = (magnitude != 0) ? 0 : Random.Range(-1, 2);
                }
                var offset = Point.Square(magnitude).Along(step.Orientation.Opposite);
                var middle = current + offset;
                
                if (Map.InBounds(middle)) {
                    PutFloorAt(middle);
                }

                var side1 = middle + Point.Square(1).Along(step.Orientation.Opposite);
                var side2 = middle - Point.Square(1).Along(step.Orientation.Opposite);

                if (Map.InBounds(side1)) {
                    PutFloorAt(side1);
                }

                if (Map.InBounds(side2)) {
                    PutFloorAt(side2);
                }
            }
        }

        private void PutFloorAt(Point point) {
            Map.SetTileAt(point, Tile.Floor);

            if (GetDepth(point) > 0.6 && Random.PercentChance(30)) {
                var rockAnimation = Random.Choice(new[] {
                    DawnLike.Still("Items/Rock", 6),
                    DawnLike.Still("Items/Rock", 7),
                });

                var rock = Container.Resolve<EntityBuilder>().Decor("Rock", rockAnimation);

                Movement.Place(rock, new Placement(Map, point));
            } 
        }

        protected override void BuildMap() {
            base.BuildMap();
            EnsureDoorFloors();
        }

        private void EnsureDoorFloors() {
            Map.PlacedEntities.Where(e => e.Has<DoorComponent>()).ForEach(door => {
                var doorDirection = door.Get<DoorComponent>().Direction;
                var entrySpot = door.Pos() - doorDirection;
                Map.SetTileAt(entrySpot, Tile.Floor);
                Map.SetTileAt(entrySpot + Point.Square(1).Along(doorDirection.Orientation.Opposite), Tile.Floor);
                Map.SetTileAt(entrySpot + Point.Square(-1).Along(doorDirection.Orientation.Opposite), Tile.Floor);
            });
        }

        protected override void AfterTerrain() {
            // TODO: add rocks
        }

        protected override Point ChooseDimensions() {  
            return new Point(25, 25);
        }

        protected override int GetEntityCount() {
            return Random.Range(5, 10);
        }

        protected override Entity CreateThematicAnimal() {   
            var species = Random.Choice(new [] {
                Species.Ram,
                Species.Ram,
                Species.Ram,
                Species.Hawk,
                Species.Hawk,
                Species.Hawk,
                Species.Snake,
            });

            return Container.Resolve<EntityBuilder>().Animal(species);
        }

        protected override int ChooseFoodCount() {
            return Random.Range(4, 8);
        }

        private float GetDepth(Point point) {
            return Mathf.PerlinNoise(
                PerlinOffset.X + 0.2f * point.X,
                PerlinOffset.Y + 0.2f * point.Y
            );
        }
    }
}