using System;
using System.Linq;
using UnityEngine;

namespace Application {
    public class HouseBuilder : SimpleBuilder {
        public HouseBuilder(Entity holder)
            : base(holder, MapType.House) {
        }

        protected override void AfterTerrain() {
            var entityBuilder = Container.Resolve<EntityBuilder>();

            var table = entityBuilder.Decor("Table", DawnLike.Still("Objects/Decor0", 57));
            var tablePosition = new Point(Map.Width / 2, Map.Height / 2);
            Movement.Place(table, new Placement(Map, tablePosition));

            var leftChair = entityBuilder.Decor("Chair", DawnLike.Still("Objects/Decor0", 58));
            leftChair.Add(new FacingComponent(Facing.Right));
            Movement.Place(leftChair, new Placement(Map, tablePosition - Point.Horizontal(1)));

            var rightChair = entityBuilder.Decor("Chair", DawnLike.Still("Objects/Decor0", 58));
            rightChair.Add(new FacingComponent(Facing.Left));
            Movement.Place(rightChair, new Placement(Map, tablePosition + Point.Horizontal(1)));

            var bed = entityBuilder.Decor("Bed", DawnLike.Still("Objects/Decor0", 71));
            Movement.Place(bed, new Placement(Map, new Point(Map.Width - 2, Map.Height - 2)));
        }

        protected override Point ChooseDimensions() {
            return new Point(9, 9);
        }

        protected override int GetEntityCount() {
            return 0;
        }

        protected override int ChooseFoodCount() {
            return 1;
        }

        protected override Tile SelectTile(Point point) {
            return Tile.Floor;
        }
    }
}