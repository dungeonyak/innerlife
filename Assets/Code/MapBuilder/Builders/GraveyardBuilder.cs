using System;
using System.Linq;
using UnityEngine;

namespace Application {
    public class GraveyardBuilder : SimpleBuilder {
        public GraveyardBuilder(Entity holder)
            : base(holder, MapType.Graveyard) {
            
        }
        
        protected override void AfterTerrain() {
            if (Holder == null) {
                SpawnKeeper();
            }

            PlaceGraves();
            PlaceTrees();
        }

        private void PlaceGraves() {
            var graveCount = (Map.Width * Map.Height) / 10;

            for (var i = 0; i < graveCount; i++) {
                var index = Container.Resolve<Randomizer>().Choice(new[] {
                    124,
                    125,
                    126,
                    127,
                    128,
                    129,
                    130,
                    131,
                    132,
                    133,
                    85,
                });
    
                var grave = Container.Resolve<EntityBuilder>().Decor("Grave", DawnLike.Animation("Objects/Decor", index));

                var gravePoint = MapUtils.FindPoint(Map, p => Movement.CanUneventfullyOccupy(grave, new Placement(Map, p)));

                if (gravePoint.HasValue) {
                    Movement.Place(grave, new Placement(Map, gravePoint.Value));
                }
            }
        }

        private void PlaceTrees() {
            var min = (Map.Width * Map.Height) / 50;
            var treeCount = Random.Range(min - 1, min + 3);

            for (int i = 0; i < treeCount; i++) {
                var tree = Container.Resolve<EntityBuilder>().Decor("Tree", DawnLike.Still("Objects/Tree0", 51));

                var position = MapUtils.FindPoint(Map, p => Movement.CanUneventfullyOccupy(tree, new Placement(Map, p)));

                if (position.HasValue) {
                    Movement.Place(tree, new Placement(Map, position.Value));
                }
            }
        }

        private void SpawnKeeper() {
            var keeper = Container.Resolve<EntityBuilder>().Animal(Species.Owl);
            keeper.Add(new CrownComponent());
            Movement.Place(keeper, new Placement(Map, new Point(Map.Width / 2 + 1, Map.Height / 2 + 2)));
        }

        protected override Point ChooseDimensions() {
            if (Holder == null) {
                return new Point(63, 63);
            } else {
                return new Point(15, 15);
            }
        }

        protected override int GetEntityCount() {
            return Map.Width / 6;
        }

        protected override int ChooseFoodCount() {
            return 0;
        }

        protected override Tile SelectTile(Point point) {
            return Tile.Grass;
        }

        protected override Entity CreateThematicAnimal() {   
            var species = Random.Choice(new [] {
                Species.Rat,
                Species.Rat,
                Species.Rat,
                Species.Spider,
                Species.Spider,
                Species.Spider,
                Species.Slime,
            });

            return Container.Resolve<EntityBuilder>().Animal(species);
        }
    }
}