using System;
using System.Linq;
using UnityEngine;

namespace Application {
    public class NestBuilder : SimpleBuilder {
        private BspTree BspTree;

        public NestBuilder(Entity holder)
            : base(holder, MapType.Nest)
        {
        }

        protected override void BeforeTerrain() {
            var bspRect = new Rectangle(
                Point.Zero,
                Map.Dimensions - Point.Square(1)
            );

            BspTree = new BspTree(bspRect, 8);
        }

        protected override void BuildTerrain() {
            Map.Points.ForEach(p => Map.SetTileAt(p, Tile.Wall));

            BspTree.Traverse(node => {
                if (node.IsLeaf) {
                    HollowOutRoom(node.Rectangle);
                } else {
                    Connect(node.FirstChild.Rectangle, node.SecondChild.Rectangle);
                }
            });

            Map.Points.ForEach(p => {
                if (Map.IsOnEdge(p)) {
                    Map.SetTileAt(p, Tile.Void);
                }
            });
        }

        private void HollowOutRoom(Rectangle rect) {
            var roomRect = new Rectangle(rect.Position + Point.Square(1), rect.Dimensions - Point.Square(1));

            foreach (var point in roomRect.Points) {
                Map.SetTileAt(point, Tile.Floor);

                if (Random.PercentChance(30)) {
                    if (point.X == roomRect.Left) {
                        if (point.Y == roomRect.Top - 1) {
                            PlaceWeb(point, "TopLeft");
                        } else if (point.Y == roomRect.Bottom) {
                            PlaceWeb(point, "BottomLeft");
                        }
                    } else if (point.X == roomRect.Right - 1) {
                        if (point.Y == roomRect.Top - 1) {
                            PlaceWeb(point, "TopRight");
                        } else if (point.Y == roomRect.Bottom) {
                            PlaceWeb(point, "BottomRight");
                        }
                    }
                }
            }
        }

        private void PlaceWeb(Point point, string assetName) {
            var web = Container.Resolve<EntityBuilder>().Decor(
                "cobweb",
                DawnLike.Still("Objects/Decor0", $"Web{assetName}"),
                solid: false
            );

            Movement.Place(web, new Placement(Map, point));
        }

        private void Connect(Rectangle rect1, Rectangle rect2) {
            var delta = (rect2.Center - rect1.Center).Sign();

            for (var point = rect1.Center; point != rect2.Center + delta; point += delta) {
                Map.SetTileAt(point, Tile.Floor);
            }
        }

        protected override Point ChooseDimensions() {  
            return new Point(25, 25);
        }

        protected override int GetEntityCount() {
            return Random.Range(10, 20);
        }

        protected override Entity CreateThematicAnimal() {   
            var species = Random.Choice(new [] {
                Species.Rabbit,
                Species.Rat,
                Species.Rat,
                Species.Rat,
                Species.Spider,
                Species.Spider,
                Species.Spider,
                Species.Gadfly,
                Species.Gadfly,
                Species.Gadfly,
                Species.Gadfly,
                Species.Gadfly,
                Species.Slime,
                Species.Slime,
            });

            return Container.Resolve<EntityBuilder>().Animal(species);
        }

        protected override int ChooseFoodCount() {
            return Random.Range(4, 8);
        }
    }
}