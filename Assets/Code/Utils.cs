using System;
using System.Linq;
using UnityEngine;

namespace Application {
    public static class Utils {
        public static void EnsureInventory(Entity entity) {
            if (entity.MaybeGet<InventoryComponent>() == null) {
                var innerMap = MapBuilderFactory.GetBuilder(entity).Build();

                entity.Add(new InventoryComponent(innerMap));
            }
        }

        public static bool IsInterestedIn(Entity entity, Entity item) {
            if (!entity.Has<HnauComponent>()) {
                return false;
            }

            if (entity.IsAvatar()) {
                return true;
            }

            if (item.Has<FoodComponent>()) {
                return entity.MaybeGet<DietComponent>() is DietComponent dietComponent
                    && Utils.DietIncludes(dietComponent.Diet, item)
                    && !(entity.Has<ScaryComponent>() && entity.GetMap()?.Holder == Utils.GetAvatar());
            }

            return false;
        }

        public static bool DietIncludes(Diet diet, Entity food) {
            if (diet == Diet.HardcoreOmnivore) {
                return food.Has<FoodComponent>()
                    || food.Has<SolidDecorComponent>()
                    || food.Has<HnauComponent>();
            }

            var foodComponent = food.MaybeGet<FoodComponent>();

            if (foodComponent == null) {
                return false;
            }

            return diet == Diet.Omnivore
                || diet == Diet.Herbivore && !foodComponent.FoodType.IsMeat
                || diet == Diet.Carnivore && foodComponent.FoodType.IsMeat;
        }

        public static bool DietIncludes(Entity eater, Entity food) {
            return eater.MaybeGet<DietComponent>() is DietComponent dietComponent
                && Utils.DietIncludes(dietComponent.Diet, food);
        }

        public static bool ConflictsWith(Entity mover, Entity occupant) {
            return occupant.Has<HnauComponent>()
                || occupant.Has<SolidDecorComponent>()
                || occupant.Has<ItemComponent>() && !IsInterestedIn(mover, occupant);
        }

        public static bool AnyTweensExist() {
            var entityFinder = Container.Resolve<EntityFinder>();

            return entityFinder.WhereHas<WalkTweenComponent>().Any()
                || entityFinder.WhereHas<PickedUpTweenComponent>().Any()
                || entityFinder.WhereHas<TravelTweenComponent>().Any()
                || entityFinder.WhereHas<ShakeTweenComponent>().Any()
                || entityFinder.WhereHas<AttackTweenComponent>().Any()
                || entityFinder.WhereHas<OngoingDialogueComponent>().Any();
        }

        public static bool CannotAct(Entity entity) {
            return entity.Has<StunComponent>();
        }

        public static bool CannotMove(Entity entity) {
            return entity.Has<StunComponent>() || entity.Has<WebbedComponent>();
        }

        public static bool HasNoFood(Entity entity) {
            var inventory = entity.MaybeGet<InventoryComponent>();

            if (inventory == null) {
                return false;
            }

            var food = inventory.Map.PlacedEntities.Where(e => e.Has<FoodComponent>());
            return !food.Any();
        }

        public static bool IsDead(Entity entity) {
            return entity.Has<DeadComponent>();
        }

        public static bool IsFlying(Entity entity) {
            return Utils.IsDead(entity) || entity.HasAbility(Ability.Flight);
        }

        public static bool IsInWater(Entity entity) {
            return entity.GetMap() is Map map && map.GetTileAt(entity.Pos()) == Tile.Water;
        }

        public static Entity GetAvatar() {
            return Container.Resolve<EntityFinder>()
                .WhereHas<PlayerControlledComponent>()
                .FirstOrDefault();
        }

        public static void Face(Entity entity, Point direction) {
            if (entity.MaybeGet<FacingComponent>() is FacingComponent facingComponent) {
                if (direction.X > 0) {
                    facingComponent.Facing = Facing.Right;
                } else if (direction.X < 0) {
                    facingComponent.Facing = Facing.Left;
                }
            }
        }

        public static bool ReadyForCommand() {
            if (Utils.GetAvatar() is Entity avatar) {
                if (avatar.IsDead() || avatar.Has<RanCommandComponent>()) {
                    return false;
                }
            }

            var interfaceStuffExists = Container.Resolve<EntityFinder>()
                .WhereHas<TargetingComponent>()
                .Any();

            return !interfaceStuffExists && !Utils.AnyTweensExist();
        }

        public static void MarkCommandRun() {
            if (Utils.GetAvatar() is Entity avatar && !avatar.Has<RanCommandComponent>()) {
                avatar.Add(new RanCommandComponent());
            }
        }

        public static Point? TranslateDirectionKey(KeyCode key) {
            switch (key) {
                case KeyCode.W:
                case KeyCode.K:
                case KeyCode.UpArrow:
                    return Point.Up;
                case KeyCode.A:
                case KeyCode.H:
                case KeyCode.LeftArrow:
                    return Point.Left;
                case KeyCode.S:
                case KeyCode.J:
                case KeyCode.DownArrow:
                    return Point.Down;
                case KeyCode.D:
                case KeyCode.L:
                case KeyCode.RightArrow:
                    return Point.Right;
            }

            return null;
        }

        public static bool IsBashable(Entity e) {
            return true;
        }

        public static void AddWalkTween(Entity entity, Point origin) {
            if (entity.MaybeGet<WalkTweenComponent>() is WalkTweenComponent existingTween) {
                existingTween.SecondOrigin = origin;
            } else {
                entity.Add(new WalkTweenComponent() { WorldOrigin = origin });
            }
        }

        public static bool IsGround(Tile tile) {
            return tile == Tile.Floor
                || tile == Tile.Grass
                || tile == Tile.Scales;
        }

        public static bool IsSolid(Tile tile) {
            return tile == Tile.Void
                || tile == Tile.Wall
                || tile == Tile.Pit;
        }

        public static int GetDepth(Entity entity) {
            var depth = 0;

            while (entity.GetMap().Holder != null) {
                entity = entity.GetMap().Holder;
                depth++;
            }

            return depth;
        }

        public static Entity FindDoor(Map map, Point direction) {
            return map.PlacedEntities.FirstOrDefault(e => e.MaybeGet<DoorComponent>()?.Direction == direction);
        }

        public static Entity FindDoor(Entity entity, Point direction) {
            var inventory = entity.MaybeGet<InventoryComponent>();

            if (inventory != null) {
                return FindDoor(inventory.Map, direction);
            }

            return null;
        }

        public static void Stun(Entity entity) {
            if (!entity.Has<StunComponent>()) {
                entity.Add(new StunComponent());
            }
        }

        public static void ApplyIntrinsics(Entity entity) {
            if (entity.MaybeGet<IntrinsicAbilityComponent>() is IntrinsicAbilityComponent intrinsics) {
                foreach (var ability in intrinsics.Abilities) {
                    entity.EnsureAbility(ability);
                }
            }
        }

        public static bool IsOnVictoryPath(Map map) {
            return map.Holder is Entity holder && holder.Has<CrownComponent>();
        }

        public static bool AvatarCanSee(Entity target) {
            var targetMap = target.GetMap();

            return targetMap != null && Utils.GetAvatar() is Entity avatar && (
                targetMap == avatar.GetMap() && target.Pos().Chebyshev(avatar.Pos()) < 7
                || targetMap == avatar.MaybeGet<InventoryComponent>()?.Map
            );
        }
    }
}