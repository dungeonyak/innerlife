﻿using System;

namespace Application {
    public struct Placement {
        public readonly Map Map;
        public readonly Point Position;

        public Placement(Map map, Point point) {
            Map = map;
            Position = point;
        }
    }
}
