﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Application {
    public static class Movement {
        public static void WalkTo(Entity entity, Point position) {
            var map = entity.Get<PlacementComponent>().Placement.Map;

            var pushee = map.GetEntitiesAt(position)
                .Where(e => Utils.ConflictsWith(entity, e))
                .FirstOrDefault();

            if (pushee == null || Push(entity, pushee)) {
                var p = entity.Get<PlacementComponent>();
                var oldPosition = p.Position;
                Movement.Place(entity, new Placement(p.Map, position));
                Utils.AddWalkTween(entity, oldPosition);

                Utils.Face(entity, position - oldPosition);
            } else {
                Swap(entity, pushee);
            }
        }

        public static void WalkInto(Entity traveler, Entity holder) {
            Movement.Enter(traveler, holder);

            if (holder.IsAvatar()) {
                Messages.Write($"{traveler.Describe()} enters you.");
            } else if (traveler.IsAvatar()) {
                Messages.Write($"You enter {holder.Describe()}.");
            }
        }

        public static void WalkOutOf(Entity traveler, Entity door) {
            var holder = door.GetMap().Holder;

            Movement.Exit(traveler, door);

            if (holder.IsAvatar()) {
                Messages.Write($"{traveler.Describe()} leaves you.");
            } else if (traveler.IsAvatar()) {
                Messages.Write($"You leave {holder.Describe()}.");
            }
        }

        public static void Enter(Entity traveler, Entity holder) {
            Utils.EnsureInventory(holder);

            var travelDirection = holder.Get<PlacementComponent>().Position
                - traveler.Get<PlacementComponent>().Position;

            var destMap = holder.Get<InventoryComponent>().Map;
            var entryDoor = destMap.PlacedEntities
                .Where(e => e.Has<DoorComponent>())
                .Where(door => door.Get<DoorComponent>().Direction == -travelDirection)
                .FirstOrDefault();

            if (entryDoor == null) {
                Debug.LogWarning("No appropriate entry door!");
                entryDoor = destMap.PlacedEntities
                    .Where(e => e.Has<DoorComponent>())
                    .First();

                // TODO: do something better than this crappy solution
                travelDirection = -entryDoor.Get<DoorComponent>().Direction;
            }

            var destPos = entryDoor.Pos() + travelDirection;

            var blocker = destMap.GetEntitiesAt(destPos)
                .FirstOrDefault();

            if (blocker == null || Push(entryDoor.Pos(), blocker)) {
                var oldPlacement = traveler.Get<PlacementComponent>().Placement;

                Movement.Place(traveler, new Placement(destMap, destPos));

                traveler.Add(new TravelTweenComponent() {
                    OldPlacement = oldPlacement,
                    EmbarkGate = holder,
                    DisembarkGate = entryDoor,
                });
            } else {
                Swap(traveler, blocker);
            }

            Utils.Stun(traveler);

            Utils.Face(traveler, travelDirection);
        }

        public static void Exit(Entity traveler, Entity door) {
            var holder = door.Get<PlacementComponent>().Map.Holder;
            var destMap = holder.Get<PlacementComponent>().Map;
            var travelDirection = door.Get<DoorComponent>().Direction;
            var destPos = holder.Get<PlacementComponent>().Position + travelDirection;

            var blocker = destMap.GetEntitiesAt(destPos)
                .Where(e => Utils.ConflictsWith(traveler, e)) // TODO
                .FirstOrDefault();

            if (!Movement.CanOccupyTile(traveler, new Placement(destMap, destPos))) {
                destPos = holder.Pos();
                Movement.Push(holder);
            } else if (blocker != null && !Push(holder, blocker)) {
                Debug.LogWarning("Failed to make room for exit!");
            }

            var oldPlacement = traveler.Get<PlacementComponent>().Placement;

            Movement.Place(traveler, new Placement(destMap, destPos));

            traveler.Add(new TravelTweenComponent() {
                OldPlacement = oldPlacement,
                EmbarkGate = door,
                DisembarkGate = holder,
            });

            Utils.Stun(traveler);

            Utils.Face(traveler, travelDirection);
        }

        public static void Place(Entity entity, Placement placement) {
            if (!placement.Map.InBounds(placement.Position)) {
                throw new Exception("Cannot place out of bounds!");
            }

            if (GetItemAt(placement) is Entity item
                && item != entity
                && Utils.IsInterestedIn(entity, item)) {
                Actions.PickUp(entity, item);
            }

            PlacementComponent placementComponent;
            Placement? oldPlacement = null;

            if (entity.MaybeGet<PlacementComponent>() is PlacementComponent p) {
                oldPlacement = p.Placement;

                placementComponent = p;
                p.Map.Displace(entity, p.Position);
                p.Placement = placement;
            } else {
                placementComponent = new PlacementComponent(placement);
                entity.Add(placementComponent);
            }

            placement.Map.Place(entity, placement.Position);

            if (oldPlacement == null || oldPlacement?.Map != placement.Map) {
                Container.Resolve<EventBus>().Dispatch(new EntityChangedMap(entity) {
                    NewMap = placement.Map,
                    OldMap = oldPlacement?.Map,
                });
            }
        }

        public static void Displace(Entity entity)
        {
            var placement = entity.Get<PlacementComponent>();

            placement.Map.Displace(entity, placement.Position);

            entity.Remove(placement);

            Container.Resolve<EventBus>().Dispatch(new EntityChangedMap(entity) {
                NewMap = null,
                OldMap = placement.Map,
            });
        }

        private static Entity GetItemAt(Placement placement) {
            return placement.Map.GetEntitiesAt(placement.Position)
                .Where(e => e.Has<ItemComponent>())
                .FirstOrDefault();
        }

        public static bool CanPushInto(Entity entity, Placement placement) {
            if (!CanOccupyTile(entity, placement)) {
                return false;
            }

            if (entity.HasAbility(Ability.Shove)) {
                return true;
            }

            var blocker = GetEntitiesAt(placement)
                .Where(e => e.Has<HnauComponent>() || e.Has<SolidDecorComponent>()).FirstOrDefault();

            return blocker == null;
        }

        public static bool CanPushInto(Entity entity, Point position) {
            return CanPushInto(entity, new Placement(entity.GetMap(), position));
        }

        public static bool CanStepInto(Entity entity, Placement placement) {
            var blocker = GetEntitiesAt(placement)
                .Where(e => Utils.ConflictsWith(entity, e))
                .FirstOrDefault();

            if (blocker != null) {
                return false;
            } else {
                return CanOccupyTile(entity, placement);
            }
        }

        public static bool CanUneventfullyOccupy(Entity entity, Placement placement) {
            var blocker = GetEntitiesAt(placement)
                .Where(e => e.Has<HnauComponent>() || e.Has<ItemComponent>() || e.Has<SolidDecorComponent>())
                .FirstOrDefault();

            return blocker == null && CanOccupyTile(entity, placement);
        }

        public static bool CanStepInto(Entity entity, Point position) {
            var placement = entity.Get<PlacementComponent>().Placement;
            return CanStepInto(entity, new Placement(placement.Map, position));
        }

        public static bool CanOccupyTile(Entity entity, Placement placement) {
            if (!placement.Map.InBounds(placement.Position)) {
                return false;
            }

            var tile = placement.Map.GetTileAt(placement.Position);

            // TODO: fix the meaning if IsSolid!
            return !Utils.IsSolid(tile) || tile == Tile.Pit && entity.IsFlying();
        }

        public static bool CanOccupyTile(Entity entity, Point position) {
            return CanOccupyTile(entity, new Placement(entity.GetMap(), position));
        }

        public static IEnumerable<Entity> GetEntitiesAt(Placement placement) {
            return placement.Map.GetEntitiesAt(placement.Position);
        }

        public static void Swap(Entity initiator, Entity victim) {
            var initiatorPlacement = initiator.Get<PlacementComponent>().Placement;
            var victimPlacement = victim.Get<PlacementComponent>().Placement;

            initiator.Get<PlacementComponent>().Placement = victimPlacement;
            victim.Get<PlacementComponent>().Placement = initiatorPlacement;

            victimPlacement.Map.Displace(victim, victimPlacement.Position);
            initiatorPlacement.Map.Displace(initiator, initiatorPlacement.Position);

            victimPlacement.Map.Place(initiator, victimPlacement.Position);
            initiatorPlacement.Map.Place(victim, initiatorPlacement.Position);
            
            if (initiatorPlacement.Map == victimPlacement.Map) {
                Utils.AddWalkTween(initiator, initiatorPlacement.Position);
                Utils.AddWalkTween(victim, victimPlacement.Position);
            } else {
                Container.Resolve<EventBus>().Dispatch(new EntityChangedMap(initiator) {
                    NewMap = victimPlacement.Map,
                    OldMap = initiatorPlacement.Map,
                });

                Container.Resolve<EventBus>().Dispatch(new EntityChangedMap(victim) {
                    NewMap = initiatorPlacement.Map,
                    OldMap = victimPlacement.Map,
                });
            }
        }

        public static bool PushIfNecessary(Placement location) {
            var pushee = location.Map.GetEntitiesAt(location.Position)
                .Where(e => e.Has<HnauComponent>() || e.Has<ItemComponent>())
                .FirstOrDefault();

            if (pushee != null) {
                return Push(pushee);
            } else {
                return true;
            }
        }

        public static bool Push(Entity pushee) {
            return Push((Point?) null, pushee);
        }

        public static bool Push(Entity initiator, Entity pushee) {
            return Push(initiator.Pos(), pushee);
        }

        public static bool Push(Point? initiator, Entity pushee) {
            var set = new HashSet<Point>();
            
            if (initiator != null) {
                set.Add(initiator.Value);
            };
            
            Point? direction = initiator != null
                ? (pushee.Pos() - initiator.Value).Sign()
                : (Point?) null;
    
            var success = FloodPush(pushee, set, direction);

            return success;
        }

        public static bool FloodPush(Entity pushee, ICollection<Point> blacklist, Point? preferredDirection) {
            var map = pushee.Get<PlacementComponent>().Map;
            var pusheePosition = pushee.Get<PlacementComponent>().Position;
            blacklist.Add(pusheePosition);

            foreach (var neighborPoint in pusheePosition.Neighbors.InRandomOrder()) {
                if (!blacklist.Contains(neighborPoint)) {
                    if (CanStepInto(pushee, neighborPoint)) {
                        WalkTo(pushee, neighborPoint);
                        return true;
                    }
                }
            }

            var neighbors = pusheePosition.Neighbors
                .OrderBy(n => n - pusheePosition == preferredDirection ? 0 : 1);

            foreach (var neighborPoint in neighbors) {
                if (!blacklist.Contains(neighborPoint)) {
                    blacklist.Add(neighborPoint);

                    var door = map.GetEntitiesAt(neighborPoint)
                        .Where(e => e.Has<DoorComponent>())
                        .FirstOrDefault();

                    if (door != null) {
                        Movement.Exit(pushee, door);
                        return true;
                    }

                    if (CanOccupyTile(pushee, neighborPoint)) {
                        var blocker = map.GetEntitiesAt(neighborPoint)
                            .Where(e => Utils.ConflictsWith(pushee, e)) // TODO
                            .FirstOrDefault();

                        if (blocker != null && FloodPush(blocker, blacklist, preferredDirection)) {
                            WalkTo(pushee, neighborPoint);
                            return true;
                        }
                    }
                }
            }
    
            return false;
        }
    }
}
