﻿using System;
namespace Application {
    [AttributeUsage(AttributeTargets.Field)]
    public class InjectAttribute : Attribute {
    }
}
