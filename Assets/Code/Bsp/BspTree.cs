using System;

namespace Application
{
    public class BspTree
    {
        public BspNode Root;

        public BspTree(Point dimensions, int minSplitSize)
            : this(new Rectangle(Point.Zero, dimensions), minSplitSize)
        {
        }

        public BspTree(Rectangle rectangle, int minSplitSize)
        {
            Root = new BspNode(rectangle, minSplitSize);
        }

        public void Traverse(Action<BspNode> callback)
        {
            Root.Traverse(callback);
        }
    }
}