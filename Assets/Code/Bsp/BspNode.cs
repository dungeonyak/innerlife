using System;
using UnityEngine;

namespace Application
{
    public class BspNode
    {
        public Rectangle Rectangle;

        public BspNode FirstChild;
        public BspNode SecondChild;

        public bool IsLeaf => FirstChild == null;

        private int MinSplitSize;
        private Randomizer Random;

        public BspNode(Rectangle rectangle, int minSplitSize)
        {
            Rectangle = rectangle;
            MinSplitSize = minSplitSize;
            Random = Container.Resolve<Randomizer>();

            Subdivide();
        }
        
        public void Traverse(Action<BspNode> callback) {
            callback(this);

            if (!IsLeaf) {
                FirstChild.Traverse(callback);
                SecondChild.Traverse(callback);
            }
        }

        private void Subdivide() {
            if (ChooseOrientation() is Orientation orientation) {
                var dividedDimension = Rectangle.Dimensions.Along(orientation);
                var normalDimension = Rectangle.Dimensions.Along(orientation.Opposite);

                var firstDimensions = normalDimension + dividedDimension / 2 + Point.Square(Random.PercentChance(50) ? -1 : 1).Along(orientation);
                var secondDimensions = normalDimension + dividedDimension - firstDimensions.Along(orientation);

                var rect1 = new Rectangle(
                    Rectangle.Position,
                    firstDimensions
                );

                var rect2 = new Rectangle(
                    Rectangle.Position + firstDimensions.Along(orientation),
                    secondDimensions
                );

                FirstChild = new BspNode(rect1, MinSplitSize);

                SecondChild = new BspNode(rect2, MinSplitSize);
            }
        }

        private Orientation ChooseOrientation()
        {
            var canSplitX = Rectangle.Dimensions.X >= MinSplitSize;
            var canSplitY = Rectangle.Dimensions.Y >= MinSplitSize;

            if (canSplitX && canSplitY) {
                return Random.PercentChance(50) ? Orientation.Horizontal : Orientation.Vertical;
            } else if (canSplitX) {
                return Orientation.Horizontal;
            } else if (canSplitY) {
                return Orientation.Vertical;
            }

            return null;
        }
    }
}