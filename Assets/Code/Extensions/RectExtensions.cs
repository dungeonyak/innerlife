using UnityEngine;

namespace Application {
    public static class RectExtensions {
        public static Rectangle ToRectangle(this Rect rect) {
            return new Rectangle(
                (int) rect.x,
                (int) rect.y,
                (int) rect.width,
                (int) rect.height
            );
        }
    }
}