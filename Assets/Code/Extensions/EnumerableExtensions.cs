using System;
using System.Collections.Generic;
using System.Linq;

namespace Application {
    public static class EnumerableExtensions {
        public static IEnumerable<T> InRandomOrder<T>(this IEnumerable<T> enumerable) {
            var random = Container.Resolve<Randomizer>();
            return enumerable.OrderBy(_ => random.Range(enumerable.Count()));
        }

        public static IEnumerable<T> ForEach<T>(this IEnumerable<T> enumerable, Action<T> action) {
            foreach (var item in enumerable) {
                action(item);
            }

            return enumerable;
        }
    }
}