﻿using System;
using System.Collections.Generic;

namespace Application {
    public static class DictionaryExtensions {
        public static V SetDefault<K, V>(this Dictionary<K, V> dict, K key) {
            return dict.SetDefault(key, () => default);
        }

        public static V SetDefault<K, V>(this Dictionary<K, V> dict, K key, Func<V> getValue) {
            V value;
            var got = dict.TryGetValue(key, out value);
            if (got) {
                return value;
            } else {
                var newValue = getValue();
                dict[key] = newValue;
                return newValue;
            }
        }

        public static V GetDefault<K, V>(this Dictionary<K, V> dict, K key) {
            return dict.GetDefault<K, V, V>(key, () => default);
        }

        public static D GetDefault<K, V, D>(this Dictionary<K, V> dict, K key, Func<D> getValue)
            where V : D {
            V value;
            var got = dict.TryGetValue(key, out value);
            return got ? value : getValue();
        }
    }
}
