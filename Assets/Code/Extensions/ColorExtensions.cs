using System;
using UnityEngine;

namespace Application {
    public static class ColorUtils {
        public static Color FromHex(string hex) {
            var r = Convert.ToInt32(hex.Substring(0, 2), 16) / 255f;
            var g = Convert.ToInt32(hex.Substring(2, 2), 16) / 255f;
            var b = Convert.ToInt32(hex.Substring(4, 2), 16) / 255f;

            return new Color(r, g, b);
        }

        public static Color Darken(this Color color, float amount) {
            return new Color(color.r * amount, color.g * amount, color.b * amount, color.a);
        }
    }
}