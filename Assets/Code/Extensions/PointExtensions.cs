using UnityEngine;

namespace Application {
    public static class PointExtensions {
        public static Vector2 ToVector(this Point point) {
            return new Vector2(point.X, point.Y);
        }

        public static Point ToPoint(this Vector2 vector) {
            return new Point((int) vector.x, (int) vector.y);
        }

        public static bool IsAdjacentTo(this Point point, Point other) {
            return point.Manhattan(other) == 1;
        }
    }
}