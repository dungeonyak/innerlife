namespace Application {
    public static class EntityExtensions {
        public static Map GetMap(this Entity entity) {
            return entity.Get<PlacementComponent>()?.Map;
        }

        public static Point Pos(this Entity entity) {
            return entity.GetPosition();
        }

        public static Point GetPosition(this Entity entity) {
            return entity.Get<PlacementComponent>().Position;
        }

        public static bool IsDead(this Entity entity) {
            return Utils.IsDead(entity);
        }

        public static bool IsFlying(this Entity entity) {
            return Utils.IsFlying(entity);
        }

        public static bool IsInWater(this Entity entity) {
            return Utils.IsInWater(entity);
        }

        public static bool IsAvatar(this Entity entity) {
            return entity.Has<PlayerControlledComponent>();
        }

        public static string Describe(this Entity entity) {
            var baseName = entity.Name;

            if (entity.IsDead()) {
                baseName = $"ghost of {baseName}";
            }

            return baseName.ToUpper();
        }

        public static bool HasAbility(this Entity entity, Ability ability) {
            return entity.MaybeGet<AbilityComponent>() is AbilityComponent abilityComponent
                && abilityComponent.Abilities.Contains(ability);
        }

        public static void EnsureAbility(this Entity entity, Ability ability) {
            if (!entity.Has<AbilityComponent>()) {
                entity.Add(new AbilityComponent());
            }

            var c = entity.Get<AbilityComponent>();

            if (!c.Abilities.Contains(ability)) {
                entity.Get<AbilityComponent>().Abilities.Add(ability);

                Container.Resolve<EventBus>().Dispatch(new GainedAbility(entity, ability));
            }
        }

        public static void RemoveAbility(this Entity entity, Ability ability) {
            if (entity.MaybeGet<AbilityComponent>() is AbilityComponent abilityComponent) {
                abilityComponent.Abilities.Remove(ability);

                Container.Resolve<EventBus>().Dispatch(new LostAbility(entity, ability));
            }
        }
    }
}