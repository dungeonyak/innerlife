using System;
using System.Linq;

namespace Application {
    public static class MapUtils {
        public static Point? FindPoint(Map map, Predicate<Point> check) {
            var random = Container.Resolve<Randomizer>();
            var randomTries = 3;

            for (var i = 0; i < randomTries; i++) {
                var point = new Point(
                    random.Range(1, map.Width),
                    random.Range(1, map.Height)
                );

                if (check(point)) {
                    return point;
                }
            }

            var candidates = map.Points.Where(p => check(p));

            if (candidates.Any()) {
                return random.Choice(candidates);
            } else {
                return null;
            }
        }
    }
}