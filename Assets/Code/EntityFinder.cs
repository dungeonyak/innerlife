﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Application {
    public class EntityFinder {
        private Dictionary<Type, HashSet<Entity>> EntitiesByComponentType = new Dictionary<Type, HashSet<Entity>>();

        public void ComponentAdded<ComponentType>(ComponentType component, Entity entity)
            where ComponentType : Component {
            EntitiesByComponentType.SetDefault(typeof(ComponentType), () => new HashSet<Entity>()).Add(entity);
        }

        public void ComponentRemoved(Type componentType, Entity entity) {
            EntitiesByComponentType.SetDefault(componentType, () => new HashSet<Entity>()).Remove(entity);
        }

        public IEnumerable<Entity> WhereHas<C>()
            where C : Component {
            return EntitiesByComponentType.ContainsKey(typeof(C))
                ? EntitiesByComponentType[typeof(C)]
                : Enumerable.Empty<Entity>();
        }

        public void Clear() {
            foreach (var pair in EntitiesByComponentType) {
                foreach (var entity in pair.Value.ToList()) {
                    entity.Remove(pair.Key);
                }
            }
            EntitiesByComponentType.Clear();
        }
    }
}
