﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Application {
    public class Map {
        public readonly MapType MapType;

        public readonly Entity Holder;
        public readonly int Width;
        public readonly int Height;
        public Point Dimensions => new Point(Width, Height);

        private Grid<Tile> Tiles;
        private Grid<int> Randomness;

        public IEnumerable<Entity> PlacedEntities => placedEntities;
        private HashSet<Entity> placedEntities = new HashSet<Entity>();

        private Dictionary<Point, HashSet<Entity>> PlacedEntitiesByPosition = new Dictionary<Point, HashSet<Entity>>();

        public Map(Entity holder, MapType mapType, int width, int height) {
            Holder = holder;
            MapType = mapType;
            Width = width;
            Height = height;
            Tiles = new Grid<Tile>(Width, Height, point => Tile.Floor);

            var random = Container.Resolve<Randomizer>();
            Randomness = new Grid<int>(Width, Height, _ => random.Range(100));
        }

        public void SetTileAt(Point position, Tile tile) {
            Tiles[position] = tile;
        }

        public Tile GetTileAt(Point position) {
            return Tiles[position];
        }

        public int GetRandomnessAt(Point position) {
            return Randomness[position];
        }

        public IEnumerable<Entity> GetEntitiesAt(Point position) {
            return PlacedEntitiesByPosition.GetDefault(position, () => Enumerable.Empty<Entity>());
        }

        public void Place(Entity entity, Point position) {
            placedEntities.Add(entity);
            PlacedEntitiesByPosition.SetDefault(position, () => new HashSet<Entity>()).Add(entity);
        }

        public void Displace(Entity entity, Point position) {
            placedEntities.Remove(entity);
            PlacedEntitiesByPosition[position].Remove(entity);
        }

        public bool InBounds(Point point) {
            return Tiles.InBounds(point);
        }

        public bool IsOnEdge(Point point) {
            return Tiles.IsOnEdge(point);
        }

        public IEnumerable<Point> Points => Tiles.Points;
    }
}
