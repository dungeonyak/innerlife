﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Application {
    public class Randomizer {
        private Random Random;

        public Randomizer(Random random) {
            Random = random;
        }

        public int Range(int min, int max) {
            return min + Random.Next() % (max - min);
        }

        public int Range(int max) {
            return Range(0, max);
        }

        public T Choice<T>(IEnumerable<T> options) {
            var index = Random.Next(0, options.Count());
            return options.ElementAt(index);
        }

        public bool PercentChance(int percent) {
            return Random.Next() % 100 < percent;
        }
    }
}
