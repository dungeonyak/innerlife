namespace Application {
    public class FoodType {
        public string Name;
        public IAnimation Animation;
        public RichText Description;
        public bool IsMeat = false;

        public static FoodType Apple = new FoodType("Apple");
        public static FoodType Orange = new FoodType("Orange");
        public static FoodType Tomato = new FoodType("Tomato");
        public static FoodType Gourd = new FoodType("Gourd");
        public static FoodType Pepper = new FoodType("Pepper");
        public static FoodType Lettuce = new FoodType("Lettuce");
        public static FoodType Radish = new FoodType("Radish");
        public static FoodType Carrot = new FoodType("Carrot");
        public static FoodType Banana = new FoodType("Banana");
        public static FoodType Meat = new FoodType("Meat", isMeat: true) {
            Description = new RichText("It's just <brown>MEAT</brown>. It does not eat.")  
        };

        public static FoodType Victory = new FoodType("Tomato of Victory", DawnLike.Still("Items/Food", "Tomato of Victory")) {
            Description = new RichText("<gold>It's TOMATO OF VICTORY!\nEat it!</gold>")
        };

        public FoodType(string name, IAnimation animation, bool isMeat = false)
        {
            Name = name;
            Animation = animation;
            IsMeat = isMeat;
        }

        public FoodType(string name, bool isMeat = false)
        {
            Name = name;
            IsMeat = isMeat;

            Animation = DawnLike.Still("Items/Food", name);
        }
    }
}