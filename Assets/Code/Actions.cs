using System;
using UnityEngine;

namespace Application {
    public static class Actions {
        public static void UnlockDoor(Entity actor, Entity target) {
            target.Get<DoorLockComponent>().IsLocked = false;
            target.Get<DoorLockComponent>().JustUnlocked = true;

            target.Add(new ShakeTweenComponent());
            actor.Add(new AttackTweenComponent(target.Pos() - actor.Pos()));

            Utils.Face(actor, target.Pos() - actor.Pos());

            if (target.IsAvatar()) {
                Messages.Write($"{actor.Describe()} unlocks your doors.");
            } else if (actor.IsAvatar()) {
                Messages.Write($"You unlock {target.Describe()}'s doors.");
            }
        }

        public static void LockDoor(Entity actor) {
            actor.Get<DoorLockComponent>().IsLocked = true;

            if (actor.IsAvatar()) {
                Messages.Write("You lock your doors.");
            }
        }

        public static void Web(Entity actor, Entity target) {
            if (target.MaybeGet<WebbedComponent>() is WebbedComponent existingComponent) {
                existingComponent.RemainingDuration = Constants.WebDuration;
            } else {
                target.Add(new WebbedComponent());
            }

            if (actor.IsAvatar()) {
                Messages.Write($"You bind {target.Describe()} in your web!");
            } else if (target.IsAvatar()) {
                Messages.Write($"{actor.Describe()} binds you in its web!");
            }
        }

        public static void Eject(Entity actor, Entity target, Entity door) {
            Movement.Exit(target, door);

            if (actor.IsAvatar()) {
                Messages.Write($"You put down {target.Describe()}.");
            }
        }

        public static void Grab(Entity actor, Entity target) {
            Actions.PickUp(actor, target);
        }

        public static void PickUp(Entity entity, Entity item) {
            if (item.GetMap().Holder is Entity holder && holder.IsAvatar() && item.Has<FoodComponent>()) {
                Messages.Write($"<red>{entity.Describe()} takes your {item.Describe()}!</red>");
            } else if (entity.IsAvatar()) {
                Messages.Write($"You pick up {item.Describe()}.");
            }

            Movement.Enter(item, entity);
        }

        /*
        public static void PickUp(Entity entity, Entity item) {
            Utils.EnsureInventory(entity);

            var oldPlacement = item.Get<PlacementComponent>().Placement;

            System.Diagnostics.Debug.Assert(
                entity.Has<InventoryComponent>(),
                $"{entity} can't pick up {item} without an inventory!"
            );
    
            var destMap = entity.Get<InventoryComponent>().Map;

            Point destPoint = GetPickUpDestination(item, destMap);

            var destPlacement = new Placement(destMap, destPoint);
            var couldntPush = !Movement.PushIfNecessary(destPlacement);

            if (couldntPush) {
                Debug.LogWarning("Couldn't make space for item!");
            } else {
                if (item.GetMap().Holder.IsAvatar() && item.Has<FoodComponent>()) {
                    Messages.Write($"<red>{entity.Describe()} takes your {item.Describe()}!</red>");
                } else if (entity.IsAvatar()) {
                    Messages.Write($"You pick up {item.Describe()}.");
                }

                Movement.Place(item, destPlacement);

                item.Add(new PickedUpTweenComponent() {
                    OldPlacement = oldPlacement
                });
            }
        }

        private static Point GetPickUpDestination(Entity item, Map map) {
            var destPoint = MapUtils.FindPoint(map, p => Movement.CanStepInto(item, p));
            
            if (destPoint.HasValue) {
                return destPoint.Value;
            } else {
                var pushPoint = MapUtils.FindPoint(map, p => Movement.CanPushInto(item, p));

                if (pushPoint.HasValue) {
                    return pushPoint.Value;
                } else {
                    throw new Exception("Could not find destination for picked-up item");
                }
            }
        }
        */

        public static void Bash(Entity actor, Entity target) {
            var bashDestination = target.Pos() + (target.Pos() - actor.Pos());

            if (Movement.CanOccupyTile(target, bashDestination)) {
                Movement.WalkTo(target, bashDestination);
            }

            target.Add(new StunComponent());
            
            actor.Add(new AttackTweenComponent(target.Pos() - actor.Pos()));
            Utils.Face(actor, target.Pos() - actor.Pos());

            if (target.MaybeGet<DoorLockComponent>() is DoorLockComponent doorLock) {
                doorLock.IsLocked = false;
                doorLock.JustUnlocked = true;
            }

            if (actor.IsAvatar()) {
                Messages.Write($"<blue>You bash {target.Describe()}!</blue>");
            } else if (target.IsAvatar()) {
                Messages.Write($"<blue>{actor.Describe()} bashes you!</blue>");
            }
        }
    }
}