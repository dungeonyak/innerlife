namespace Application
{
    public static class Constants
    {
        public const int MaxSatiety = 120;
        public const int BoredomThreshold = 50;
        public const int MinVictoryDepth = 2;
        public const int WebDuration = 2;
    }
}