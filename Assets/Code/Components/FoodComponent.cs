using System;
namespace Application {
    public class FoodComponent : Component {
        public FoodType FoodType;

        public FoodComponent(FoodType foodType)
        {
            FoodType = foodType;
        }
    }
}
