using System;
namespace Application {
    public class ShakeTweenComponent : Component {
        public readonly int Magnitude;
        public double Progress = 0;

        public ShakeTweenComponent(int magnitude = 1) {
            Magnitude = magnitude;
        }
    }
}
