using System;
namespace Application {
    public class FacingComponent : Component {
        public Facing Facing;

        public FacingComponent(Facing facing = Facing.Left) {
            Facing = facing;
        }
    }
}
