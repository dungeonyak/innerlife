namespace Application {
    public class DoorComponent : Component {
        public readonly Point Direction;

        public DoorComponent(Point direction) {
            Direction = direction;
        }
    }
}