using System;
namespace Application {
    public class SpeciesComponent : Component {
        public readonly Species Species;

        public SpeciesComponent(Species species) {
            Species = species;
        }
    }
}
