﻿using System;
namespace Application {
    public class BodySpriteComponent : Component {
        public readonly IAnimation Animation;

        public BodySpriteComponent(IAnimation animation) {
            Animation = animation;
        }
    }
}
