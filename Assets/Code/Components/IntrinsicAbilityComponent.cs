using System.Collections.Generic;

namespace Application {
    public class IntrinsicAbilityComponent : Component {
        public ICollection<Ability> Abilities = new HashSet<Ability>();
    }
}