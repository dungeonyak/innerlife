namespace Application {
    public class InventoryComponent : Component {
        public readonly Map Map;

        public InventoryComponent(Map map) {
            Map = map;
        }
    }
}