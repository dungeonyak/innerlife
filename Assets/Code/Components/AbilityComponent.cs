using System.Collections.Generic;

namespace Application {
    public class AbilityComponent : Component {
        public ICollection<Ability> Abilities = new HashSet<Ability>();
    }
}