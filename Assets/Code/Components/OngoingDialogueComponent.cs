using System.Collections.Generic;

namespace Application {
    public class OngoingDialogueComponent : Component {
        public IEnumerable<RichText> Lines;
        public int CurrentLineIndex = 0;
        public int CurrentLineProgress = 0;
    }
}