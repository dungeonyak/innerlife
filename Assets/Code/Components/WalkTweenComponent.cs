﻿using System;
namespace Application {
    public class WalkTweenComponent : Component {
        public Point WorldOrigin;
        public Point? SecondOrigin;
        public double Progress = 0;

        public WalkTweenComponent() {
        }
    }
}
