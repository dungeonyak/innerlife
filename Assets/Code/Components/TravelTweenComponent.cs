using System;
namespace Application {
    public class TravelTweenComponent : Component {
        public Entity EmbarkGate;
        public Entity DisembarkGate;
        public Placement OldPlacement;
        public Placement NewPlacement;
        public double Progress = 0;

        public TravelTweenComponent() {
        }
    }
}
