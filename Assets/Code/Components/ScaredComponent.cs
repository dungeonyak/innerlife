namespace Application
{
    public class ScaredComponent : Component
    {
        public Entity Scarer;

        public ScaredComponent(Entity scarer) {
            Scarer = scarer;
        }
    }
}