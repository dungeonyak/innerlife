namespace Application {
    public class DoorLockComponent : Component {
        public bool IsLocked = true;
        public bool JustUnlocked = false;
    }
}