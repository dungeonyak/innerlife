using System;
namespace Application {
    public class AttackTweenComponent : Component {
        public readonly Point Direction;
        public double Progress = 0;

        public AttackTweenComponent(Point direction) {
            Direction = direction;
        }
    }
}
