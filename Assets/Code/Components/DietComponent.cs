namespace Application {
    public class DietComponent : Component {
        public Diet Diet;

        public DietComponent(Diet diet) {
            Diet = diet;
        }
    }
}