﻿using System;
using System.Drawing;

namespace Application {
    public class PlacementComponent : Component {
        public Placement Placement;

        public Map Map => Placement.Map;
        public Point Position => Placement.Position;

        public PlacementComponent(Placement placement) {
            Placement = placement;
        }
    }
}
