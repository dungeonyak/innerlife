namespace Application {
    public class DecorSpriteComponent : Component {
        public IAnimation Animation;

        public DecorSpriteComponent(IAnimation animation) {
            Animation = animation;
        }
    }
}