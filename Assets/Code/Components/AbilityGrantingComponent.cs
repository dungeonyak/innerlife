using System.Collections.Generic;

namespace Application {
    public class AbilityGrantingComponent : Component {
        public readonly Ability Ability;

        public AbilityGrantingComponent(Ability ability) {
            Ability = ability;
        }
    }
}