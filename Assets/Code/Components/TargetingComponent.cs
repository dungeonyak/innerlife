using System;
namespace Application {
    public class TargetingComponent : Component {
        public TargetingIntent Intent;

        public TargetingComponent(TargetingIntent intent) {
            Intent = intent;
        }
    }
}
