﻿using System;
using System.Collections.Generic;

namespace Application {
    public class Entity {
        public string Name = "Entity";

        private Dictionary<Type, Component> Components = new Dictionary<Type, Component>();

        public ComponentType Add<ComponentType>(ComponentType component)
            where ComponentType : Component {
            Components[typeof(ComponentType)] = component;

            Container.Resolve<EntityFinder>().ComponentAdded(component, this);

            return component;
        }

        public bool Has<ComponentType>()
            where ComponentType : Component {
            return MaybeGet<ComponentType>() != null;
        }

        public ComponentType Get<ComponentType>()
            where ComponentType : Component {
            return (ComponentType) Components[typeof(ComponentType)];
        }

        public ComponentType MaybeGet<ComponentType>()
            where ComponentType : Component {
            return (ComponentType) Components.GetDefault(typeof(ComponentType));
        }

        public void Remove<ComponentType>(ComponentType component = null)
            where ComponentType : Component {
            Components.Remove(typeof(ComponentType));

            Container.Resolve<EntityFinder>().ComponentRemoved(typeof(ComponentType), this);
        }

        public void Remove(Type componentType)
        {
            Components.Remove(componentType);
            Container.Resolve<EntityFinder>().ComponentRemoved(componentType, this);   
        }

        public void RemoveIfPresent<ComponentType>(ComponentType component = null)
            where ComponentType : Component {
            if (Components.ContainsKey(typeof(ComponentType))) {
                Components.Remove(typeof(ComponentType));

                Container.Resolve<EntityFinder>().ComponentRemoved(typeof(ComponentType), this);
            }
        }

        public override string ToString() {
            return $"Entity \"{Name}\"";
        }
    }
}
