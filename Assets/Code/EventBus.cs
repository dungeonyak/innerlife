﻿using System;
using System.Collections.Generic;

namespace Application {
    public class EventBus {
        private Dictionary<Type, ICollection<object>> Handlers = new Dictionary<Type, ICollection<object>>();

        public void Subscribe<EventType>(Action<EventType> handler) {
            Subscribe(typeof(EventType), handler);
        }

        public void Subscribe(Type type, object handler) {
            Handlers
                .SetDefault(type, () => new List<object>())
                .Add(handler);
        }

        public void Dispatch<EventType>(EventType e) {
            if (Handlers.GetDefault(typeof(EventType)) is ICollection<object> handlers) {
                foreach (var handler in handlers) {
                    if (GameState.IsRestarting) {
                        break;
                    }

                    var callback = (Action<object>) handler;
                    callback(e);
                }
            }
        }

        public void Clear() {
            Handlers.Clear();
        }
    }
}
