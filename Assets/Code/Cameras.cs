﻿using System;
using System.Drawing;
using UnityEngine;

namespace Application {
    public static class Cameras {
        public static GameObject MainMap => GameObject.Find("Main Map Camera");
        public static GameObject InventoryMap => GameObject.Find("Inventory Camera");
        public static GameObject Ui => GameObject.Find("UI Camera");
        public static GameObject LeftMap => GameObject.Find("Left Map Camera");
        public static GameObject RightMap => GameObject.Find("Right Map Camera");
        public static GameObject UpMap => GameObject.Find("Up Map Camera");
        public static GameObject DownMap => GameObject.Find("Down Map Camera");

        public static void Configure(GameObject cameraObject,
            Point dimensions,
            Point position,
            int scale
        ) {
            var cameraComponent = cameraObject.GetComponent<Camera>();
            cameraComponent.orthographicSize = (float)dimensions.Y / 2;
            cameraComponent.aspect = (float)dimensions.X / dimensions.Y;
            cameraComponent.rect = new Rect(
                (float)position.X / Screen.width,
                (float)position.Y / Screen.height,
                ((float)(dimensions.X * scale) / Screen.width),
                ((float)(dimensions.Y * scale) / Screen.height)
            );
        }

        public static Rect GetViewRectangle(GameObject cameraObject) {
            var cameraComponent = cameraObject.GetComponent<Camera>();

            float height = (cameraComponent.orthographicSize * 2);
            float width = (height * cameraComponent.aspect);

            float left = (cameraObject.transform.position.x) - (width / 2);
            float top = (cameraObject.transform.position.y) - (height / 2);

            return new Rect(
                (int) left,
                (int) top,
                (int) width,
                (int) height
            );
        }

        public static Point? GetMousePosition(GameObject camera) {
            var screenMouse = new Vector2(
                Input.mousePosition.x,
                Input.mousePosition.y
            );

            var cameraRect = camera.GetComponent<Camera>().rect;
            var screenRect = new Rect(
                cameraRect.x * Screen.width,
                cameraRect.y * Screen.height,
                cameraRect.width * Screen.width,
                cameraRect.height * Screen.height
            );

            if (screenRect.Contains(screenMouse)) {
                var viewRect = GetViewRectangle(camera);
                var cameraMouse = screenMouse - screenRect.position;
                var cameraScale = screenRect.height / (camera.GetComponent<Camera>().orthographicSize * 2);

                return new Point(
                    (int) (cameraMouse.x / cameraScale),
                    (int) (cameraMouse.y / cameraScale)
                ) + new Point(
                    (int) viewRect.xMin,
                    (int) viewRect.yMin
                );
            } else {
                return null;
            }
        }

        public static Rectangle ScreenRect(GameObject camera) {
            return camera.GetComponent<Camera>().pixelRect.ToRectangle();
        }
    }
}
