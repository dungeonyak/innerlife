using System;
using System.Linq;
using System.Reflection;

namespace Application
{
    public static class GameState
    {
        public static int CrownsCreated;
        public static bool IsRestarting;

        public static void Initialize() {
            CrownsCreated = 0;
            IsRestarting = false;

            InitializeListeners();

            var map = MapBuilderFactory
                .GetBuilder(null)
                .Build();

            var guy = new Entity();
            guy.Name = "You";
            guy.Add(new HnauComponent());
            guy.Add(new BodySpriteComponent(DawnLike.Animation("Characters/Player", 19)));
            guy.Add(new PlayerControlledComponent());

            var innerMap = MapBuilderFactory.GetBuilder(guy).Build();
            guy.Add(new InventoryComponent(innerMap));

            guy.Add(new FacingComponent());

            guy.Add(new HungerComponent());

            var intrinsics = guy.Add(new IntrinsicAbilityComponent());
            intrinsics.Abilities.Add(Ability.Bash);
            intrinsics.Abilities.Add(Ability.Grab);
            intrinsics.Abilities.Add(Ability.Eject);
            intrinsics.Abilities.Add(Ability.Compass);

            Utils.ApplyIntrinsics(guy);

//            guy.Add(new DoorLockComponent());

            Movement.Place(guy, new Placement(map, new Point(map.Width / 2, map.Height / 2)));
    
            Container.Resolve<EventBus>().Dispatch(new Ready());
        }

        private static void InitializeListeners() {
            var types = Assembly.GetAssembly(typeof(GameState))
                .GetTypes()
                .Where(type => type.GetCustomAttributes(typeof(ListenerAttribute), true).Length > 0);

            var eventBus = Container.Resolve<EventBus>();

            foreach (var type in types) {
                var instance = Container.Resolve(type);

                var handlers = type.GetMethods(BindingFlags.Public | BindingFlags.Instance)
                    .Where(method => method.GetCustomAttributes<HandlerAttribute>(true).Count() > 0);

                foreach (var handler in handlers) {
                    var eventType = handler.GetParameters().First().ParameterType;
                    Action<object> callback = e => handler.Invoke(instance, new object[] { e });
                    eventBus.Subscribe(eventType, callback);
                }
            }
        }

        public static void Restart() {
            Initialize();
        }
    }
}