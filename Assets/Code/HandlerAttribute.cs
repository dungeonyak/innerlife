using System;
namespace Application {
    [AttributeUsage(AttributeTargets.Method)]
    public class HandlerAttribute : Attribute {
        public int Priority = 0;
    }
}
