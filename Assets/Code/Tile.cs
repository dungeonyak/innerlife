﻿using System;
namespace Application {
    public enum Tile {
        Wall,
        Floor,
        Grass,
        Water,
        Scales,
        Pit,
        Void,
    }
}
