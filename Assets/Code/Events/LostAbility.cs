namespace Application {
    public class LostAbility {
        public readonly Entity Entity;
        public readonly Ability Ability;

        public LostAbility(Entity entity, Ability ability) {
            Entity = entity;
            Ability = ability;
        }
    }
}