namespace Application {
    public class GainedAbility {
        public readonly Entity Entity;
        public readonly Ability Ability;

        public GainedAbility(Entity entity, Ability ability) {
            Entity = entity;
            Ability = ability;
        }
    }
}