namespace Application {
    public class EntityChangedMap {
        public readonly Entity Entity;
        public Map OldMap;
        public Map NewMap;

        public EntityChangedMap(Entity entity) {
            Entity = entity;
        }
    }
}