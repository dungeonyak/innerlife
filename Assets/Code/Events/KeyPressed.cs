using UnityEngine;

namespace Application {
    public struct KeyPressed {
        public KeyCode Key;

        public KeyPressed(KeyCode key) {
            Key = key;
        }
    }
}