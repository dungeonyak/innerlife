namespace Application {
    public enum Ability {
        Compass,
        Bash,
        Armor,
        Grab,
        Eject,
        Web,
        Flight,
        Speed,
        Shove,
    }
}