using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Text.RegularExpressions;
using System.IO;

public class MenuItems : MonoBehaviour {

    [MenuItem("Sprites/Set Pivot(s)")]
    static void SetPivots() {

        UnityEngine.Object[] textures = GetSelectedTextures();

        Selection.objects = new UnityEngine.Object[0];
        foreach (Texture2D texture in textures) {
            string path = AssetDatabase.GetAssetPath(texture);
            TextureImporter ti = AssetImporter.GetAtPath(path) as TextureImporter;
            ti.isReadable = false; // this 2 lines you need to add
            AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
            ti.isReadable = true;
            List<SpriteMetaData> newData = new List<SpriteMetaData>();
            for (int i = 0; i < ti.spritesheet.Length; i++) {
                SpriteMetaData d = ti.spritesheet[i];
                d.alignment = 9;
                d.pivot = ti.spritesheet[0].pivot;
                newData.Add(d);
            }
            ti.spritesheet = newData.ToArray();
            AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
        }
    }

    [MenuItem("Sprites/Generate Prefabs")]
    static void GeneratePrefabs() {
        var sprites = AssetDatabase.FindAssets("t:Sprite")
           .Select(guid => AssetDatabase.GUIDToAssetPath(guid))
           .Distinct()
           .SelectMany(path => {
               var sprs = AssetDatabase.LoadAllAssetsAtPath(path).OfType<Sprite>();
               return sprs.Select(sprite => (sprite, path));
           });

        foreach (var (sprite, path) in sprites) {
            var prefabDirName = "Assets/Resources/AutoPrefabs/" + Regex.Replace(Regex.Replace(path, "^Assets/", ""), "\\..+$", "");

            EnsureFolder(prefabDirName);

            var prefabPath = prefabDirName + "/" + sprite.name + ".prefab";

            if (AssetDatabase.LoadAssetAtPath(prefabPath, typeof(GameObject)) == null) {
                var gameObject = new GameObject();
                var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
                spriteRenderer.sprite = sprite;
                PrefabUtility.SaveAsPrefabAsset(gameObject, prefabPath);
                GameObject.DestroyImmediate(gameObject);
            }
        }
        /*
        Object[] textures = GetSelectedTextures();

        Selection.objects = new Object[0];
        foreach (Texture2D texture in textures) {
            string path = AssetDatabase.GetAssetPath(texture);
            TextureImporter ti = AssetImporter.GetAtPath(path) as TextureImporter;
            ti.isReadable = false; // this 2 lines you need to add
            AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
            ti.isReadable = true;
            List<SpriteMetaData> newData = new List<SpriteMetaData>();
            for (int i = 0; i < ti.spritesheet.Length; i++) {
                SpriteMetaData d = ti.spritesheet[i];
                d.alignment = 9;
                d.pivot = ti.spritesheet[0].pivot;
                newData.Add(d);
            }
            ti.spritesheet = newData.ToArray();
            AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
        }*/
    }

    private static void EnsureFolder(string path, string parent = "") {
        var parts = path.Split('/');
        var first = parts.First();

        if (!AssetDatabase.IsValidFolder(parent + "/" + first)) {
            AssetDatabase.CreateFolder(parent, first);
        }

        var rest = parts.Skip(1);

        if (rest.Any()) {
            EnsureFolder(String.Join("/", rest), (parent + "/" + first).Trim('/'));
        }
    }

    static UnityEngine.Object[] GetSelectedTextures() {
        return Selection.GetFiltered(typeof(Texture2D), SelectionMode.DeepAssets);
    }
}